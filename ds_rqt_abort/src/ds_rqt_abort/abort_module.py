# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import time
from datetime import datetime

from qt_gui.plugin import Plugin
# python_qt_binding importing and acting like PyQt5, it seems
#from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import *
from python_qt_binding.QtCore import QThread, pyqtSignal, Qt
from ds_core_msgs.msg import Abort
from ds_hotel_msgs.srv import AbortCmdRequest
from ds_hotel_msgs.srv import AbortCmd

module_path = os.path.abspath(__file__)

class AbortPlugin(Plugin):

    # To be emitted upon receiving a message on the /abort topic:
    ttl_signal = pyqtSignal(float)
    # For TimerTTLThread thread to emit:
    no_data_since_ttl_signal = pyqtSignal()
    # For TimerTTLThread thread to emit to update GUI periodically:
    ttl_update_signal = pyqtSignal(float)

    def __init__(self, context):
        super(AbortPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('AbortPlugin')
        print 'ABORT PLUGIN LOADED'

        # Create an empty Abort message to later modify:
        self.abort_message = Abort()
        self.abort_cmd = AbortCmdRequest(0)

        # Initialize as False, that way if no data on /abort topic at start,
        # clicking ABORT button outputs a True in the /abort_cmd message
        self.abort_state = False

        # Initialize abort command publisher:
        #self.pub_abort_cmd = rospy.Publisher('abort_cmd', Abort, queue_size=1)
        self.abort_cmd_client = rospy.ServiceProxy("/sentry/abort_cmd", AbortCmd)

        # Three abort states:
        #   1. Abort
        #   2. Don't abort
        #   3. No data

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        self._init_widgets()
        self._init_stylesheet()

        # Get path to UI file which should be in the "resource" folder of this package
        #ui_file = os.path.join(rospkg.RosPack().get_path('ds_rqt_demo'), 'resource', 'demo_dialog.ui')
        # Extend the widget with all attributes and children from UI file
        #loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('AbortPluginUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.

        self._widget.setWindowTitle('Abort Monitor')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
#        else:
#            self._widget.setWindowTitle('Demo rqt Plugin')


        # Add widget to the user interface
        context.add_widget(self._widget)
        #context.add_widget(self.hb)

        self.no_data_since_ttl_signal.connect(self.display_no_data)
        self.ttl_update_signal.connect(self.update_ttl_label)
        # Initialize ttl timer thread:
        self.ttl_thread = TimerTTLThread(self.no_data_since_ttl_signal,
                                         self.ttl_update_signal)
        self.ttl_signal.connect(self.ttl_thread.get_ttl)
        # Calling a QThread object's start() method starts running its run()
        # method:
        self.ttl_thread.start()

        # Create a ROS subscriber:
        self._abort_subscription = rospy.Subscriber('/sentry/abort', Abort,
                                                    self._abort_msg_callback)

    def _init_stylesheet(self):
        style_file = os.path.join(os.path.dirname(module_path),
                                  'sentrysitter.qss')
        with open(style_file, 'r') as f:
            self._widget.setStyleSheet(f.read())

    def _init_widgets(self):
        # Create QWidget
        self._widget = QWidget()
        self._widget.setWindowTitle('Abort Monitor')

        # Create some QtWidgets
        # Overall window layout:
        self.layout = QVBoxLayout(self._widget)
        self.layout.setObjectName('layout')
        self.top_vb = QVBoxLayout() #(self._widget)
        self.top_vb.setObjectName('top_vb')
        self.groupbox_vb = QVBoxLayout() #(self._widget)
        self.groupbox_vb.setObjectName('groupbox_vb')
        self.abort_button = QPushButton('ABORT', self._widget)
        self.abort_button.setObjectName('ss_button')
        self.enable_button = QPushButton('ENABLE', self._widget)
        self.enable_button.setObjectName('ss_button')
        self.abort_button.setMaximumWidth(175)
        self.enable_button.setMaximumWidth(175)
        self.abort_button.clicked.connect(self.publish_abort_cmd)
        self.enable_button.clicked.connect(self.toggle_enable_cmd)
        self.top_vb.addWidget(self.abort_button)
        self.top_vb.addWidget(self.enable_button)

        # ttl label:
        self.ttl_label = QLabel(self._widget)
        self.ttl_label.setObjectName('ttl_label')
        self.display_no_data()
        self.top_vb.addWidget(self.ttl_label)
        self.top_vb.setAlignment(Qt.AlignTop)

        # abort_cmd TextEdit textbox to keep a log of times when messages are
        # published to the /abort_cmd topic:
        #self.abort_cmd_log = QTextEdit(self._widget)
        #self.abort_cmd_log.setObjectName('abort_cmd_log')
        #self.abort_cmd_log.setReadOnly(True)
        #self.groupbox_vb.addWidget(self.abort_cmd_log)

        # QGroupBox for QTextEdit
#        self.gb = QGroupBox('Log of times published to /abort_cmd topic', self._widget)
        #self.gb = QGroupBox('Log of publishing to /abort_cmd topic', self._widget)
        #self.gb.setObjectName('gb')
        #self.gb.setLayout(self.groupbox_vb)
        self.layout.addLayout(self.top_vb)
        #self.layout.addWidget(self.gb)
        self.layout.setAlignment(Qt.AlignTop)

    def display_no_data(self):
        try:
            self.ttl_label.setText('State: NO DATA')
            self.abort_button.setProperty('Status', 'disabled')
            self.enable_button.setProperty('Status', 'disabled')
            self.abort_button.setStyle(self.abort_button.style())
            self.enable_button.setStyle(self.enable_button.style())
        except RuntimeError:
            # Can occur when trying to close plugin and TimerTTLThread
            # continues to run and emits a signal
            # Quit timer thread:
            #self.ttl_thread.quit()
            self.shutdown_plugin()

    def update_ttl_label(self, ttl):
        try:
            self.ttl_label.setText('State: Time to live = {0} s'.format(ttl))
        except RuntimeError:
            # Can occur when trying to close plugin and TimerTTLThread
            # continues to run and emits a signal
            # Quit timer thread:
            #self.ttl_thread.quit()
            self.shutdown_plugin()

    def publish_abort_cmd(self):
        # Toggle abort field based on last abort state received on /abort topic:
        self.abort_message.abort = not self.abort_state

        if self.abort_message.abort:
            self.abort_cmd.command = 1 # abort
        else:
            self.abort_cmd.command = 2 # don't abort

        # Keep self.abort_message.enable the same as in previous message
        #self.pub_abort_cmd.publish(self.abort_message)
        self.abort_cmd_client(self.abort_cmd)

        #preexisting_text = self.abort_cmd_log.toPlainText()
        #self.abort_cmd_log.setPlainText(preexisting_text+
        #                           'ABORT - Message published at UTC datetime: '+
        #                           datetime.strftime(datetime.utcnow(),
        #                                             '%Y-%m-%d %H:%M:%S')+
        #                           '\n')
        #scroll_bar = self.abort_cmd_log.verticalScrollBar()
        #scroll_bar.setValue(scroll_bar.maximum())

    def toggle_enable_cmd(self):
        # Toggle enable field:
        self.abort_message.enable = not self.abort_message.enable

        if self.abort_message.enable:
            self.abort_cmd.command = 3 # enable
        else:
            self.abort_cmd.command = 4 # disable

        # Keep self.abort_message.abort the same as in previous message
        #self.pub_abort_cmd.publish(self.abort_message)
        self.abort_cmd_client.call(self.abort_cmd)

        #preexisting_text = self.abort_cmd_log.toPlainText()
        #self.abort_cmd_log.setPlainText(preexisting_text+
        #                           'ENABLE - Message published at UTC datetime: '+
        #                           datetime.strftime(datetime.utcnow(),
        #                                             '%Y-%m-%d %H:%M:%S')+
        #                           '\n')
        #scroll_bar = self.abort_cmd_log.verticalScrollBar()
        #scroll_bar.setValue(scroll_bar.maximum())

    def _abort_msg_callback(self, msg):
        self.abort_state = msg.abort
        self.enable_state = msg.enable
        # Send a signal about time to live:
        self.ttl_signal.emit(msg.ttl)
        # Update GUI:
        if self.abort_state:
            self.abort_button.setProperty('widgetStatus', 'error')
            self.abort_button.setStyle(self.abort_button.style())
        else:
            self.abort_button.setProperty('widgetStatus', 'good')
            self.abort_button.setStyle(self.abort_button.style())
        if self.enable_state:
            self.enable_button.setProperty('widgetStatus', 'good')
            self.enable_button.setStyle(self.enable_button.style())
        else:
            self.enable_button.setProperty('widgetStatus', 'error')
            self.enable_button.setStyle(self.enable_button.style())

    def shutdown_plugin(self):
        # Gets run when user closes plugin
        # TODO unregister all publishers here
        # TODO: Sometimes, a "Segmentation fault (core dumped)" error prints
        # out to the terminal. Not exactly sure what is causing this, but
        # ideally this should be fixed
        self._abort_subscription.unregister()
        self.abort_cmd_client.close()
        self.ttl_thread.quit_thread()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog


class TimerTTLThread(QThread):
    def __init__(self, no_data_since_ttl_signal, ttl_update_signal):
        QThread.__init__(self)
        self.no_data_since_ttl_signal = no_data_since_ttl_signal
        self.ttl_update_signal = ttl_update_signal
        self.time_increment = 0.5 # number of seconds between checks to ttl
        self.got_ttl_signal = False
        #self.got_first_signal = False
        self.still_running = True

#    def __del__(self):
#        self.wait()

    def run(self):
        while self.still_running:
            if self.got_ttl_signal:
                self.ttl_countdown(self.ttl)
                self.got_ttl_signal = False
            else:
                self.usleep(int(0.05*1e6))

    def ttl_countdown(self, ttl):
        elapsed_time = 0.
        tw = time.time()
        # If we are constantly receiving new abort messages before ttl
        # runs out, we should remain in this loop:
        while self.ttl > 0 and self.still_running:
            self.ttl_update_signal.emit(self.ttl)
            cur_wall_time = time.time()
            sleep_time = tw + self.time_increment - cur_wall_time
            if sleep_time >= 0:
                # Puts the QThread to sleep for n microseconds:
                self.usleep(int(sleep_time*1e6))
            tw += self.time_increment
            self.ttl -= self.time_increment

        # At this point, ttl has run out, so send a signal to publish message
        # on the /abort_cmd topic and to change the GUI:
        self.no_data_since_ttl_signal.emit()

    def get_ttl(self, ttl):
        self.ttl = ttl
        self.got_ttl_signal = True
        #self.got_first_signal = True

    def quit_thread(self):
        # Ensure that the while loops of this thread cease to continue/block:
        self.still_running = False






