README for Abort Monitor plugin (ds_rqt_abort)
Written by Theo Guerin (tguerin@whoi.edu) 2018-01-19

Note: In this document, ">> " indicates a terminal command.
Version information:
    This rqt plugin was made while using ROS Kinetic.

###############################################################################
BRIEF OVERVIEW

This plugin allows the user to monitor incoming Abort messages on the /abort
topic and send abort commands on the /abort_cmd topic.

###############################################################################
STYLESHEET REQUIREMENT

Requires style.qss file that has been modified specifically for this plugin
(making use of custom, dynamic properties) and can be modified further for
additional rqt plugins.

    Some stylesheet documentation:
        - https://wiki.qt.io/Dynamic_Properties_and_Stylesheets
        - http://dgovil.com/blog/2017/02/24/qt_stylesheets/
        - http://doc.qt.io/archives/qt-4.8/stylesheet-reference.html


###############################################################################
RUNNING THIS PLUGIN

- You should run rqt in a terminal in which one of the devel/setup.*sh files of
  your catkin workspace has been sourced. E.g., in a catkin workspace:

    >> source devel/setup.bash


- Run this plugin with rqt, either standalone or not:

  1. Not standalone (i.e., can add other plugins to rqt environment):

    >> rqt

    The plugin should appear in the menu bar under Plugins in a folder called
    ds_rqt_plugins. If it does not appear in the menu bar, try:

    >> rqt --force-discover


  2. Standalone:

    >> rqt -s AbortPlugin

    As in Option 1, if the plugin does not appear with the above command, try:

    >> rqt -s AbortPlugin --force-discover



