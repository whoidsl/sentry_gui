# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from functools import partial
import math
import os
import roslib
import rospy
import threading
import yaml


from ds_actuator_msgs.msg import ServoCmd
import sentry_msgs.srv
import sentry_msgs.msg
from sentry_msgs.srv._SailServoCmd import SailServoCmd, SailServoCmdRequest

from qt_gui.plugin import Plugin
from python_qt_binding.QtWidgets import QWidget, QLabel, QGridLayout, QPushButton, QMessageBox, QVBoxLayout
from python_qt_binding.QtCore import Qt
from python_qt_binding.QtGui import QFont


module_path = os.path.abspath(__file__)

class ServoPlugin(Plugin):
    def __init__(self, context):
        super(ServoPlugin, self).__init__(context)
        self.setObjectName('ServoPlugin')

        # Create the core widget
        self._widget = QWidget()

        self._layout = QVBoxLayout(self._widget)

        # Font for fwd/aft label
        position_font = QFont()
        position_font.setPointSize(14)

        servo_font = QFont()
        servo_font.setPointSize(18)
        servo_font.setBold(True)

        #########################
        # Legacy GUI for SAIL servo

        sail_label = QLabel("SAIL SERVO")  # because I didn't know how to make it bold
        sail_label.setFont(servo_font)

        self._sail_layout = QGridLayout()

        self._fwd_lbl = QLabel("Fwd", self._widget)
        self._fwd_lbl.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self._fwd_lbl.setFont(position_font)
        self._fwd_index  = QPushButton("Index", self._widget)
        self._fwd_brake  = QPushButton("Brake", self._widget)
        self._fwd_offset = QPushButton("Offset", self._widget)
        self._fwd_coast  = QPushButton("Coast", self._widget)
        self._fwd_sleep  = QPushButton("Sleep", self._widget)

        self._aft_lbl = QLabel("Aft", self._widget)
        self._aft_lbl.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
        self._aft_lbl.setFont(position_font)
        self._aft_index = QPushButton("Index", self._widget)
        self._aft_brake = QPushButton("Brake", self._widget)
        self._aft_offset = QPushButton("Offset", self._widget)
        self._aft_coast = QPushButton("Coast", self._widget)
        self._aft_sleep = QPushButton("Sleep", self._widget)

        # Add widgets to the layout
        self._sail_layout.addWidget(self._fwd_lbl,    0, 0)
        self._sail_layout.addWidget(self._fwd_index,  1, 0)
        self._sail_layout.addWidget(self._fwd_brake,  2, 0)
        self._sail_layout.addWidget(self._fwd_offset, 3, 0)
        self._sail_layout.addWidget(self._fwd_coast,  4, 0)
        self._sail_layout.addWidget(self._fwd_sleep,  5, 0)

        self._sail_layout.addWidget(self._aft_lbl,    0, 1)
        self._sail_layout.addWidget(self._aft_index,  1, 1)
        self._sail_layout.addWidget(self._aft_brake,  2, 1)
        self._sail_layout.addWidget(self._aft_offset, 3, 1)
        self._sail_layout.addWidget(self._aft_coast,  4, 1)
        self._sail_layout.addWidget(self._aft_sleep,  5, 1)

        # Connect the buttons to useful functions
        self._fwd_index.clicked.connect(partial(self._cmd_callback, 'fwd', SailServoCmdRequest.SERVO_CMD_INDEX))
        self._fwd_brake.clicked.connect(partial(self._cmd_callback, 'fwd', SailServoCmdRequest.SERVO_CMD_BRAKE))
        self._fwd_coast.clicked.connect(partial(self._cmd_callback, 'fwd', SailServoCmdRequest.SERVO_CMD_COAST))
        self._fwd_sleep.clicked.connect(partial(self._cmd_callback, 'fwd', SailServoCmdRequest.SERVO_CMD_SLEEP))

        self._aft_index.clicked.connect(partial(self._cmd_callback, 'aft', SailServoCmdRequest.SERVO_CMD_INDEX))
        self._aft_brake.clicked.connect(partial(self._cmd_callback, 'aft', SailServoCmdRequest.SERVO_CMD_BRAKE))
        self._aft_coast.clicked.connect(partial(self._cmd_callback, 'aft', SailServoCmdRequest.SERVO_CMD_COAST))
        self._aft_sleep.clicked.connect(partial(self._cmd_callback, 'aft', SailServoCmdRequest.SERVO_CMD_SLEEP))

        # Offset commands require topic publishers
        self._fwd_pub = rospy.Publisher('/sentry/actuators/servo_fwd/cmd', ServoCmd, queue_size=1)
        self._fwd_offset.clicked.connect(partial(self._publish_cmd, self._fwd_pub, math.pi/4))

        self._aft_pub = rospy.Publisher('/sentry/actuators/servo_aft/cmd', ServoCmd, queue_size=1)
        self._aft_offset.clicked.connect(partial(self._publish_cmd, self._aft_pub, -math.pi/4))

        # Subscribe to status updates for configure/zero results
        self._fwd_index_result_sub = rospy.Subscriber('/sentry/actuators/servo_fwd/servo_index_result',
                                                     sentry_msgs.msg.ServoIndexResult,
                                                     partial(self.indexResultCallback, "fwd"))
        self._aft_index_result_sub = rospy.Subscriber('/sentry/actuators/servo_aft/servo_index_result',
                                                     sentry_msgs.msg.ServoIndexResult,
                                                     partial(self.indexResultCallback, "aft"))

        #########################
        # GUI for the Harmonic servo

        harmonic_label = QLabel("HARMONIC SERVO")
        harmonic_label.setFont(servo_font)
        harmonic_layout = QGridLayout()

        fwd_label = QLabel("Fwd")
        fwd_coast_on_button = QPushButton("Coast On")
        fwd_coast_off_button = QPushButton("Coast Off")
        fwd_index_button = QPushButton("Go Home")
        fwd_configure_button = QPushButton("Configure")
        fwd_0deg_button = QPushButton("0 deg")
        fwd_pos90deg_button = QPushButton("+90 deg")
        fwd_neg90deg_button = QPushButton("-90 deg")
        fwd_pos45deg_button = QPushButton("+45 deg")
        fwd_neg45deg_button = QPushButton("-45 deg")
        self.index_status_format = "Homing Status: {}"
        self.index_status_labels = {"aft": QLabel(self.index_status_format.format("N/A")),
                                    "fwd": QLabel(self.index_status_format.format("N/A"))}

        fwd_coast_on_button.clicked.connect(lambda: self.coastButtonPushed("fwd", True))
        fwd_coast_off_button.clicked.connect(lambda: self.coastButtonPushed("fwd", False))
        fwd_index_button.clicked.connect(lambda: self.indexButtonPushed("fwd"))
        fwd_0deg_button.clicked.connect(lambda: self._publish_cmd(self._fwd_pub, 0))
        fwd_pos90deg_button.clicked.connect(lambda: self._publish_cmd(self._fwd_pub, math.pi/2))
        fwd_neg90deg_button.clicked.connect(lambda: self._publish_cmd(self._fwd_pub, -math.pi/2))
        fwd_pos45deg_button.clicked.connect(lambda: self._publish_cmd(self._fwd_pub, math.pi/4))
        fwd_neg45deg_button.clicked.connect(lambda: self._publish_cmd(self._fwd_pub, -math.pi/4))

        aft_label = QLabel("Aft")
        aft_coast_on_button = QPushButton("Coast On")
        aft_coast_off_button = QPushButton("Coast Off")
        aft_index_button = QPushButton("Go Home")
        aft_0deg_button = QPushButton("0 deg")
        aft_pos90deg_button = QPushButton("+90 deg")
        aft_neg90deg_button = QPushButton("-90 deg")
        aft_pos45deg_button = QPushButton("+45 deg")
        aft_neg45deg_button = QPushButton("-45 deg")
    
        aft_coast_on_button.clicked.connect(lambda: self.coastButtonPushed("aft", True))
        aft_coast_off_button.clicked.connect(lambda: self.coastButtonPushed("aft", False))
        aft_index_button.clicked.connect(lambda: self.indexButtonPushed("aft"))
        aft_0deg_button.clicked.connect(lambda: self._publish_cmd(self._aft_pub, 0))
        aft_pos90deg_button.clicked.connect(lambda: self._publish_cmd(self._aft_pub, math.pi/2))
        aft_neg90deg_button.clicked.connect(lambda: self._publish_cmd(self._aft_pub, -math.pi/2))
        aft_pos45deg_button.clicked.connect(lambda: self._publish_cmd(self._aft_pub, math.pi/4))
        aft_neg45deg_button.clicked.connect(lambda: self._publish_cmd(self._aft_pub, -math.pi/4))

        row = 0
        harmonic_layout.addWidget(fwd_label, row, 0, 1, 3)
        row += 1
        harmonic_layout.addWidget(fwd_coast_on_button, row, 0, 1, 3)
        row += 1
        harmonic_layout.addWidget(fwd_coast_off_button, row, 0, 1, 3)
        row += 1
        harmonic_layout.addWidget(fwd_index_button, row, 0, 1, 3)
        row += 1
        harmonic_layout.addWidget(self.index_status_labels["fwd"], row, 0, 1, 3)
        row += 1
        harmonic_layout.addWidget(fwd_0deg_button, row, 0)
        harmonic_layout.addWidget(fwd_pos90deg_button, row, 1)
        harmonic_layout.addWidget(fwd_neg90deg_button, row, 2)
        row += 1
        harmonic_layout.addWidget(fwd_pos45deg_button, row, 1)
        harmonic_layout.addWidget(fwd_neg45deg_button, row, 2)

        row = 0
        harmonic_layout.addWidget(aft_label, row, 3, 1, 3)
        row += 1
        harmonic_layout.addWidget(aft_coast_on_button, row, 3, 1, 3)
        row += 1
        harmonic_layout.addWidget(aft_coast_off_button, row, 3, 1, 3)
        row += 1
        harmonic_layout.addWidget(aft_index_button, row, 3, 1, 3)
        row += 1
        harmonic_layout.addWidget(self.index_status_labels["aft"], row, 3, 1, 3)
        row += 1
        harmonic_layout.addWidget(aft_0deg_button, row, 3)
        harmonic_layout.addWidget(aft_pos90deg_button, row, 4)
        harmonic_layout.addWidget(aft_neg90deg_button, row, 5)
        row += 1
        harmonic_layout.addWidget(aft_pos45deg_button, row, 4)
        harmonic_layout.addWidget(aft_neg45deg_button, row, 5)

        self._layout.addWidget(harmonic_label)
        self._layout.addLayout(harmonic_layout)
        self._layout.addStretch(1)
        self._layout.addWidget(sail_label)
        self._layout.addLayout(self._sail_layout)

        # Expose the widget

        self._init_stylesheet()
        context.add_widget(self._widget)

    def _init_stylesheet(self):
        style_file = os.path.join(os.path.dirname(module_path),
                                  'sentrysitter.qss')
        with open(style_file, 'r') as f:
            self._widget.setStyleSheet(f.read())

    def _publish_cmd(self, pub, cmdval):
        msg = ServoCmd()
        msg.stamp = rospy.Time.now()
        msg.cmd_radians = cmdval
        pub.publish(msg)

    def _cmd_callback(self, servoname, cmd):

        servicename = ('/sentry/actuators/servo_%s/ctrl_cmd' % servoname)
        rospy.loginfo('Waiting for service \"%s\"' % servicename)

        try:
            rospy.wait_for_service(servicename, 1)
        except rospy.ROSException as e:
            rospy.logerr("Unable to connect to service \"%s\"" % servicename)
            return
        srv = rospy.ServiceProxy(servicename, SailServoCmd)

        req = SailServoCmdRequest()
        req.command = cmd
        rospy.loginfo('Sending command %d to service \"%s\"' % (cmd, servicename))
        try:
            if not srv(req):
                rospy.logerr('Unable to send SAIL servo command %d to \"%s\"' % (cmd, servicename))
        except rospy.service.ServiceException as e:
            box = QMessageBox()
            box.setIcon(QMessageBox.Critical)
            box.setText("Service reports failure! (servo not enabled?)")
            box.setWindowTitle("Servo command serviced FAILED")
            box.setStandardButtons(QMessageBox.Ok)
            box.exec_()

    def indexResultCallback(self, servo, msg):
        label = self.index_status_labels[servo]
        if msg.succeeded:
            label.setText(self.index_status_format.format("succeeded"))
            style = "QLabel { background-color : green; color : black; }"
        else:
            label.setText(self.index_status_format.format("failed"))
            style = "QLabel { background-color : red; color : black; }"
        label.setStyleSheet(style)
    
    def coastButtonPushed(self, servo, coast):
        '''
        Handles the new-style coast command

        Parameters:
        * servo - {'fwd', 'aft'} which servo command applies to
        * coast - {True, False} whether to set coast or cancel coast.
        '''
        service_name = "/sentry/actuators/servo_{}/coast_cmd".format(servo)

        try:
            rospy.wait_for_service(service_name, 1.0)
        except rospy.ROSException as e:
            errmsg = "Unable to connect to service: {}".format(service_name)
            rospy.logerr(errmsg)
            self.popUpError(errmsg)
            return

        proxy = rospy.ServiceProxy(service_name, sentry_msgs.srv.ServoCoastCmd)
        req = sentry_msgs.srv.ServoCoastCmdRequest()
        req.coast = coast
        rospy.loginfo("sending coast command ({}) to service {}".format(coast, service_name))
        try:
            resp = proxy(req)
            if not resp:
                errmsg = "Unable to send coast command ({}) to service {}".format(coast, service_name)
                rospy.logerr(errmsg)
                self.popUpError(errmsg)
        except rospy.service.ServiceException as e:
            errmsg = "Coast service reports failure!"
            rospy.logerr(errmsg)
            self.popUpError(errmsg)

    def indexButtonPushed(self, servo):
        '''
        Handles the new-style index command.

        Parameters:
        * servo - {'fwd', 'aft'} which servo command applies to
        '''
        service_name = "/sentry/actuators/servo_{}/goto_index_cmd".format(servo)

        try:
            rospy.wait_for_service(service_name, 1.0)
        except rospy.ROSException as e:
            errmsg = "Unable to connect to service: {}".format(service_name)
            rospy.logerr(errmsg)
            self.popUpError(errmsg)
            return

        proxy = rospy.ServiceProxy(service_name, sentry_msgs.srv.ServoHomeCmd)
        req = sentry_msgs.srv.ServoHomeCmdRequest()
        try:
            resp = proxy(req)
            if not resp:
                errmsg = "Unable to send Go Home command to service {}".format(service_name)
                rospy.logerr(errmsg)
                self.popUpError(errmsg)
            else:
                status_label = self.index_status_labels[servo]
                status_label.setText(self.index_status_format.format("pending"))
                style = "QLabel { background-color : lightgray; color : black; }"
                status_label.setStyleSheet(style)
        except rospy.ROSException as e:
            errmsg = "Homing service reports failure!"
            rospy.logerr(errmsg)
            self.popUpError(errmsg)


    def popUpError(self, error_message):
        '''
        Generate pop up dialog with "OK" button for any errors encountered
        '''
        box = QMessageBox()
        box.setText(error_message)
        box.setStandardButtons(QMessageBox.Ok)
        box.exec_()
