from sentry_msgs.msg import HarmonicServoState
from sentry_msgs.srv import (ServoCoastCmd, ServoHomeCmd, ServoIndexCmd)
from ds_core_msgs.msg import Abort
from ds_actuator_msgs.msg import ServoCmd

import os
import time
import yaml
import rospy
import rospkg
import rosbag

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtCore import (QMetaObject, pyqtSlot, pyqtSignal, QTimer)
from python_qt_binding.QtWidgets import (QWidget, QDialog, QLCDNumber, QFileDialog, QMessageBox)

from math import pi
class OpenReplayDialog(QDialog):

    def __init__(self, parent=None):
        super(OpenReplayDialog, self).__init__(parent)
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_harmonic_servo'), 'resources', 'OpenReplayFile.ui')
        loadUi(ui_file, self)

    @pyqtSlot()
    def onOpenButtonClicked(self):
        (bagfile, _) = QFileDialog.getOpenFileName(self, "Choose ROS bag file", "", "ROS (*.bag);;Any (*.*)")
        if bagfile:
            self.loadBagFile(bagfile)

    @pyqtSlot(str)
    def loadBagFile(self, filename):
        rospy.loginfo("Loading replay from: %s", filename)
        bag = rosbag.Bag(filename, 'r')
        servo_topics = []
        for conn in bag._get_connections():
            if conn.datatype == 'ds_actuator_msgs/ServoCmd' and conn.topic not in servo_topics:
                servo_topics.append(conn.topic)

        if len(servo_topics) == 0:
            QMessageBox.critical(self, "Invalid servo bag file", "No 'ds_actuator_msgs/ServoCmd' message topics found in %s" % (filename,))
            return

        self.topicCombo.clear()
        for topic in sorted(servo_topics):
            self.topicCombo.addItem(topic)

        self.replayFileName.setText(filename)

class ReplayWidget(QWidget):

    setAngleDegrees = pyqtSignal(int)
    replayEnabledChanged = pyqtSignal(bool)

    def __init__(self, parent=None):
        super(ReplayWidget, self).__init__(parent)
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_harmonic_servo'), 'resources', 'ReplayWidget.ui')
        rospy.logwarn("Loading replay widget ui")
        loadUi(ui_file, self)
        self.setVisible(False)

        self.__bag = None
        self.__topic = None
        self.__messages = None
        self.__pending_message = None
        self.__bag_start_timestamp = None
        self.__bag_duration = None
        self.__enabled = False

        timer = QTimer(self)
        timer.setSingleShot(True)
        timer.timeout.connect(self.onTimerTimeout)
        self.__timer = timer
        self.speedLabel.setText("Speed: x%d" % (self.speedDial.value(),))
        

    @pyqtSlot(str, str)
    def setReplayFile(self, filename, topic):
        self.replayFileName.setText(filename)
        self.originalTopicName.setText(topic)
        self.playButton.setText("&Play")

        rospy.loginfo("Reading messages from topic: %s in file: %s", topic, filename)
        self.__bag = rosbag.Bag(filename, 'r')
        self.__topic = topic
        self.__messages = self.__bag.read_messages(topics=[topic])
        self.__pending_message = None
        info = yaml.load(self.__bag._get_yaml_info())
        self.__bag_start_timestamp = info['start']
        self.__bag_duration = info['duration']
        self.timeRemaining.setText("")

        self.setVisible(True)

    @pyqtSlot()
    def onTimerTimeout(self):
        if self.__pending_message is None:
            if self.__messages is None:
                raise RuntimeError("No messages loaded.  Cannot send next message.")
            self.__pending_message = next(self.__messages)

        ((topic, message, timestamp), self.__pending_message) = (self.__pending_message, None)
        self.setAngleDegrees.emit(int(message.cmd_radians * 180.0 / pi))

        elapsed = timestamp.to_sec() - self.__bag_start_timestamp

        progress = int(elapsed / self.__bag_duration * 100.0)
        self.progressBar.setValue(progress)

        # Replay time remaining is modified by the speed dial value
        time_remaining = (self.__bag_duration - elapsed) / self.speedDial.value()

        days = int(time_remaining / (24 * 3600))
        remainder = (time_remaining - (days * 24 * 3600))
        hours = int(remainder / 3600)
        remainder = remainder - hours * 3600
        minutes = int(remainder / 60)
        remainder = remainder - minutes * 60
        seconds = int(remainder)

        if days > 0:
            time_remaining = "%ddays %02dhours %02dmins %02dsecs" % (days, hours, minutes, seconds)
        elif hours > 0:
            time_remaining = "%02dhours %02dmins %02dsecs" % (hours, minutes, seconds)
        elif minutes > 0:
            time_remaining = "%02dmins %02dsecs" % (minutes, seconds)
        else:
            time_remaining = "%02dsecs" % (seconds,)

        self.timeRemaining.setText(time_remaining)


        # Load the next message
        try:
            self.__pending_message = next(self.__messages)
            delay = (self.__pending_message.timestamp - timestamp).to_sec() / self.speedDial.value()
        except StopIteration:
            # End of bag file.  If continuous mode is enabled we need to restart from the beginning
            rospy.logwarn("Reached end of replay file")

            self.__messages = self.__bag.read_messages(topics=[self.__topic])
            if self.repeatCheckBox.isChecked():
                rospy.logwarn("Restarting replay from beginning")
                # Recreate message generator and set the delay to 0 to immediately re-trigger and re-send
                self.__pending_message = next(self.__messages)
                delay = 0
            else:
                rospy.logwarn("Halting replay")
                self.__pending_message = None
                self.replayEnabledChanged.emit(False)
                self.playButton.setText("&Play")
                
                return

        self.__timer.setInterval(int(delay * 1000))
        self.__timer.start()

    @pyqtSlot()
    def onPlayButtonClicked(self):
        if self.__timer.isActive():
            self.__timer.stop()
            self.__enabled = False
            self.playButton.setText("&Play")
            self.replayEnabledChanged.emit(False)
        else:
            self.playButton.setText("&Pause")
            self.__enabled = True
            self.replayEnabledChanged.emit(True)
            self.onTimerTimeout()

    @pyqtSlot(int)
    def onSpeedChanged(self, speed):
        self.speedLabel.setText("Speed: x%d" % (speed,))


class BenchTestWidget(QWidget):
    goHomeClicked = pyqtSignal()
    setHomeClicked = pyqtSignal()
    coastModeChanged = pyqtSignal(bool)
    setAngleChanged = pyqtSignal(int)
    replayEnabledChanged = pyqtSignal(bool)
    abortEnableClicked = pyqtSignal()

    def __init__(self, angle_min_deg, angle_max_deg, fin_direction, parent=None):
        super(BenchTestWidget, self).__init__(parent)
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_harmonic_servo'), 'resources', 'BenchTest.ui')
        loadUi(ui_file, self)
        self.goHomeButton.setEnabled(True)
        self.angleSlider.setEnabled(True)

        self.__home_is_set = True
        self.__coast_enabled = False
        self.__replay_enabled = False

        rospy.logwarn("On startup angle max is %s and angle min is %s", angle_max_deg, angle_min_deg)
        self.angle_max_deg = angle_max_deg
        self.angle_min_deg = angle_min_deg
        self.fin_direction = fin_direction
        if (angle_max_deg < angle_min_deg): 
           self.angleSlider.setRange(int(angle_max_deg), int(angle_min_deg))
           
           self.angleSlider.setInvertedAppearance(True)
           rospy.logwarn("Inverting angle slider")
        else:
           self.angleSlider.setRange(int(angle_min_deg), int(angle_max_deg))
           rospy.logwarn("Not inverting angle slider")

        self.angleSlider.setValue(angle_min_deg)
        self.angleSlider.sliderMoved.connect(self.setAngleChanged)

        self.replayWidget.setAngleDegrees.connect(self.setAngleChanged)
        self.replayWidget.replayEnabledChanged.connect(self.onReplayEnabledChanged)

    @pyqtSlot()
    def onModeClicked(self):
        if self.modeButton.text() == "&Enable Break":
            self.coastModeChanged.emit(False)
            self.__coast_enabled = False
        elif self.modeButton.text() == "&Enable Coast":
            self.coastModeChanged.emit(True)
            self.__coast_enabled = True
        else:
            raise ValueError("Unknown mode button text: %s" % (self.modeButton.text()))

    @pyqtSlot()
    def onSetHomeClicked(self):
        self.setHomeClicked.emit()

    @pyqtSlot()
    def onGoHomeClicked(self):
        self.goHomeClicked.emit()

    @pyqtSlot()
    def onAbortEnableClicked(self):
        self.abortEnableClicked.emit()

    def updateButtonState(self):
        if self.__replay_enabled:
            # Replay is enabled
            self.goHomeButton.setEnabled(False)
            self.setHomeButton.setEnabled(False)
            self.angleSlider.setEnabled(False)
            self.loadReplayButton.setEnabled(False)
        elif self.__coast_enabled:
            # Coast mode enabled
            self.goHomeButton.setEnabled(False)
            self.setHomeButton.setEnabled(False)
            self.angleSlider.setEnabled(False)
            self.loadReplayButton.setEnabled(False)
        elif not self.__coast_enabled and not self.__home_is_set:
            self.goHomeButton.setEnabled(True)
            self.setHomeButton.setEnabled(True)
            self.angleSlider.setEnabled(False)
            self.loadReplayButton.setEnabled(False)
        elif not self.__coast_enabled and self.__home_is_set:
            # Coast mode disabled and home position has been set
            self.goHomeButton.setEnabled(True)
            self.setHomeButton.setEnabled(True)
            self.angleSlider.setEnabled(True)
            self.loadReplayButton.setEnabled(True)
        else:
            raise RuntimeError("BUG:  Unhandled servo state: replay_enabled: %s, coast_enabled: %s, home_set: %s" %(self.__replay_enabled, self.__coast_enabled, self.__home_is_set,))

    @pyqtSlot(bool)
    def setCoastEnabled(self, enabled):
        # this reverse logic due to the 'brake_activated' field of 'CoplyState'
        # being set TRUE when the brake is NOT ACTIVE.
        # TODO:  Confirm whether or not this flag needs to be fixed
        if not enabled:
            self.modeButton.setText("&Enable Break")
            self.__coast_enabled = True
            self.servoCoastState.setText("servo is coasting")
            self.__home_is_set = True
        else:
            self.modeButton.setText("&Enable Coast")
            self.__coast_enabled = False
            self.servoCoastState.setText("servo brake is set")
            self.modeButton.setEnabled(True)

        self.updateButtonState()

    @pyqtSlot(bool)
    def onReplayEnabledChanged(self, enabled):
        rospy.logerr("onReplayEnabledChanged: %s", enabled)
        if enabled:
            self.angleSlider.setEnabled(False)
            self.modeButton.setEnabled(False)
            self.setHomeButton.setEnabled(False)
            self.goHomeButton.setEnabled(False)
            self.setCoastEnabled(True)
            #rospy.logdebug("DISABLING ALL BUTTONS")
        else:
            # Again, need to flip the logic value here due to the 'brake_activated'
            # state inversion bug.  See note in setCoastEnabled
            rospy.logwarn("SETTING COAST ENABLE")
            self.setCoastEnabled(not self.__coast_enabled)

        self.__replay_enabled = enabled

    @pyqtSlot(int)
    def onEncoderCountChanged(self, count):
        self.countNumber.setDecMode()
        self.countNumber.display(str(count))


    @pyqtSlot(float)
    def onServoAngleChanged(self, radians):
        degrees = 180.0 * radians / pi
        #if self.fin_direction is 1:
        #tj not too positive why, but this needs to be flipped to appear correctly
        degrees = -degrees
        self.countNumber.setDecMode()
        self.angleNumber.display("%.2f\'" % (degrees,))
        if not self.angleSlider.isSliderDown():
            self.angleSlider.setValue(int(degrees))

    @pyqtSlot(float)
    def onCommandedServoAngleChanged(self, radians):
        degrees = 180.0 * radians / pi
        self.commandedAngleNumber.display("%.2f\'" % (degrees,))
        if not self.angleSlider.isSliderDown():
            self.angleSlider.setValue(int(degrees))

    @pyqtSlot()
    def setCoastCommandConfirmed(self):
        self.goHomeButton.setEnabled(False)
        self.angleSlider.setEnabled(True)

    @pyqtSlot()
    def onHomeCommandConfirmed(self):
        self.__home_is_set = True

    @pyqtSlot(str, str)
    def onLoadReplaySuccess(self, replay_file, topic_name):
        rospy.logdebug("loading servo topic %s from %s", topic_name, replay_file)

    @pyqtSlot()
    def onLoadReplayClicked(self):
        dialog = OpenReplayDialog(self)
        if not dialog.exec_():
            return

        self.replayWidget.setReplayFile(dialog.replayFileName.text(), dialog.topicCombo.currentText())
        self.replayWidget.setVisible(True)


class BenchTestPlugin(Plugin):

    encoderCountChanged = pyqtSignal(int)
    servoAngleChanged = pyqtSignal(float)
    commandedServoAngleChanged = pyqtSignal(float)
    coastEnabledChanged = pyqtSignal(bool)
    setHomeCommandConfirmed = pyqtSignal()

    def __init__(self, context):
        super(BenchTestPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('BenchTestPlugin')
        # Process standalone plugin command-line arguments
        # from argparse import ArgumentParser
        # parser = ArgumentParser()
        # # Add argument(s) to the parser.
        # parser.add_argument("-q", "--quiet", action="store_true",
        #               dest="quiet",
        #               help="Put plugin in silent mode")
        # args, unknowns = parser.parse_known_args(context.argv())
        # if not args.quiet:
        #     print 'arguments: ', args
        #     print 'unknowns: ', unknowns

        # Create QWidget
        # self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        # ui_file = os.path.join(rospkg.RosPack().get_path('rqt_harmonic_servo'), 'resources', 'BenchTest.ui')
        # Extend the widget with all attributes and children from UI file
        # loadUi(ui_file, self._widget)
        # Give QObjects reasonable names

        max_goal_counts = rospy.get_param('/harmonic_servo/max_goal_counts')
        min_goal_counts = rospy.get_param('/harmonic_servo/min_goal_counts')

        fin_direction = rospy.get_param('/harmonic_servo/encoder_fin_direction')
        encoder_counts = rospy.get_param('/harmonic_servo/encoder_counts')
        encoder_center = rospy.get_param('/harmonic_servo/encoder_center')
        gear_ratio = rospy.get_param('/harmonic_servo/gear_ratio')

        
        #this logic does not seem to work
        #if fin_direction < 0:
        #   encoder_center = -1 * encoder_center
        
        #this evaluates to 4096 * 1.33333 / 6.28 == 869
        counts_per_radian = encoder_counts * gear_ratio / (2 * pi)


        angle_min_deg = self.radians_from_counts(min_goal_counts, fin_direction, encoder_center, counts_per_radian) * 180.0 / pi
        angle_max_deg = self.radians_from_counts(max_goal_counts, fin_direction, encoder_center, counts_per_radian) * 180.0 / pi

        #angle_min_deg = -40
        #angle_max_deg = 80

        rospy.logwarn("CALCULATED min to  %s and max to  %s", angle_min_deg, angle_max_deg)


        self.ui = BenchTestWidget(angle_min_deg, angle_max_deg, fin_direction)
        self.ui.setObjectName('ui')
        self.ui.coastModeChanged.connect(self.onCoastModeChanged)
        self.ui.goHomeClicked.connect(self.onGoHomeClicked)
        self.ui.abortEnableClicked.connect(self.onAbortEnabledClicked)
        self.ui.setHomeClicked.connect(self.onSetHomeClicked)
        self.encoderCountChanged.connect(self.ui.onEncoderCountChanged)
        self.servoAngleChanged.connect(self.ui.onServoAngleChanged)
        self.commandedServoAngleChanged.connect(self.ui.onCommandedServoAngleChanged)
        self.coastEnabledChanged.connect(self.ui.setCoastEnabled)
        self.setHomeCommandConfirmed.connect(self.ui.onHomeCommandConfirmed)

        self.ui.setAngleChanged.connect(self.onSetAngleChanged)

        self.subscriptions = {
            'harmonic_servo_state': rospy.Subscriber("/harmonic_servo/sentry_servo_state", HarmonicServoState, self.onHarmonicServoState)
        }

        self.publishers = {
            'commanded_angle': rospy.Publisher("/harmonic_servo/cmd", ServoCmd, queue_size=10),
            'abort_status': rospy.Publisher("/harmonic_servo/abort", Abort, queue_size=10)
        }

        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        # if context.serial_number() > 1:
        #     self.setWindowTitle(self.ui.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self.ui)

        # Enter brake mode on startup
        self.onCoastModeChanged(False)



    def shutdown_plugin(self):
        while len(self.subscriptions) > 0:
            (_, sub) = self.subscriptions.popitem()
            sub.unregister()

        while len(self.publishers) > 0:
            (_, pub) = self.publishers.popitem()
            pub.unregister()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
        pass

    def radians_from_counts(self, counts, fin_direction, center, cpr):
        #fins fwd
        # example min 4000, 1, 2110, 869   . 2.17 rads = 124.34
        #example max 100, 1, 2110, 869 == -2.31 == -132deg

        #fins bkwd
        #4000, -1, -2110, 869
        return -fin_direction * (counts - center) / cpr


    @pyqtSlot(bool)
    def onCoastModeChanged(self, enabled):

        rospy.wait_for_service('/harmonic_servo/coast_cmd')
        set_coast = rospy.ServiceProxy('/harmonic_servo/coast_cmd', ServoCoastCmd)
        try:
            response = set_coast(enabled)
        except rospy.ServiceException as e:
            rospy.logerr("Unable to set harmonic servo to coast: %r", str(e))

    @pyqtSlot()
    def onSetHomeClicked(self):

        rospy.wait_for_service('/harmonic_servo/index_cmd')
        set_home = rospy.ServiceProxy('/harmonic_servo/index_cmd', ServoIndexCmd)
        rospy.logwarn("Setting home position")
        try:
            response = set_home()
            if response:
                self.setHomeCommandConfirmed.emit()
        except rospy.ServiceException as e:
            rospy.logerr("Unable to set harmonic servo home: %r", str(e))

    @pyqtSlot()
    def onGoHomeClicked(self):
        goto_home = rospy.ServiceProxy('/harmonic_servo/goto_index_cmd', ServoHomeCmd)
        rospy.logwarn("Returning to home position")
        try:
            response = goto_home()
        except rospy.ServiceException as e:
            rospy.logerr("Unable to set harmonic servo to home position: %r", str(e))

    @pyqtSlot()
    def onAbortEnabledClicked(self):
        self.publishers['abort_status'].publish(enable=True)



    @pyqtSlot(int)
    def onSetAngleChanged(self, degrees):
        radians = degrees * pi / 180.0
        self.publishers['commanded_angle'].publish(cmd_radians=radians)
        self.commandedServoAngleChanged.emit(radians)
        rospy.logwarn("Angle changed. Degrees:  %d Radians  %d", degrees, radians)


    def onHarmonicServoState(self, state):
        self.encoderCountChanged.emit(state.encoder_counts)
        self.servoAngleChanged.emit(state.servo_state.actual_radians)
        self.coastEnabledChanged.emit(not state.event_status.brake_activated)
