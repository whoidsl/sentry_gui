/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jogarcia on 11/23/20.
//

#include "../include/ds_widget/trigger_board.h"

using namespace ds_widget;

Trigger::Trigger(ros::ServiceClient* be_cl, ros::ServiceClient* ci_cl, ros::ServiceClient* pc_cl, QWidget* parent)
  : QWidget(parent)
{
  
  param_check_cl = pc_cl;
  board_enablement_cl = be_cl;
  config_install_cl = ci_cl;


  sonars_on_btn = new DsButton("Activate Sonars", "Start pinging all sonars?", this);
  sonars_off_btn = new DsButton("Stop Sonars", "Stop all sonar pinging?", this);
  //validate_settings_btn = new DsButton("Validate Sonars Config", "", this);
  //install_config_btn = new DsButton("Install Sonars Config", "", this);

  QGridLayout* grid = new QGridLayout();
  enablement_box = new QGroupBox();
  auto enablement_box_grid = new QGridLayout();

  enablement_box_grid->addWidget(sonars_on_btn, 0, 0);
  enablement_box_grid->addWidget(sonars_off_btn, 0, 1);
  enablement_box->setLayout(enablement_box_grid);

  grid->addWidget(enablement_box, 0, 0);
  setLayout(grid);

  connect(sonars_on_btn, SIGNAL(confirmed()), this, SLOT(enable_board()));
  connect(sonars_off_btn, SIGNAL(confirmed()), this, SLOT(disable_board()));
}

void Trigger::enable_board()
{
  std_srvs::SetBool cmd;
  cmd.request.data = true;
  if (board_enablement_cl->call(cmd))
  {
    ROS_INFO_STREAM("Enabled the trigger board!");
  }
  else
  {
    ROS_ERROR_STREAM("Unable to enable the board");
  }
  return;
}

void Trigger::disable_board()
{
  std_srvs::SetBool cmd;
  cmd.request.data = false;

  if (board_enablement_cl->call(cmd))
  {
    ROS_INFO_STREAM("Disabled the trigger board!");
  }
  else
  {
    ROS_ERROR_STREAM("Unable to disable the board");
  }
  return;
}

// Feature coming, will have to figure out what the user experience on sentrysitter will be like for this (i.e., how will we prompt for and accept parameter changes).
void Trigger::install_configuration()
{
  return;
}

// Feature coming, we'll need to figure out if the user can specify a yaml file here, input a yaml-string, etc. 
// Also, what will happen if the actual board configuration does not match the user-specified reference?
void Trigger::verify_configuration()
{
  return;
}

