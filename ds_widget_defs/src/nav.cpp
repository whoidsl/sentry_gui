/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/22/18.
//

#include "../include/ds_widget/nav.h"

using namespace ds_widget;

DvlRanges::DvlRanges(QWidget* parent) : QWidget(parent)
{
  for (int i = 0; i < 4; i++)
  {
    ranges[i] = new QLabel(this);
  }
  QLabel* xlbl = new QLabel(">Dvl Beams<", this);
  QGridLayout* grid = new QGridLayout(this);
  grid->addWidget(ranges[0], 0, 0);
  grid->addWidget(ranges[1], 0, 2);
  grid->addWidget(xlbl, 1, 0, 1, 3);
  grid->addWidget(ranges[2], 2, 0);
  grid->addWidget(ranges[3], 2, 2);
  setLayout(grid);
}

void DvlRanges::update(ds_sensor_msgs::Dvl msg)
{
  auto r = msg.range;
  for (int i = 0; i < 4; i++)
  {
    ranges[i]->setText(QString::number(ds_util::float_round(r[i], 2)));
  }
}

Nav::Nav(ros::ServiceClient* _cl1, ros::ServiceClient* _cl2, QWidget* parent)
  : QWidget(parent)
  , cl1(_cl1)
  , cl2(_cl2)
  , phins_needs_update(false)
  , paro_needs_update(false)
  , dvl_needs_update(false)
  , phins_clk(0)
  , paro_clk(0)
  , dvl_clk(0)
{
  setup_heading();
  setup_velocities();
  setup_ranges();
  setup_timouts();
  QGridLayout* grid = new QGridLayout(this);
  grid->addWidget(heading_box, 0, 0);
  grid->addWidget(velocities_box, 0, 1);
  grid->addWidget(ranges_box, 0, 2);
  grid->addWidget(timeout_box, 0, 3);
  setLayout(grid);
}

void Nav::setup_heading()
{
  heading_box = new QGroupBox("Gyro", this);
  QGridLayout* grid = new QGridLayout(heading_box);
  heading = new LabelPair("Heading", this);
  phins_cal_btn = new DsButton("Calibrate PHINS", "Run Phins Cal?", this);
  roll = new LabelPair("Roll", this);
  pitch = new LabelPair("Pitch", this);
  grid->addWidget(heading, 0, 0, 1, 3);
  grid->addWidget(roll, 1, 0, 1, 3);
  grid->addWidget(pitch, 2, 0, 1, 3);
  grid->addWidget(phins_cal_btn, 3, 0, 1, 3);
  heading_box->setLayout(grid);
  connect(phins_cal_btn, SIGNAL(confirmed()), this, SLOT(cal_phins()));
}

void Nav::setup_velocities()
{
  velocities_box = new QGroupBox("Velocities", this);
  QGridLayout* grid = new QGridLayout(velocities_box);
  QString v[3] = { QString("x"), QString("y"), QString("z") };
  for (int i = 0; i < 3; i++)
  {
    velocities[i] = new LabelPair("V" + v[i], this);
    grid->addWidget(velocities[i], i, 0, 1, 3);
  }
  velocities_box->setLayout(grid);
}

void Nav::setup_ranges()
{
  ranges_box = new QGroupBox("Ranges", this);
  QGridLayout* grid = new QGridLayout(ranges_box);
  depth = new LabelPair("Depth", this);
  zero_paro_btn = new DsButton("Zero Paro", "Zero Paro?", this);
  altitude = new LabelPair("Alt", this);
  dvl = new DvlRanges(this);
  grid->addWidget(depth, 0, 0);
  grid->addWidget(zero_paro_btn, 1, 0);
  grid->addWidget(altitude, 2, 0);
  grid->addWidget(dvl, 3, 0);
  ranges_box->setLayout(grid);
  connect(zero_paro_btn, SIGNAL(confirmed()), this, SLOT(zero_paro()));
}

void Nav::setup_timouts()
{
  timeout_box = new QGroupBox("Timeouts", this);
  QGridLayout* grid = new QGridLayout(timeout_box);
  dvl_timeout = new LabelPair("Dvl", this);
  paro_timeout = new LabelPair("Paro", this);
  phins_timeout = new LabelPair("Phins", this);
  grid->addWidget(dvl_timeout, 0, 0);
  grid->addWidget(paro_timeout, 1, 0);
  grid->addWidget(phins_timeout, 2, 0);
  timeout_box->setLayout(grid);
}

void Nav::cal_phins()
{
  auto cmd = ds_core_msgs::VoidCmd{};
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("PHINS Cal successful");
  }
  else
  {
   ROS_ERROR_STREAM("PHINS cal failed");
  }
  return;
}
void Nav::zero_paro()
{
  auto cmd = ds_core_msgs::VoidCmd{};
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Zero Paros successful");
  }
  else
  {
    ROS_ERROR_STREAM("Zero Paros failed");
  }
  return;
}

void Nav::update_paro(ds_sensor_msgs::DepthPressure in_paro)
{
  std::unique_lock<std::mutex> lck(paro_mutex);
  paro_msg = in_paro;
  paro_clk = 0;
  paro_needs_update = true;
}

void Nav::update_dvl(ds_sensor_msgs::Dvl in_dvl)
{
  std::unique_lock<std::mutex> lck(dvl_mutex);
  dvl_msg = in_dvl;
  dvl_clk = 0;
  dvl_needs_update = true;
}

void Nav::update_phins(ds_sensor_msgs::Gyro in_phins)
{
  std::unique_lock<std::mutex> lck(phins_mutex);
  phins_msg = in_phins;
  phins_clk = 0;
  phins_needs_update = true;
}

void Nav::update_dvl_view()
{
  std::unique_lock<std::mutex> lck(dvl_mutex);
  velocities[0]->update(QString::number(ds_util::float_round(dvl_msg.velocity.x, 2)));
  velocities[1]->update(QString::number(ds_util::float_round(dvl_msg.velocity.y, 2)));
  velocities[2]->update(QString::number(ds_util::float_round(dvl_msg.velocity.z, 2)));
  dvl->update(dvl_msg);
  altitude->update(QString::number(ds_util::float_round(dvl_msg.altitude, 2)));
  velocities_box->setStyleSheet("QGroupBox {  border: 1px solid green;}");
  dvl_timeout->update(QString::number(dvl_clk));
  dvl_needs_update = false;
}
void Nav::update_phins_view()
{
  std::unique_lock<std::mutex> lck(phins_mutex);
  heading->update(QString::number(ds_util::float_round(phins_msg.heading * 180.0 / 3.141, 1)));
  roll->update(QString::number(ds_util::float_round(phins_msg.roll * 180.0 / 3.141, 2)));
  pitch->update(QString::number(ds_util::float_round(phins_msg.pitch * 180.0 / 3.141, 2)));
  heading_box->setStyleSheet("QGroupBox {  border: 1px solid green;}");
  //    compass->update(phins_msg.heading);
  phins_timeout->update(QString::number(phins_clk));
  phins_needs_update = false;
}

void Nav::update_paro_view()
{
  std::unique_lock<std::mutex> lck(paro_mutex);
  depth->update(QString::number(ds_util::float_round(paro_msg.depth, 2)));
  ranges_box->setStyleSheet("QGroupBox {  border: 1px solid green;}");
  paro_timeout->update(QString::number(paro_clk));
  paro_needs_update = false;
}

void Nav::tick()
{
  if (paro_needs_update)
  {
    update_paro_view();
  }
  else
  {
    paro_timeout->update(QString::number(paro_clk));
    if (paro_clk > 30)
    {
      ranges_box->setStyleSheet("QGroupBox {  border: 1px solid yellow;}");
    }
  }
  if (dvl_needs_update)
  {
    update_dvl_view();
  }
  else
  {
    dvl_timeout->update(QString::number(dvl_clk));
    if (dvl_clk > 30)
    {
      velocities_box->setStyleSheet("QGroupBox {  border: 1px solid yellow;}");
    }
  }
  if (phins_needs_update)
  {
    update_phins_view();
  }
  else
  {
    phins_timeout->update(QString::number(phins_clk));
    if (phins_clk > 30)
    {
      heading_box->setStyleSheet("QGroupBox {  border: 1px solid yellow;}");
    }
  }
  dvl_clk += 1;
  phins_clk += 1;
  paro_clk += 1;
}
