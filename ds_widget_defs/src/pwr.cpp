/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/5/18.
//

#include "../include/ds_widget/pwr.h"
#include <QFrame>

using namespace ds_widget;

PwrGroup::PwrGroup(QString _name, QWidget* _parent)
  : QGroupBox(_name, _parent), name(_name), max_height(2), num_btns(0), width(.25)
{
  grid = new QGridLayout(this);
}

void PwrGroup::add_pwr_btn(PwrButton* btn_ptr)
{
  int row = num_btns % max_height;
  int column = num_btns / max_height;
  grid->addWidget(btn_ptr, row, column);
  num_btns += 1;
  width = column + 1;
  height = (num_btns < 4 ? num_btns : 4);
}

Pwr::Pwr(ros::ServiceClient* _cl, std::string _param, QWidget* _parent)
  : QWidget(_parent), cl(_cl), param(_param), timesince(0), view_needs_update(false)
{
  ROS_INFO_STREAM("pwr grid made!");
  make_grid();
}

void Pwr::make_grid()
{
  grid = new QGridLayout();

  QFrame* hline;
  hline = new QFrame();
  hline->setFrameShape(QFrame::HLine);
  hline->setFrameShadow(QFrame::Sunken);

  QFrame* vline;
  vline = new QFrame();
  vline->setFrameShape(QFrame::VLine);
  vline->setFrameShadow(QFrame::Sunken);

  std::string name, group;
  bool confirm_on, confirm_off;

  for (int i = 0; i < 0xFF; i++)
  {
    name = grd_util::get_name(i);
    confirm_on = grd_util::get_confirm_on(i);
    confirm_off = grd_util::get_confirm_off(i);
    group = grd_util::get_group(i);
    if (name != "")
    {
      new_btn(QString::fromStdString(name), i, confirm_on, confirm_off, group);
    }
  }
  int current_column = 0;
  int current_row = 0;
  int max_btn_width = 6;
  //    for ( const auto &myPair : myMap ) {
  //        std::cout << myPair.first << "\n";
  //    }
  for (const auto& grp : groups)
  {
    int grp_width = grp.second->get_width();
    int grp_height = grp.second->get_height();
    if (current_column > max_btn_width)
    {
      current_row += 4;
      current_column = 0;
    }
    ROS_INFO_STREAM("row " << current_row << " column " << current_column << " height " << grp_height << " width "
                           << grp_width);
    grid->addWidget(grp.second, current_row, current_column, 4, grp_width);
    current_column += grp_width;
  }

  setLayout(grid);
}

void Pwr::new_btn(QString _name, int _address, bool c_on, bool c_off, std::string _group)
{
  PwrButton* new_btn = new PwrButton(_name, _address, cl, c_on, c_off, this);
  connect(new_btn, SIGNAL(clicked()), new_btn, SLOT(pwr_click()));

  //    ROS_INFO_STREAM(_group);

  if (!groups.count(_group))
  {
    groups[_group] = new PwrGroup(QString::fromStdString(_group), this);
  }
  groups[_group]->add_pwr_btn(new_btn);

  btns.push_back(new_btn);
}

void Pwr::update(ds_hotel_msgs::PWR in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);

  msg = in_msg;
  timesince = 0;
  view_needs_update = true;
}

void Pwr::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);

  int size_msg = msg.devices.size();
  int size_btn = btns.size();

  if (size_msg != size_btn)
  {
    ROS_ERROR_STREAM("Size mismatch between message and buttons!");
    return;
  }

  for (int i = 0; i < size_btn; i++)
  {
    if (msg.devices[i].name == btns[i]->get_name().toStdString())
    {
      btns[i]->set_color(msg.devices[i].is_on);
    }
  }
  view_needs_update = false;
}

void Pwr::tick()
{
  timesince += 1;
  if (timesince > 20)
  {
    for (int i = 0; i < btns.size(); i++)
    {
      btns[i]->makeYellow();
    }
  }
  if (view_needs_update)
  {
    update_view();
  }
}

std::string Pwr::getParam()
{
  return param;
}

void Pwr::setParam(std::string _param)
{
  param = _param;
  while (btns.size() > 0)
  {
    btns.pop_back();
  }
  make_grid();
}
