/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 10/25/20.
//

#include "ds_widget/syprid.h"

namespace ds_widget{

Syprid::Syprid(ros::ServiceClient* _cmd_cl
    , QWidget* parent)
    : QWidget(parent)
    , cmd_cl(_cmd_cl)
{
  ROS_INFO_STREAM("Start Syprid Layout");
  auto grid = new QGridLayout();
  auto stat = new QGroupBox("Status");
  auto stat_grid = new QGridLayout();
  pkz_conn_lbl = new QLabel("");
  pkz_dcam_lbl = new QLabel("");
  dcam_port_state_lbl = new QLabel("");
  dcam_stbd_state_lbl = new QLabel("");
  dcam_persistence_state_lbl = new QLabel("");
  dcam_port_state_desired_lbl = new QLabel("");
  dcam_stbd_state_desired_lbl = new QLabel("");
  dcam_persistence_state_desired_lbl = new QLabel("");
  elmo_port_rpm_latest_lbl = new QLabel("");
  elmo_port_rpm_avg_lbl = new QLabel("");
  elmo_stbd_rpm_latest_lbl = new QLabel("");
  elmo_stbd_rpm_avg_lbl = new QLabel("");
  elmo_port_temp_lbl = new QLabel("");
  elmo_stbd_temp_lbl = new QLabel("");
  flow_sensor_state_lbl = new QLabel("");
  flow_sensor_state_desired_lbl = new QLabel("");
  stat_grid->addWidget(pkz_conn_lbl, 0,0);
  stat_grid->addWidget(pkz_dcam_lbl, 1, 0);
  stat_grid->addWidget(dcam_port_state_lbl, 2, 0);
  stat_grid->addWidget(dcam_port_state_desired_lbl, 3, 0);
  stat_grid->addWidget(elmo_port_rpm_latest_lbl, 4, 0);
  stat_grid->addWidget(elmo_port_rpm_avg_lbl, 5,0);
  stat_grid->addWidget(elmo_port_temp_lbl, 6, 0);
  stat_grid->addWidget(dcam_stbd_state_lbl, 2, 1);
  stat_grid->addWidget(dcam_stbd_state_desired_lbl, 3, 1);
  stat_grid->addWidget(dcam_persistence_state_lbl, 4, 1);
  stat_grid->addWidget(elmo_stbd_rpm_latest_lbl, 5, 1);
  stat_grid->addWidget(elmo_stbd_rpm_avg_lbl, 6, 1);
  stat_grid->addWidget(elmo_stbd_temp_lbl, 7, 1);
  stat_grid->addWidget(flow_sensor_state_lbl, 8, 1);
  stat_grid->addWidget(flow_sensor_state_desired_lbl, 9, 1);
  stat->setLayout(stat_grid);
  ROS_INFO_STREAM("... done Syprid status layout");
  
  auto cmds = new QGroupBox("Commands");
  auto cmd_grid = new QGridLayout();
  cmd_lbl = new QLabel("");
  cmd_combo = new QComboBox();
  torque_1_edit = new QLineEdit("");
  torque_1_edit->setPlaceholderText("Torque 1 Value");
  torque_2_edit = new QLineEdit("");
  torque_2_edit->setPlaceholderText("Torque 2 Value");
  cmd_combo->addItem("Set PKZ Command", 0);
  cmd_combo->addItem("PKZ START ALL", sentry_msgs::SypridCmd::Request::PKZ_START_ALL);
  cmd_combo->addItem("PKZ STOP ALL", sentry_msgs::SypridCmd::Request::PKZ_STOP_ALL);
  cmd_combo->addItem("PKZ START PORT", sentry_msgs::SypridCmd::Request::PKZ_START_PORT);
  cmd_combo->addItem("PKZ START STBD", sentry_msgs::SypridCmd::Request::PKZ_START_STBD);
  cmd_combo->addItem("PKZ STOP PORT", sentry_msgs::SypridCmd::Request::PKZ_STOP_PORT);
  cmd_combo->addItem("PKZ STOP STBD", sentry_msgs::SypridCmd::Request::PKZ_STOP_STBD);
  cmd_combo->addItem("PKZ OPEN DCAM PORT", sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_PORT);
  cmd_combo->addItem("PKZ OPEN DCAM STBD", sentry_msgs::SypridCmd::Request::PKZ_OPEN_DCAM_STBD);
  cmd_combo->addItem("PKZ CLOSE PORT DCAM", sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_PORT);
  cmd_combo->addItem("PKZ CLOSE STBD DCAM", sentry_msgs::SypridCmd::Request::PKZ_CLOSE_DCAM_STBD);
  cmd_combo->addItem("PKZ ENABLE DCAM PERSISTENCE", sentry_msgs::SypridCmd::Request::PKZ_ENABLE_DCAM_PERSISTENCE);
  cmd_combo->addItem("PKZ DISABLE DCAM PERSISTENCE", sentry_msgs::SypridCmd::Request::PKZ_DISABLE_DCAM_PERSISTENCE);
  cmd_combo->addItem("PKZ REQUEST FLOW SENSOR DATA", sentry_msgs::SypridCmd::Request::PKZ_REQUEST_FLOW_SENSOR_DATA);
  cmd_combo->addItem("PKZ TORQUE PORT", sentry_msgs::SypridCmd::Request::PKZ_TORQUE_PORT);
  cmd_combo->addItem("PKZ TORQUE STBD", sentry_msgs::SypridCmd::Request::PKZ_TORQUE_STBD);
  cmd_btn = new QPushButton("Send Syprid Command");
  connect(cmd_btn, &QPushButton::clicked, this, &Syprid::cmd_click);
  cmd_grid->addWidget(cmd_lbl, 0, 0);
  cmd_grid->addWidget(cmd_combo, 1, 0);
  cmd_grid->addWidget(torque_1_edit, 2, 0);
  cmd_grid->addWidget(torque_2_edit, 3, 0);
  cmd_grid->addWidget(cmd_btn, 5, 0);
  cmds->setLayout(cmd_grid);
  ROS_INFO_STREAM("... done Syprid Cmd layout");

  grid->addWidget(stat, 0, 0);
  grid->addWidget(cmds, 0, 1);
  setLayout(grid);
  ROS_INFO_STREAM("... done Syprid final layout");
}

void Syprid::tick() {
    std::unique_lock<std::mutex> lck(data_mutex);
    if (status_u) {
        if (status.PKZ_CONNECTED == 1)
            pkz_conn_lbl->setText("Syprid Disconnected");
        else
            pkz_conn_lbl->setText("Syprid Disconnected");
        if (status.PKZ_DCAM_OPEN == 1)
            pkz_dcam_lbl->setText("Syprid DCAM Open");
        else
            pkz_dcam_lbl->setText("Syprid DCAM Closed");
        dcam_port_state_lbl->setText("Port State: " + QString::number(status.dcam_port_state));
        dcam_stbd_state_lbl->setText("Stbd State: " + QString::number(status.dcam_stbd_state));
        dcam_persistence_state_lbl->setText("DCAM Persistence State: " + QString::number(status.dcam_persistence_state));
        dcam_port_state_desired_lbl->setText("Port Desired: " + QString::number(status.dcam_port_state_desired));
        dcam_stbd_state_desired_lbl->setText("Stbd Desired: " + QString::number(status.dcam_stbd_state_desired));
        dcam_persistence_state_desired_lbl->setText("DCAM PersistenceDesired: " + QString::number(status.dcam_persistence_state_desired));
        elmo_port_rpm_latest_lbl->setText("Port Elmo RPM Latest: " + QString::number(status.elmo_port_rpm_latest));
        elmo_port_rpm_avg_lbl->setText("Port Elmo RPM Avg: " + QString::number(status.elmo_port_rpm_avg));
        elmo_stbd_rpm_latest_lbl->setText("Stbd Elmo RPM Latest: " + QString::number(status.elmo_stbd_rpm_latest));
        elmo_stbd_rpm_avg_lbl->setText("Stbd Elmo RPM Avg: " + QString::number(status.elmo_stbd_rpm_avg));
        elmo_port_temp_lbl->setText("Port Elmo Temp Avg: " + QString::number(status.elmo_port_temp_avg));
        elmo_stbd_temp_lbl->setText("Stbd Elmo Temp Avg: " + QString::number(status.elmo_stbd_temp_avg));
        flow_sensor_state_lbl->setText("Flow Sensor State: " + QString::number(status.flow_sensor_state));
        flow_sensor_state_desired_lbl->setText("Flow Sensor Desired: " + QString::number(status.flow_sensor_state_desired));
        status_u = false;
    }
}
void
Syprid::update_status(sentry_msgs::SypridState msg){
  std::unique_lock<std::mutex> lck(data_mutex);
  status = msg;
  status_u = true;
}

void
Syprid::cmd_click()
{
  sentry_msgs::SypridCmd cmd{};
  cmd.request.pkz_cmd = cmd_combo->currentIndex();
  cmd.request.torque_1 = torque_1_edit->text().toFloat();
  cmd.request.torque_2 = torque_1_edit->text().toFloat();
  if (cmd_cl->call(cmd)){
    ROS_INFO_STREAM("TORQUE COMMANDED: " << cmd.response.action);
  } else {
    ROS_ERROR_STREAM("TORQUE COMMAND FAILED");
  }
}

} //namespace