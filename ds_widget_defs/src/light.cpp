/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/20/18.
//

#include <ds_widget/light.h>
#include <ros/package.h>

namespace ds_widget
{
Light::Light(QString gs, QString rs, QString ys, QString os, QString nm, QWidget* _parent)
  : color("yellow"), name(nm), QWidget(_parent)
{
  //        ROS_INFO_STREAM("Light initialize! "<<name);
  std::string path = ros::package::getPath("ds_widget_defs");
  QString path_str = QString::fromStdString(path);
  if (!red_pm.load(path_str + rs))
  {
    //            ROS_ERROR_STREAM("No file "path_str + rs);
  }
  if (!green_pm.load(path_str + gs))
  {
    //            ROS_ERROR_STREAM("No file "path_str + gs);
  }
  if (!yellow_pm.load(path_str + ys))
  {
    //            ROS_ERROR_STREAM("No file "path_str + ys);
  }
  if (!orange_pm.load(path_str + os))
  {
    //            ROS_ERROR_STREAM("No file "path_str + os);
  }
  txtLbl = new QLabel();
  txtLbl->setStyleSheet("QLabel {background-color: transparent; color: black; }");
  txtLbl->setText(name + "\nhello");
  picLbl = new QLabel();
  picLbl->setPixmap(yellow_pm);
  grid = new QGridLayout();
  grid->addWidget(picLbl, 0, 0, Qt::AlignCenter);
  grid->addWidget(txtLbl, 0, 0, Qt::AlignCenter);
  setLayout(grid);
}

void Light::change_color()
{
  if (color == "red")
  {
    toGreen();
  }
  else if (color == "green")
  {
    toYellow();
  }
  else if (color == "yellow")
  {
    toRed();
  }
  else if (color == "orange")
  {
    toOrange();
  }
}

void Light::set_text(QString txt)
{
  //        ROS_INFO_STREAM("Light set text! "<<name);
  txtLbl->setText(name + "\n" + txt);
}

QString Light::get_text()
{
  return txtLbl->text();
}

}  // NAMESPACE
