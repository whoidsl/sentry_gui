/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/2/18.
//

#include "../include/ds_widget/ds_button.h"

namespace ds_widget
{
void DsButton::confirm_click()
{
  if (!confirm_dlog)
  {
    emit confirmed();
    return;
  }
  QMessageBox msgBox(this);
  msgBox.setWindowTitle("Confirm " + display_name);
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Ok);
  msgBox.setText(confirm_string);
  int ret = msgBox.exec();

  switch (ret)
  {
    case QMessageBox::Ok:
      // Save was clicked
      emit confirmed();
      break;
    case QMessageBox::Cancel:
      // Cancel was clicked
      break;
    default:
      // should never be reached
      break;
  }
}

OnOff::OnOff(QString _display_name, QString _confirm_string, QString _on, QString _off, QWidget* _parent)
  : DsButton(_display_name, _confirm_string, _parent), on(false)
{
  std::string path = ros::package::getPath("ds_widget_defs");
  QString path_str = QString::fromStdString(path);
  on_pm = QIcon(path_str + _on);
  off_pm = QIcon(path_str + _off);

  //        QGridLayout *grid = new QGridLayout();
  //        QLabel *name_lbl = new QLabel(name);
  setIcon(off_pm);
  //        grid->addWidget(name_lbl, 0, 0);
  //        grid->addWidget(lbl, 0, 1);
  //        setLayout(grid);
}

void OnOff::On()
{
  if (!on)
  {
    on = true;
    setIcon(on_pm);
  }
}

void OnOff::Off()
{
  if (on)
  {
    on = false;
    setIcon(off_pm);
  }
}

RedGreen::RedGreen(QString _name, QWidget* parent, bool _confirm_dlog)
  : DsButton(_name, _name, parent, _confirm_dlog), color("none")
{
  makeYellow();
}

void RedGreen::makeGreen()
{
  if (color == "green")
  {
    return;
  }
  color = "green";
  setStyleSheet("QPushButton { background-color: lightGreen; color: black; }");
}

void RedGreen::makeRed()
{
  if (color == "red")
  {
    return;
  }
  color = "red";
  setStyleSheet("QPushButton { background-color: red;  color: black;}");
}

void RedGreen::makeYellow()
{
  if (color == "yellow")
  {
    return;
  }
  color = "yellow";
  setStyleSheet("QPushButton { background-color: yellow;  color: black;}");
}

}  // end namespace
