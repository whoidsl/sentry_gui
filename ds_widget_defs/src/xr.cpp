/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/5/18.
//

#include "../include/ds_widget/xr.h"
#include <QDoubleValidator>

using namespace ds_widget;

Xr::Xr(ros::ServiceClient* _cl1, ros::ServiceClient* _cl2, ros::ServiceClient* _deadhour_cl1,
       ros::ServiceClient* _deadhour_cl2, QWidget* _parent)
  : QWidget(_parent)
  , cl1(_cl1)
  , cl2(_cl2)
  , deadhour_cl1(_deadhour_cl1)
  , deadhour_cl2(_deadhour_cl2)
  , timesince1(0)
  , timesince2(0)
  , view_needs_update_1(false)
  , view_needs_update_2(false)
{
  setupWidget();
  setupConnections();
}

void Xr::setupWidget()
{
  QFont f("Arial", 30, QFont::Bold);
  QLabel* xr1_dhr_txt = new QLabel("XR 1:");
  xr1_dhr = new QLabel();
  xr1_dhr->setFont(f);
  QLabel* xr2_dhr_txt = new QLabel("XR 2:");
  xr2_dhr = new QLabel();
  xr2_dhr->setFont(f);

  xr1_short = new QLabel();
  xr2_short = new QLabel();

  dead_btn = new DsButton("Send Deadhour", "Send Deadhour?");
  dead_edit = new QLineEdit(this);
  QDoubleValidator* valid = new QDoubleValidator(0, 255, 2, dead_edit);
  dead_edit->setValidator(valid);
  dead_edit->setPlaceholderText("0-255");

  p_burn = new OnOff("P", "Burn Port?");
  d_burn = new OnOff("D", "Burn Descent?");
  s_burn = new OnOff("S", "Burn Starboard?");
  burn_all = new DsButton("Burn All", "Burn all burnwires?");

  p_snuff = new DsButton("P", "Secure port burnwire?");
  d_snuff = new DsButton("D", "Secure descent burnwire?");
  s_snuff = new DsButton("S", "Secure starboard burnwire?");
  snuff_all = new DsButton("Secure All", "Secure all burnwires?");

  p_hold = new OnOff("P", "Hold Port?");
  d_hold = new OnOff("D", "Hold Descent?");
  s_hold = new OnOff("S", "Hold Starboard");
  hold_all = new DsButton("Hold All", "Hold all dropweights?");

  p_drop = new OnOff("P", "Drop Port?");
  d_drop = new OnOff("D", "Drop Descent?");
  s_drop = new OnOff("S", "Drop Starboard?");
  drop_all = new DsButton("Drop All", "Drop all dropweights?");

  QGridLayout* grid = new QGridLayout(this);

  grid->addWidget(xr1_dhr_txt, 0, 0);
  grid->addWidget(xr1_dhr, 0, 1, 2, 3);
  grid->addWidget(xr1_short, 1, 0);
  grid->addWidget(xr2_dhr_txt, 2, 0);
  grid->addWidget(xr2_dhr, 2, 1, 2, 3);
  grid->addWidget(xr2_short, 3, 0);

  grid->addWidget(dead_btn, 5, 2, 1, 3);
  grid->addWidget(dead_edit, 5, 0, 1, 2);

  grid->addWidget(p_burn, 6, 2, 1, 1);
  grid->addWidget(d_burn, 6, 3, 1, 1);
  grid->addWidget(s_burn, 6, 4, 1, 1);
  grid->addWidget(burn_all, 6, 0, 1, 2);

  grid->addWidget(p_snuff, 7, 2, 1, 1);
  grid->addWidget(d_snuff, 7, 3, 1, 1);
  grid->addWidget(s_snuff, 7, 4, 1, 1);
  grid->addWidget(snuff_all, 7, 0, 1, 2);

  grid->addWidget(p_hold, 8, 2, 1, 1);
  grid->addWidget(d_hold, 8, 3, 1, 1);
  grid->addWidget(s_hold, 8, 4, 1, 1);
  grid->addWidget(hold_all, 8, 0, 1, 2);

  grid->addWidget(p_drop, 9, 2, 1, 1);
  grid->addWidget(d_drop, 9, 3, 1, 1);
  grid->addWidget(s_drop, 9, 4, 1, 1);
  grid->addWidget(drop_all, 9, 0, 1, 2);

  setLayout(grid);
}

void Xr::setupConnections()
{
  connect(dead_btn, SIGNAL(confirmed()), this, SLOT(deadhour_set()));

  connect(burn_all, SIGNAL(confirmed()), p_burn, SLOT(confirm()));
  connect(burn_all, SIGNAL(confirmed()), d_burn, SLOT(confirm()));
  connect(burn_all, SIGNAL(confirmed()), s_burn, SLOT(confirm()));

  connect(snuff_all, SIGNAL(confirmed()), p_snuff, SLOT(confirm()));
  connect(snuff_all, SIGNAL(confirmed()), d_snuff, SLOT(confirm()));
  connect(snuff_all, SIGNAL(confirmed()), s_snuff, SLOT(confirm()));

  connect(hold_all, SIGNAL(confirmed()), p_hold, SLOT(confirm()));
  connect(hold_all, SIGNAL(confirmed()), d_hold, SLOT(confirm()));
  connect(hold_all, SIGNAL(confirmed()), s_hold, SLOT(confirm()));

  connect(drop_all, SIGNAL(confirmed()), p_drop, SLOT(confirm()));
  connect(drop_all, SIGNAL(confirmed()), d_drop, SLOT(confirm()));
  connect(drop_all, SIGNAL(confirmed()), s_drop, SLOT(confirm()));

  connect(p_burn, SIGNAL(confirmed()), this, SLOT(port_burn()));
  connect(d_burn, SIGNAL(confirmed()), this, SLOT(descent_burn()));
  connect(s_burn, SIGNAL(confirmed()), this, SLOT(starboard_burn()));

  connect(p_snuff, SIGNAL(confirmed()), this, SLOT(port_snuff()));
  connect(d_snuff, SIGNAL(confirmed()), this, SLOT(descent_snuff()));
  connect(s_snuff, SIGNAL(confirmed()), this, SLOT(starboard_snuff()));

  connect(p_hold, SIGNAL(confirmed()), this, SLOT(port_hold()));
  connect(d_hold, SIGNAL(confirmed()), this, SLOT(descent_hold()));
  connect(s_hold, SIGNAL(confirmed()), this, SLOT(starboard_hold()));

  connect(p_drop, SIGNAL(confirmed()), this, SLOT(port_drop()));
  connect(d_drop, SIGNAL(confirmed()), this, SLOT(descent_drop()));
  connect(s_drop, SIGNAL(confirmed()), this, SLOT(starboard_drop()));
}

void Xr::update(ds_hotel_msgs::XR msg)
{
  if (msg.idnum == 1)
  {
    std::unique_lock<std::mutex> lck1(model_mutex_1);
    xr1_model = msg;
    view_needs_update_1 = true;
    timesince1 = 0;
  }

  if (msg.idnum == 2)
  {
    std::unique_lock<std::mutex> lck2(model_mutex_2);
    xr2_model = msg;
    view_needs_update_2 = true;
    timesince2 = 0;
  }
}

void Xr::tick()
{
  timesince1 += 1;
  timesince2 += 1;

  if (view_needs_update_1)
  {
    update_view_1();
  }
  if (view_needs_update_2)
  {
    update_view_2();
  }
}

void Xr::update_view_1()
{
  std::unique_lock<std::mutex> lck1(model_mutex_1);
  auto msg = xr1_model;
  xr1_dhr->setText(QString::number(ds_util::float_round(msg.deadhour, 1)));
  xr1_short->setText(QString::number(ds_util::float_round(msg.short_deadsecs, 0)));

  if (msg.motor_2_drop)
  {
    p_drop->On();
  }
  else
  {
    p_drop->Off();
  }
  if (msg.motor_2_hold)
  {
    p_hold->On();
  }
  else
  {
    p_hold->Off();
  }

  if (msg.burnwire_1_drive)
  {
    d_burn->On();
  }
  else
  {
    d_burn->Off();
  }
  if (msg.burnwire_2_drive)
  {
    s_burn->On();
  }
  else
  {
    s_burn->Off();
  }
  view_needs_update_1 = false;
}

void Xr::update_view_2()
{
  std::unique_lock<std::mutex> lck2(model_mutex_2);
  auto msg = xr2_model;
  xr2_dhr->setText(QString::number(ds_util::float_round(msg.deadhour, 1)));
  xr2_short->setText(QString::number(ds_util::float_round(msg.short_deadsecs, 0)));

  if (msg.motor_1_drop)
  {
    d_drop->On();
  }
  else
  {
    d_drop->Off();
  }
  if (msg.motor_1_hold)
  {
    d_hold->On();
  }
  else
  {
    d_hold->Off();
  }
  if (msg.motor_2_drop)
  {
    s_drop->On();
  }
  else
  {
    s_drop->Off();
  }
  if (msg.motor_2_hold)
  {
    s_hold->On();
  }
  else
  {
    s_hold->Off();
  }

  if (msg.burnwire_2_drive)
  {
    p_burn->On();
  }
  else
  {
    p_burn->Off();
  }
  view_needs_update_2 = false;
}

void Xr::port_drop()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_2;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Port drop!");
  }
  else
  {
    ROS_ERROR_STREAM("Port drop failure!");
  }
}

void Xr::starboard_drop()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_2;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Starboard drop!");
  }
  else
  {
    ROS_ERROR_STREAM("Starboard drop failure!");
  }
}

void Xr::descent_drop()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_OPEN_1;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Descent drop!");
  }
  else
  {
    ROS_ERROR_STREAM("Descent drop failure!");
  }
}

void Xr::port_hold()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE_2;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Port hold!");
  }
  else
  {
    ROS_ERROR_STREAM("Port hold failure!");
  }
}

void Xr::starboard_hold()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE_2;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Starboard hold!");
  }
  else
  {
    ROS_ERROR_STREAM("Starboard hold failure!");
  }
}

void Xr::descent_hold()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_DCAM_CLOSE_1;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Descent hold!");
  }
  else
  {
    ROS_ERROR_STREAM("Descent hold failure!");
  }
}

void Xr::port_burn()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_2;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Port burn!");
  }
  else
  {
    ROS_ERROR_STREAM("Port burn failure!");
  }
}

void Xr::starboard_burn()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_2;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Starboard burn!");
  }
  else
  {
    ROS_ERROR_STREAM("Starboard burn failure!");
  }
}

void Xr::descent_burn()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_ON_1;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Descent burn!");
  }
  else
  {
    ROS_ERROR_STREAM("Descent burn failure!");
  }
}

void Xr::port_snuff()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF;
  if (cl2->call(cmd))
  {
    ROS_INFO_STREAM("Port snuff!");
  }
  else
  {
    ROS_ERROR_STREAM("Port snuff failure!");
  }
}

void Xr::starboard_snuff()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF_2;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Starboard snuff!");
  }
  else
  {
    ROS_ERROR_STREAM("Starboard snuff failure!");
  }
}

void Xr::descent_snuff()
{
  auto cmd = sentry_msgs::XRCmd();
  cmd.request.command = sentry_msgs::XRCmd::Request::XR_CMD_BURNWIRE_OFF_1;
  if (cl1->call(cmd))
  {
    ROS_INFO_STREAM("Descent snuff!");
  }
  else
  {
    ROS_ERROR_STREAM("Descent snuff failure!");
  }
}

void Xr::deadhour_set()
{
  sentry_msgs::DeadhourCmd cmd;

  float deadhour = dead_edit->text().toFloat();

  cmd.request.deadhour = deadhour;
  if (deadhour_cl1->call(cmd))
  {
    ROS_INFO_STREAM("XR1 DEADHOUR SET: " << deadhour << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 DEADHOUR SET: " << deadhour << " FAILURE");
  }
  if (deadhour_cl2->call(cmd))
  {
    ROS_INFO_STREAM("XR2 DEADHOUR SET: " << deadhour << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 DEADHOUR SET: " << deadhour << " FAILURE");
  }
}
