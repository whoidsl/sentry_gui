/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/3/18.
//

#include "../include/ds_widget/power_lights.h"

namespace ds_widget
{
BatmanLights::BatmanLights(QWidget* _parent) : QWidget(_parent), timesince(0), time_thresh(60), view_needs_update(false)
{
  setupWidget();
}

void BatmanLights::setupWidget()
{
  percent = new RoundLight("Total %");
  ahr = new RoundLight("Total Ahr");
  QLabel* spacer_lbl1 = new QLabel("");
  QLabel* spacer_lbl2 = new QLabel("");

  grid = new QGridLayout();

  grid->addWidget(percent, 0, 0);
  grid->addWidget(ahr, 0, 1);
  grid->addWidget(spacer_lbl1, 1, 0);
  grid->addWidget(spacer_lbl2, 1, 1);

  setLayout(grid);
}

void BatmanLights::update(ds_hotel_msgs::BatMan in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  timesince = 0;
  msg = in_msg;
  view_needs_update = true;
}

void BatmanLights::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);

  percent->set_text(QString::number(ds_util::float_round(msg.percentFull, 2)));
  if (msg.percentFull > 10)
  {
    percent->toGreen();
  }
  else
  {
    percent->toRed();
  }
  ahr->set_text(QString::number(ds_util::float_round(msg.chargeAh, 2)));
  if (msg.chargeAh < 30)
  {
    ahr->toRed();
  }
  else
  {
    ahr->toGreen();
  }
  view_needs_update = false;
}

void BatmanLights::tick()
{
  timesince += 1;
  if (timesince > time_thresh)
  {
    percent->toYellow();
    ahr->toYellow();
  }
  if (view_needs_update)
  {
    update_view();
  }
}

ShoreLights::ShoreLights(QWidget* _parent) : QWidget(_parent), timesince(0), time_thresh(60), view_needs_update(false)
{
  setupWidget();
}

void ShoreLights::setupWidget()
{
  voltage = new RoundLight("Shore V");
  current = new RoundLight("Shore C");

  grid = new QGridLayout();

  grid->addWidget(voltage, 0, 0);
  grid->addWidget(current, 0, 1);

  setLayout(grid);
}

void ShoreLights::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);
  voltage->toGreen();
  current->toGreen();

  voltage->set_text(QString::number(ds_util::float_round(msg.meas_volts, 2)));
  if (msg.meas_volts < 60 || msg.meas_volts > 75)
  {
    voltage->toRed();
  }
  current->set_text(QString::number(ds_util::float_round(msg.meas_amps, 2)));
  if (msg.meas_amps < 1)
  {
    current->toRed();
  }
  view_needs_update = false;
}

void ShoreLights::update(ds_hotel_msgs::PowerSupply in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  timesince = 0;
  msg = in_msg;
  view_needs_update = true;
}

void ShoreLights::tick()
{
  timesince += 1;
  if (timesince > time_thresh)
  {
    voltage->toYellow();
    current->toYellow();
  }
  if (view_needs_update)
  {
    update_view();
  }
}

BatPercentLight::BatPercentLight(int _id, QWidget* _parent)
  : QWidget(_parent), id(_id), timesince(0), time_thresh(60), view_needs_update(false)
{
  setupWidget();
}

void BatPercentLight::setupWidget()
{
  percent = new RoundLight("Bat " + QString::number(id) + " %");
  state = new QLabel("");
  state->setAlignment(Qt::AlignCenter);
  grid = new QGridLayout();
  grid->addWidget(percent, 0, 0);
  grid->addWidget(state, 1, 0);
  setLayout(grid);
}

void BatPercentLight::update_view()
{
  percent->toGreen();

  percent->set_text(QString::number(ds_util::float_round(msg.percentFull, 2)));
  if (msg.percentFull < 10)
  {
    percent->toRed();
  }
  if (msg.safetyString != "Normal")
  {
    state->setText(QString::fromStdString(msg.safetyString));
  }
  else if (msg.discharging)
  {
    state->setText("Discharging");
  }
  else if (msg.balanceEnable)
  {
    state->setText("Balance");
  }
  else if (msg.charging)
  {
    state->setText("Charging");
  }
  else
  {
    state->setText("Off");
  }

  view_needs_update = false;
}

void BatPercentLight::update(ds_hotel_msgs::Battery in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  timesince = 0;
  msg = in_msg;
  view_needs_update = true;
}

void BatPercentLight::tick()
{
  timesince += 1;
  if (timesince > time_thresh)
  {
    percent->toYellow();
    state->setText("");
  }
  if (view_needs_update)
  {
    update_view();
  }
}
}
