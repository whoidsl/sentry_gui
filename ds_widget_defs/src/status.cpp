/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 6/6/18.
//

#include <../include/ds_widget/status.h>

using namespace ds_widget;

StatusObj::StatusObj(QString _name, int _status, QWidget* _parent) : QWidget(_parent), timesince(0), status(-1)
{
  QGridLayout* grid = new QGridLayout(this);
  name_lbl = new QLabel(_name, this);
  //    status_lbl = new QLabel("", this);
  grid->addWidget(name_lbl, 0, 0);
  //    grid->addWidget(status_lbl, 0, 1);
  update_status(_status);
}

void StatusObj::update_status(int new_status)
{
  if (new_status == status)
  {
    timesince = 0;
    return;
  }
  status = new_status;
  switch (status)
  {
    case ds_core_msgs::Status::STATUS_GOOD:
      // status good : green
      //            status_lbl->setText("Good");
      setStyleSheet("QLabel { background-color : green; color : black; }");
      break;
    case ds_core_msgs::Status::STATUS_WARN:
      // status warn : orange
      //            status_lbl->setText("Warn");
      setStyleSheet("QLabel { background-color : orange; color : black; }");
      break;
    case ds_core_msgs::Status::STATUS_ERROR:
      // status error : red
      //            status_lbl->setText("Error");
      setStyleSheet("QLabel { background-color : red; color : black; }");
      break;
    default:
      // status default : yellow
      //            status_lbl->setText("Unknown");
      setStyleSheet("QLabel { background-color : yellow; color : black; }");
      return;
  }
  timesince = 0;
}

void StatusObj::tick()
{
  if (timesince > 15000)
  {
    update_status(timesince);
  }
  timesince++;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Status::Status(QWidget* parent) : QWidget(parent)
{
  num_sensors = 0;
  view_needs_update = false;
  grid = new QGridLayout(this);
  //    new_item = new StatusObj("", -1, this);
  //    grid->addWidget(new_item, 0, 0);
}

void Status::update(ds_core_msgs::Status in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  model = in_msg;
  view_needs_update = true;
}

void Status::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  //    new_item->tick();
  for (auto it = statuses.begin(); it != statuses.end(); it++)
  {
    it->second->tick();
  }
}

void Status::update_view()
{
  ROS_ERROR_STREAM("Update view!");
  std::unique_lock<std::mutex> lck(model_mutex);
  if (statuses.count(model.descriptive_name) == 0)
  {
    num_sensors++;
    ROS_ERROR_STREAM("new object");
    StatusObj* item = new StatusObj(QString::fromStdString(model.descriptive_name), model.status, this);
    ROS_ERROR_STREAM("created statusobj");
    statuses[model.descriptive_name] = item;
    ROS_ERROR_STREAM("added to std::map");
    grid->addWidget(statuses[model.descriptive_name], num_sensors % 15, num_sensors / 15);
    ROS_ERROR_STREAM("added to display");
  }
  else
  {
    statuses[model.descriptive_name]->update_status(model.status);
  }
  view_needs_update = false;
}

void Status::reorder_view()
{
  int i = 0;
  for (auto it = statuses.begin(); it != statuses.end(); it++)
  {
    grid->addWidget(it->second, i, 0);
    i++;
  }
}