/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// https://wiki.python.org/moin/PyQt/Compass%20widget
// Adapted from existing python widget
// On the #pyqt channel on Freenode, epifanio was creating a compass widget.
// Although he eventually decided to use Graphics View for this, the following
// code could be used as the starting point for a simple custom widget with
// a custom signal and a property for controlling the angle.
//
// Created by jvaccaro on 4/16/18.
//

#include "../include/ds_widget/compass.h"

namespace ds_widget
{
Compass::Compass(QWidget* parent) : QWidget(parent), _angle(0.0), _margins(10)
{
  _pointText = { { 0, "N" },   { 45, "NE" },  { 90, "E" },  { 135, "SE" },
                 { 180, "S" }, { 225, "SW" }, { 270, "W" }, { 315, "NW" } };
}

void Compass::resizeEvent(QResizeEvent* event)
{
  QWidget::resizeEvent(event);
}

void Compass::paintEvent(QPaintEvent* event)
{
  //        QPainter painter(this);
  //        painter.fillRect(event->rect(), palette().brush(QPalette::Window));
  //        painter.restore();

  drawMarkings();
  drawNeedle();

  QWidget::paintEvent(event);
}

//    void Compass::paintEvent(QResizeEvent *event) {
//        QPainter painter(this);
//        painter.begin(this);
//        painter.setRenderHint(QPainter::Antialiasing);
//        painter.fillRect(rect(), palette().brush(QPalette::Window));
//        painter.end();
//        drawMarkings();
//        drawNeedle();
//    }

void Compass::drawMarkings()
{
  QPainter painter(this);
  //        painter.begin(this);
  painter.save();
  painter.translate(width() / 2, height() / 2);
  float scale = qMin((width() - _margins) / 120.0, (height() - _margins) / 120.0);
  painter.scale(scale, scale);
  QFont local_font(font());
  local_font.setPixelSize(10);
  auto metrics = QFontMetricsF(local_font);

  painter.setFont(local_font);
  painter.setPen(palette().color(QPalette::Shadow));

  int i = 0;
  while (i < 360)
  {
    if (i % 45 == 0)
    {
      painter.drawLine(0, -40, 0, -50);
      painter.drawText(-metrics.width(_pointText[i]) / 2.0, -52, _pointText[i]);
    }
    else
    {
      painter.drawLine(0, -45, 0, -50);
    }

    painter.rotate(15);
    i += 15;
  }

  painter.restore();

  //        painter.end();
}

void Compass::drawNeedle()
{
  QPainter painter(this);
  //        painter.begin(this);
  painter.save();
  painter.translate(width() / 2, height() / 2);
  painter.rotate(_angle);
  float scale = qMin((width() - _margins) / 120.0, (height() - _margins) / 120.0);
  painter.scale(scale, scale);

  painter.setPen(QPen(Qt::NoPen));
  painter.setBrush(palette().brush(QPalette::Shadow));

  static const QPoint poly1[7] = { QPoint(-10, 5), QPoint(-5, -25), QPoint(0, -45), QPoint(5, -25),
                                   QPoint(10, 5),  QPoint(0, 10),   QPoint(-10, 5) };

  painter.drawPolygon(poly1, 7);

  painter.setBrush(palette().brush(QPalette::Highlight));

  static const QPoint poly2[5] = { QPoint(-5, -25), QPoint(0, -45), QPoint(5, -25), QPoint(0, -30), QPoint(-5, -25) };
  painter.drawPolygon(poly2, 5);

  painter.restore();

  //        painter.end();
}

QSize Compass::sizeHint()
{
  return QSize(150, 150);
}

float Compass::angle()
{
  return _angle;
}

void Compass::setAngle(float angle)
{
  if (angle != _angle)
  {
    _angle = angle;
    emit angleChanged(angle);
    update();
  }
}

}  // NAMESPACE
