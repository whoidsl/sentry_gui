/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 10/7/19.
//

#include "ds_widget/kongsberg.h"

namespace ds_widget{

Kongsberg::Kongsberg(ros::ServiceClient* _ping_cl
    , ros::ServiceClient* _bist_cl
    , ros::ServiceClient* _settings_cl
    , QWidget* parent)
    : QWidget(parent)
    , ping_cl(_ping_cl)
    , bist_cl(_bist_cl)
    , settings_cl(_settings_cl)
{
  ROS_INFO_STREAM("Start Kongsberg Layout");
  auto grid = new QGridLayout();
  auto stat = new QGroupBox("Status");
  auto stat_grid = new QGridLayout();
  name_lbl = new QLabel("");
  ship_lbl = new QLabel("");
  kctrl_con_lbl = new QLabel("");
  pu_pow_lbl = new QLabel("");
  pu_con_lbl = new QLabel("");
  kmall_con_lbl = new QLabel("");
  pinging_lbl = new QLabel("");
  ping_combo = new QComboBox();
  ping_combo->addItem("Choose Ping Cmd", 0);
  ping_combo->addItem("Ping Start", ds_kongsberg_msgs::PingCmd::Request::PING_START);
  ping_combo->addItem("Ping Stop", ds_kongsberg_msgs::PingCmd::Request::PING_STOP);
  ping_combo->addItem("New File", ds_kongsberg_msgs::PingCmd::Request::PING_NEWFILE);
  ping_combo->addItem("Start Sounder", ds_kongsberg_msgs::PingCmd::Request::PING_STARTUP);
  ping_combo->addItem("Force New File", ds_kongsberg_msgs::PingCmd::Request::PING_NEWFILE_FORCE);
  ping_btn = new QPushButton("Send PingCmd");
  connect(ping_btn, &QPushButton::clicked, this, &Kongsberg::ping_click);
  stat_grid->addWidget(name_lbl, 0, 0);
  stat_grid->addWidget(ship_lbl, 1, 0);
  stat_grid->addWidget(kctrl_con_lbl, 2, 0);
  stat_grid->addWidget(pu_pow_lbl, 3, 0);
  stat_grid->addWidget(pu_con_lbl, 4, 0);
  stat_grid->addWidget(kmall_con_lbl, 5, 0);
  stat_grid->addWidget(pinging_lbl, 6, 0);
  stat_grid->addWidget(ping_combo, 7, 0);
  stat_grid->addWidget(ping_btn, 8, 0);
  stat->setLayout(stat_grid);
  ROS_INFO_STREAM("... done Kongsberg status layout");
  auto bist = new QGroupBox("BIST");
  auto bist_grid = new QGridLayout();
  bist_lbl = new QLabel("");
  bist_current_lbl = new QLabel("");
  bist_combo = new QComboBox();
  bist_combo->addItem("Choose BIST Cmd", 0);
  bist_combo->addItem("Deck Sequence", ds_kongsberg_msgs::BistCmd::Request::BIST_ONDECK);
  bist_combo->addItem("Inwater Sequence", ds_kongsberg_msgs::BistCmd::Request::BIST_INWATER);
  bist_combo->addItem("Cancel BIST", ds_kongsberg_msgs::BistCmd::Request::BIST_CANCEL);
  bist_combo->addItem("CPU", ds_kongsberg_msgs::BistCmd::Request::BIST_CPU);
  bist_combo->addItem("CBMF", ds_kongsberg_msgs::BistCmd::Request::BIST_CBMF);
  bist_combo->addItem("RX Unit", ds_kongsberg_msgs::BistCmd::Request::BIST_RX_UNIT);
  bist_combo->addItem("TX Unit", ds_kongsberg_msgs::BistCmd::Request::BIST_TX_UNIT);
  bist_combo->addItem("CBMF to CPU", ds_kongsberg_msgs::BistCmd::Request::BIST_CBMF_CPU);
  bist_combo->addItem("RX to CBMF", ds_kongsberg_msgs::BistCmd::Request::BIST_RX_CBMF);
  bist_combo->addItem("RX Channels", ds_kongsberg_msgs::BistCmd::Request::BIST_RX_CHANNELS);
  bist_combo->addItem("TX via RX", ds_kongsberg_msgs::BistCmd::Request::BIST_TX_VIA_RX);
  bist_combo->addItem("RX Noise Level", ds_kongsberg_msgs::BistCmd::Request::BIST_RX_NOISE_LEVEL);
  bist_combo->addItem("RX Noise Spectrum", ds_kongsberg_msgs::BistCmd::Request::BIST_RX_NOISE_SPECTRUM);
  bist_combo->addItem("Software Versions", ds_kongsberg_msgs::BistCmd::Request::BIST_SOFTWARE_VERSIONS);
  bist_combo->addItem("System Info", ds_kongsberg_msgs::BistCmd::Request::BIST_SYSTEM_INFO);
  bist_btn = new QPushButton("Command BIST");
  connect(bist_btn, &QPushButton::clicked, this, &Kongsberg::bist_click);
  bist_grid->addWidget(bist_lbl, 0, 0);
  bist_grid->addWidget(bist_current_lbl, 1, 0);
  bist_grid->addWidget(bist_combo, 2, 0);
  bist_grid->addWidget(bist_btn, 3, 0);
  bist->setLayout(bist_grid);
  ROS_INFO_STREAM("... done Kongsberg bist layout");

  auto data = new QGroupBox("Data");
  ROS_INFO_STREAM("... done Kongsberg data layout");

  auto sensors = new QGroupBox("Sensors");
  auto sensors_grid = new QGridLayout();
  cpu_temp_lbl = new QLabel("");
  pos1_lbl = new QLabel("");
  pos2_lbl = new QLabel("");
  pos3_lbl = new QLabel("");
  att1_lbl = new QLabel("");
  att2_lbl = new QLabel("");
  dep_lbl = new QLabel("");
  svp_lbl = new QLabel("");
  clk_lbl = new QLabel("");
  pps_lbl = new QLabel("");
  pu_offset_lbl = new QLabel("");
  sensors_grid->addWidget(cpu_temp_lbl, 0, 0);
  sensors_grid->addWidget(pos1_lbl, 1, 0);
  sensors_grid->addWidget(pos2_lbl, 2, 0);
  sensors_grid->addWidget(pos3_lbl, 3, 0);
  sensors_grid->addWidget(att1_lbl, 4, 0);
  sensors_grid->addWidget(att2_lbl, 5, 0);
  sensors_grid->addWidget(dep_lbl, 6, 0);
  sensors_grid->addWidget(svp_lbl, 7, 0);
  sensors_grid->addWidget(clk_lbl, 8, 0);
  sensors_grid->addWidget(pps_lbl, 9, 0);
  sensors_grid->addWidget(pu_offset_lbl, 10, 0);
  sensors->setLayout(sensors_grid);
  ROS_INFO_STREAM("... done Kongsberg sensors layout");

  auto ping = new QGroupBox("Ping");
  auto ping_grid = new QGridLayout();
  ping_num_lbl = new QLabel("");
  num_soundings_lbl = new QLabel("");
  percent_good_lbl = new QLabel("");
  max_depth_lbl = new QLabel("");
  min_depth_lbl = new QLabel("");
  center_depth_lbl = new QLabel("");
  max_range_lbl = new QLabel("");
  min_range_lbl = new QLabel("");
  center_range_lbl = new QLabel("");
  ping_grid->addWidget(ping_num_lbl, 0, 0);
  ping_grid->addWidget(num_soundings_lbl, 1, 0);
  ping_grid->addWidget(percent_good_lbl, 2, 0);
  ping_grid->addWidget(max_depth_lbl, 3, 0);
  ping_grid->addWidget(min_depth_lbl, 4, 0);
  ping_grid->addWidget(center_depth_lbl, 5, 0);
  ping_grid->addWidget(max_range_lbl, 6, 0);
  ping_grid->addWidget(min_range_lbl, 7, 0);
  ping_grid->addWidget(center_range_lbl, 8, 0);
  ping->setLayout(ping_grid);
  ROS_INFO_STREAM("... done Kongsberg ping layout");

  auto params = new QGroupBox("Params");
  auto param_grid = new QGridLayout();
  rt_freq_lbl = new QLabel("");
  rt_trig_lbl = new QLabel("");
  rt_min_depth_lbl = new QLabel("");
  rt_max_depth_lbl = new QLabel("");
  rt_depth_mode_lbl = new QLabel("");
  p_name_edit = new QLineEdit("");
  p_name_edit->setPlaceholderText("PARAM_NAME");
  p_val_edit = new QLineEdit("");
  p_val_edit->setPlaceholderText("PARAM_VALUE");
  p_btn = new QPushButton("Send ParamCmd");
  connect(p_btn, &QPushButton::clicked, this, &Kongsberg::param_click);
  param_grid->addWidget(rt_freq_lbl, 0, 0);
  param_grid->addWidget(rt_trig_lbl, 1, 0);
  param_grid->addWidget(rt_min_depth_lbl, 2, 0);
  param_grid->addWidget(rt_max_depth_lbl, 3, 0);
  param_grid->addWidget(rt_depth_mode_lbl, 4, 0);
  param_grid->addWidget(p_name_edit, 5, 0);
  param_grid->addWidget(p_val_edit, 6, 0);
  param_grid->addWidget(p_btn, 7, 0);
  params->setLayout(param_grid);
  ROS_INFO_STREAM("... done Kongsberg param layout");

  grid->addWidget(stat, 0, 0);
  grid->addWidget(bist, 0, 1);
  grid->addWidget(data, 0, 2);
  grid->addWidget(sensors, 0, 3);
  grid->addWidget(ping, 0, 4);
  grid->addWidget(params, 0, 5);
  setLayout(grid);
  ROS_INFO_STREAM("... done Kongsberg final layout");
}

void
Kongsberg::tick(){
  std::unique_lock<std::mutex> lck(data_mutex);
  if (status_u){
    name_lbl->setText(QString::fromStdString(status.sounder_name));
    ship_lbl->setText(QString::fromStdString(status.ship_name));
    if (status.kctrl_connected)
      kctrl_con_lbl->setText("Kctrl connected");
    else
      kctrl_con_lbl->setText("Kctrl DISCONNECTED");
    if (status.pu_powered)
      pu_pow_lbl->setText("PU powered");
    else
      pu_pow_lbl->setText("PU NOT POWERED");
    if (status.pu_connected)
      pu_con_lbl->setText("PU connected");
    else
      pu_con_lbl->setText("PU NOT CONNECTED");
    if (status.kmall_connected)
      kmall_con_lbl->setText("KMAll connected");
    else
      kmall_con_lbl->setText("KMALL NOT CONNECTED");
    if (status.pinging)
      pinging_lbl->setText("Pinging");
    else
      pinging_lbl->setText("NOT PINGING");
    if (status.bist_running)
      bist_lbl->setText("Bist running!");
    else
      bist_lbl->setText("");
    bist_current_lbl->setText(QString::fromStdString(status.bist_current));
    cpu_temp_lbl->setText(QString::fromStdString(status.cpu_temperature));
    pos1_lbl->setText(QString::fromStdString(status.position_1));
    pos2_lbl->setText(QString::fromStdString(status.position_2));
    pos3_lbl->setText(QString::fromStdString(status.position_3));
    att1_lbl->setText(QString::fromStdString(status.attitude_1));
    att2_lbl->setText(QString::fromStdString(status.attitude_2));
    dep_lbl->setText(QString::fromStdString(status.depth));
    svp_lbl->setText(QString::fromStdString(status.svp));
    clk_lbl->setText(QString::fromStdString(status.time_sync));
    pps_lbl->setText(QString::fromStdString(status.pps));

    ping_num_lbl->setText("#" + QString::number(status.ping_num));
    num_soundings_lbl->setText("Soundings: " + QString::number(status.num_soundings));
    percent_good_lbl->setText("Good: " + QString::number(status.percent_good) + "%");
    max_depth_lbl->setText("Max Dep: " + QString::number(status.max_depth) + "m");
    min_depth_lbl->setText("Min Dep: " + QString::number(status.min_depth) + "m");
    center_depth_lbl->setText("Center Dep: " + QString::number(status.center_depth) + "m");
    max_range_lbl->setText("Max Rng: " + QString::number(status.max_range) + "m");
    min_range_lbl->setText("Min Rng: " + QString::number(status.min_range) + "m");
    center_range_lbl->setText("Center Rng: " + QString::number(status.center_range) + "m");

    rt_freq_lbl->setText("RT Freq: " + QString::fromStdString(status.rt_ping_freq));
    rt_trig_lbl->setText("RT Trigger: " + QString::fromStdString(status.rt_trigger));
    rt_min_depth_lbl->setText("RT MinDep: " + QString::fromStdString(status.rt_min_depth));
    rt_max_depth_lbl->setText("RT MaxDep: " + QString::fromStdString(status.rt_max_depth));
    rt_depth_mode_lbl->setText("RT DepMode: " + QString::fromStdString(status.rt_depth_mode));
    status_u = false;
  }
  if (pu_status_u){
    std::unique_lock<std::mutex> lck(pu_data_mutex);
    pu_offset_lbl->setText("PU Time Offset: " + QString::number(pu_offset.device_stamp_minus_ros_stamp_sec));
    pu_status_u = false;
  }
}



void
Kongsberg::update_status(ds_kongsberg_msgs::KongsbergStatus msg){
  std::unique_lock<std::mutex> lck(data_mutex);
  status = msg;
  status_u = true;
}

void
Kongsberg::update_pu_offset_status(ds_core_msgs::ClockOffset msg){
  std::unique_lock<std::mutex> lck(pu_data_mutex);
  pu_offset = msg;
  pu_status_u = true;
}
void
Kongsberg::ping_click()
{
  ds_kongsberg_msgs::PingCmd cmd{};
  cmd.request.ping = ping_combo->currentIndex();
  if (ping_cl->call(cmd)){
    ROS_INFO_STREAM("PING COMMANDED: " << cmd.response.action);
  } else {
    ROS_ERROR_STREAM("PING COMMAND FAILED");
  }
}

void
Kongsberg::bist_click()
{
  ds_kongsberg_msgs::BistCmd cmd{};
  cmd.request.bist_command = bist_combo->currentIndex();
  if (bist_cl->call(cmd)){
    ROS_INFO_STREAM("BIST COMMANDED: " << cmd.response.action);
  } else {
    ROS_ERROR_STREAM("BIST COMMAND FAILED");
  }
}

void
Kongsberg::param_click()
{
  ds_kongsberg_msgs::SettingsCmd cmd{};
  cmd.request.setting_name = p_name_edit->text().toStdString();
  cmd.request.setting_value = p_val_edit->text().toStdString();
  if (settings_cl->call(cmd)){
    ROS_INFO_STREAM("PARAM COMMANDED: " << cmd.response.command_sent);
  } else {
    ROS_ERROR_STREAM("PARAM COMMAND FAILED");
  }
}

//int
//Kongsberg::get_ping_enum(std::string cmd)
//{
//  if (cmd=="Ping Start")
//    return ds_kongsberg_msgs::PingCmd::PING_START;
//  else if (cmd=="Ping Stop")
//    return ds_kongsberg_msgs::PingCmd::PING_STOP;
//  else if (cmd=="New File")
//    return ds_kongsberg_msgs::PingCmd::PING_NEWFILE;
//  else if (cmd=="Start Sounder")
//    return ds_kongsberg_msgs::PingCmd::PING_STARTUP;
//  else if (cmd=="Force New File")
//    return ds_kongsberg_msgs::PingCmd::PING_NEWFILE_FORCE;
//  else
//    return 0;
//}

//std::string
//Kongsberg::get_sensor_enum(int cmd){
//  switch (cmd){
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_OK :
//      return "OK";
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_MISSING :
//      return "MISSING";
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_OFF :
//      return "OFF";
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_ACTIVE_BAD :
//      return "ACTIVE BAD";
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_BAD :
//      return "OK";
//    case ds_kongsberg_msgs::KongsbergStatus::SENSOR_UNKNOWN :
//    default:
//      return "Unknown";
//  }
//}

//int
//Kongsberg::get_bist_enum(std::string cmd){
//  if (cmd=="Deck Sequence")
//    return ds_kongsberg_msgs::BistCmd::BIST_ONDECK;
//  else if (cmd=="Inwater Sequence")
//    return ds_kongsberg_msgs::BistCmd::BIST_INWATER;
//  else if (cmd=="Cancel")
//    return ds_kongsberg_msgs::BistCmd::BIST_CANCEL;
//  else if (cmd=="CPU")
//    return ds_kongsberg_msgs::BistCmd::BIST_CPU;
//  else if (cmd=="CBMF")
//    return ds_kongsberg_msgs::BistCmd::BIST_CBMF;
//  else if (cmd=="RX Unit")
//    return ds_kongsberg_msgs::BistCmd::BIST_RX_UNIT;
//  else if (cmd=="TX Unit")
//    return ds_kongsberg_msgs::BistCmd::BIST_TX_UNIT;
//  else if (cmd=="CBMF to CPU")
//    return ds_kongsberg_msgs::BistCmd::BIST_CBMF_CPU;
//  else if (cmd=="RX to CBMF")
//    return ds_kongsberg_msgs::BistCmd::BIST_RX_CBMF;
//  else if (cmd=="RX Channels")
//    return ds_kongsberg_msgs::BistCmd::BIST_RX_CHANNELS;
//  else if (cmd=="TX via RX")
//    return ds_kongsberg_msgs::BistCmd::BIST_TX_VIA_RX;
//  else if (cmd=="RX Noise Level")
//    return ds_kongsberg_msgs::BistCmd::BIST_RX_NOISE_LEVEL;
//  else if (cmd=="RX Noise Spectrum")
//    return ds_kongsberg_msgs::BistCmd::BIST_RX_NOISE_SPECTRUM;
//  else if (cmd=="Software Versions")
//    return ds_kongsberg_msgs::BistCmd::BIST_SOFTWARE_VERSIONS;
//  else if (cmd=="System Info")
//    return ds_kongsberg_msgs::BistCmd::BIST_SYSTEM_INFO;
//  else
//    return 0;
//}

} //namespace