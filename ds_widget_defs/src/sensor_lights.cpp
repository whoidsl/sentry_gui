/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/29/18.
//

#include "../include/ds_widget/sensor_lights.h"

using namespace ds_widget;

Rbite::Rbite(QWidget* parent) : RoundLight("Rbite s", parent), view_needs_update(false), clk(0)
{
}

void Rbite::update(sentry_msgs::ResonRbite in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Rbite::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Rbite::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float rbite = msg.stack_minus_reson_drf_sec;
  set_text(QString::number(rbite));
  if (rbite > 5)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Orp::Orp(QWidget* parent) : RoundLight("Orp V", parent), view_needs_update(false), clk(0)
{
}

void Orp::update(ds_hotel_msgs::A2D2 in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Orp::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Orp::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float orp = msg.raw[0];
  set_text(QString::number(orp));
  if (orp < 0)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Obs::Obs(QWidget* parent) : RoundLight("Obs V", parent), view_needs_update(false), clk(0)
{
}

void Obs::update(ds_hotel_msgs::A2D2 in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Obs::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Obs::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float obs = msg.raw[0];
  set_text(QString::number(obs));
  if (obs < 0)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Opt::Opt(QWidget* parent) : RoundLight("Opt uM", parent), view_needs_update(false), clk(0)
{
}

void Opt::update(ds_sensor_msgs::OxygenConcentration in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Opt::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Opt::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float opt = ds_util::float_round(msg.concentration, 2);
  set_text(QString::number(opt));
  if (opt < 0)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Ctd::Ctd(QWidget* parent) : RoundLight("Ctd m/s", parent), view_needs_update(false), clk(0)
{
}

void Ctd::update(ds_sensor_msgs::Ctd in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Ctd::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Ctd::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float ctd = ds_util::float_round(msg.sound_speed, 0);
  set_text(QString::number(ctd));
  if (ctd < 1200)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Svp::Svp(QWidget* parent) : RoundLight("Svp m/s", parent), view_needs_update(false), clk(0)
{
}

void Svp::update(ds_sensor_msgs::SoundSpeed in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Svp::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Svp::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float svp = ds_util::float_round(msg.speed, 0);
  set_text(QString::number(svp));
  if (svp < 1200)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Dvl::Dvl(QWidget* parent) : RoundLight("Dvl m/s", parent), view_needs_update(false), clk(0)
{
}

void Dvl::update(ds_sensor_msgs::Dvl in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Dvl::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Dvl::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float spd = ds_util::float_round(msg.speed_gnd, 2);
  set_text(QString::number(spd));
  if (spd > 2.0 || spd < -2.0)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Phins::Phins(QWidget* parent) : RoundLight("Phins deg", parent), view_needs_update(false), clk(0)
{
}

void Phins::update(ds_sensor_msgs::Gyro in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Phins::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Phins::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float hdng = ds_util::float_round(180 * msg.heading / 3.14159, 1);
  set_text(QString::number(hdng));
  if (hdng > 361)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

Paro::Paro(QWidget* parent) : RoundLight("Paro m", parent), view_needs_update(false), clk(0)
{
}

void Paro::update(ds_sensor_msgs::DepthPressure in_msg)
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void Paro::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 5)
  {
    toYellow();
  }
  clk += 1;
}

void Paro::update_view()
{
  std::unique_lock<std::mutex> lck(msg_mutex);
  float depth = ds_util::float_round(msg.depth, 1);
  set_text(QString::number(depth));
  if (depth > 10)
  {
    toRed();
  }
  else
  {
    toGreen();
  }
  view_needs_update = false;
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

SensorLights::SensorLights(QWidget* parent) : QWidget(parent)
{
  QGridLayout* grid = new QGridLayout(this);

  rbite = new Rbite();
  orp = new Orp();
  obs = new Obs();
  opt = new Opt();
  ctd = new Ctd();
  svp = new Svp();
  dvl = new Dvl();
  phins = new Phins();
  paro = new Paro();

  grid->addWidget(rbite, 0, 0);
  grid->addWidget(orp, 0, 1);
  grid->addWidget(obs, 0, 2);
  grid->addWidget(opt, 0, 3);
  grid->addWidget(ctd, 0, 4);
  grid->addWidget(svp, 0, 5);
  grid->addWidget(dvl, 0, 6);
  grid->addWidget(phins, 0, 7);
  grid->addWidget(paro, 0, 8);
}

void SensorLights::tick()
{
  rbite->tick();
  orp->tick();
  obs->tick();
  opt->tick();
  ctd->tick();
  svp->tick();
  dvl->tick();
  phins->tick();
  paro->tick();
}
