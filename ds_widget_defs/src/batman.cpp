/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/12/18.
//

#include "../include/ds_widget/batman.h"

using namespace ds_widget;

Batman::Batman(ros::ServiceClient* _charge_cl, ros::ServiceClient* _bat_cl, ros::ServiceClient* _shore_cl,
               QWidget* _parent)
  : QWidget(_parent), charge_cl(_charge_cl), bat_cl(_bat_cl), shore_cl(_shore_cl), timesince(0), time_thresh(30)
{
  setupWidget();
  setupConnections();
}

void Batman::setupWidget()
{
  start_charge_btn = new ds_widget::DsButton("Start Charge", "Start Charge?", this);
  stop_charge_btn = new ds_widget::DsButton("Stop Charge", "Stop Charge?", this);
  bat_on_btn = new ds_widget::DsButton("Bat On", "Turn Batteries on?", this);
  bat_off_btn = new ds_widget::DsButton("Bat Off", "Turn Batteries off?", this);
  shore_on_btn = new ds_widget::DsButton("Shore On", "Turn shore power on?", this);
  shore_off_btn = new ds_widget::DsButton("Shore Off", "Turn shore power off?", this);

  grid = new QGridLayout();

  grid->addWidget(start_charge_btn, 0, 0);
  grid->addWidget(stop_charge_btn, 0, 1);
  grid->addWidget(bat_on_btn, 1, 0);
  grid->addWidget(bat_off_btn, 1, 1);
  grid->addWidget(shore_on_btn, 2, 0);
  grid->addWidget(shore_off_btn, 2, 1);

  setLayout(grid);
}

void Batman::setupConnections()
{
  connect(start_charge_btn, SIGNAL(confirmed()), this, SLOT(start_charge()));
  connect(stop_charge_btn, SIGNAL(confirmed()), this, SLOT(stop_charge()));
  connect(bat_on_btn, SIGNAL(confirmed()), this, SLOT(bat_on()));
  connect(bat_off_btn, SIGNAL(confirmed()), this, SLOT(bat_off()));
  connect(shore_on_btn, SIGNAL(confirmed()), this, SLOT(shore_on()));
  connect(shore_off_btn, SIGNAL(confirmed()), this, SLOT(shore_off()));
}

void Batman::update(ds_hotel_msgs::BatMan msg)
{
}

void Batman::tick()
{
  timesince += 1;
  if (timesince > time_thresh)
  {
  }
}

void Batman::start_charge()
{
  auto cmd = sentry_msgs::ChargeCmd{};
  cmd.request.command = sentry_msgs::ChargeCmd::Request::CHARGE_CMD_CHARGE;
  if (charge_cl->call(cmd))
  {
    ROS_INFO_STREAM("Start charge click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Start charge click failure");
  }
}

void Batman::stop_charge()
{
  auto cmd = sentry_msgs::ChargeCmd{};
  cmd.request.command = sentry_msgs::ChargeCmd::Request::CHARGE_CMD_OFF;
  if (charge_cl->call(cmd))
  {
    ROS_INFO_STREAM("Stop charge click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Stop charge click failure");
  }
}

void Batman::bat_on()
{
  auto cmd = sentry_msgs::PowerCmd{};
  cmd.request.command = sentry_msgs::PowerCmd::Request::POWER_CMD_ON;
  if (bat_cl->call(cmd))
  {
    ROS_INFO_STREAM("Bat on click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Bat on click failure");
  }
}

void Batman::bat_off()
{
  auto cmd = sentry_msgs::PowerCmd{};
  cmd.request.command = sentry_msgs::PowerCmd::Request::POWER_CMD_OFF;
  if (bat_cl->call(cmd))
  {
    ROS_INFO_STREAM("Bat off click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Bat off click failure");
  }
}

void Batman::shore_on()
{
  auto cmd = sentry_msgs::PowerCmd{};
  cmd.request.command = sentry_msgs::PowerCmd::Request::POWER_CMD_ON;
  if (shore_cl->call(cmd))
  {
    ROS_INFO_STREAM("Shore on click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Shore on click failure");
  }
}

void Batman::shore_off()
{
  auto cmd = sentry_msgs::PowerCmd{};
  cmd.request.command = sentry_msgs::PowerCmd::Request::POWER_CMD_OFF;
  if (shore_cl->call(cmd))
  {
    ROS_INFO_STREAM("Shore off click success!");
  }
  else
  {
    ROS_ERROR_STREAM("Shore off click failure");
  }
}
