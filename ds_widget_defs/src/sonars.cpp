/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/29/18.
//

#include "../include/ds_widget/sonars.h"

using namespace ds_widget;

Sonar::Sonar(QWidget* parent) : QWidget(parent), rbite_clk(0), rbite_needs_update(false)
{
  QGridLayout* grid = new QGridLayout(this);
  setup_values();
  setup_timouts();
  grid->addWidget(values_box, 0, 0);
  grid->addWidget(timeout_box, 0, 1);
}

void Sonar::setup_values()
{
  values_box = new QGroupBox("Values", this);
  rbite = new LabelPair("Rbite", values_box);
  QGridLayout* grid = new QGridLayout(values_box);
  grid->addWidget(rbite, 0, 0);
  values_box->setLayout(grid);
}

void Sonar::setup_timouts()
{
  timeout_box = new QGroupBox("Timeout", this);
  rbite_timeout = new LabelPair("Rbite", timeout_box);
  QGridLayout* grid = new QGridLayout(timeout_box);
  grid->addWidget(rbite_timeout, 0, 0);
  timeout_box->setLayout(grid);
}

void Sonar::update_rbite_view()
{
  std::unique_lock<std::mutex> lck(rbite_mutex);
  rbite_timeout->update(QString::number(rbite_clk));
  rbite->update(QString::number(rbite_msg.stack_minus_reson_drf_sec));
  rbite_needs_update = false;
}

void Sonar::update_rbite(sentry_msgs::ResonRbite in_rbite)
{
  std::unique_lock<std::mutex> lck(rbite_mutex);
  rbite_msg = in_rbite;
  rbite_clk = 0;
  rbite_needs_update = true;
}

void Sonar::tick()
{
  if (rbite_needs_update)
  {
    update_rbite_view();
  }
  else
  {
    rbite_timeout->update(QString::number(rbite_clk));
  }
  rbite_clk += 1;
}
