/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/15/18.
//

#include "../include/ds_widget/bat_grid.h"

// Row index for battery grid model
enum BAT_GRID_ROW {
  BAT_GRID_ROW_ID = 0,
  BAT_GRID_ROW_PCT,
  BAT_GRID_ROW_STATUS,
  //BAT_GRID_ROW_SHUNT,
  BAT_GRID_ROW_SAFETY,
  BAT_GRID_ROW_CHG_AHR,
  BAT_GRID_ROW_TOTAL_V,
  BAT_GRID_ROW_MAX_CELL_V,
  BAT_GRID_ROW_MIN_CELL_V,
  BAT_GRID_ROW_SWITCH_T,
  BAT_GRID_ROW_MAX_CELL_T,
  BAT_GRID_ROW_MIN_CELL_T,
  BAT_GRID_ROW_CHG_ID,
  BAT_GRID_ROW_N, // number of rows in battery grid
};

const std::map<int, const char*> BAT_GRID_ROW_LABELS = {
  {BAT_GRID_ROW_ID, "Battery"},
  {BAT_GRID_ROW_PCT, "%"},
  {BAT_GRID_ROW_STATUS, "Status"},
  {BAT_GRID_ROW_SAFETY, "Safety"},
  {BAT_GRID_ROW_CHG_AHR, "Chg Ahr"},
  {BAT_GRID_ROW_TOTAL_V, "Total V"},
  {BAT_GRID_ROW_MAX_CELL_V, "Max Cell V"},
  {BAT_GRID_ROW_MIN_CELL_V, "Min Cell V"},
  {BAT_GRID_ROW_SWITCH_T, "Switch T"},
  {BAT_GRID_ROW_MAX_CELL_T, "Max Cell T"},
  {BAT_GRID_ROW_MIN_CELL_T, "Min Cell T"},
  //{BAT_GRID_ROW_SHUNT, "Shunt Cmd"},
  {BAT_GRID_ROW_CHG_ID, "Charger"},
};

// Row index for charger grid model
enum CHG_GRID_ROW {
  CHG_GRID_ROW_ID = 0,
  CHG_GRID_ROW_MODE,
  CHG_GRID_ROW_V_SET,
  CHG_GRID_ROW_V_MEAS,
  CHG_GRID_ROW_C_SET,
  CHG_GRID_ROW_C_MEAS,
  CHG_GRID_ROW_FRONTPANEL,
  CHG_GRID_ROW_OUTPUT,
  CHG_GRID_ROW_STATUS,
  CHG_GRID_ROW_N, // number of rows in charger grid
};

const std::map<int, const char*> CHG_GRID_ROW_LABELS = {
  {CHG_GRID_ROW_ID, "Charger"},
  {CHG_GRID_ROW_MODE, "Mode"},
  {CHG_GRID_ROW_V_SET, "V set"},
  {CHG_GRID_ROW_V_MEAS, "V meas"},
  {CHG_GRID_ROW_C_SET, "C set"},
  {CHG_GRID_ROW_C_MEAS, "C meas"},
  {CHG_GRID_ROW_FRONTPANEL, "Frontpanel"},
  {CHG_GRID_ROW_OUTPUT, "Output"},
  {CHG_GRID_ROW_STATUS, "Status"},
};

using namespace ds_widget;

GridModel::GridModel(QString _type, QWidget* parent) : QWidget(parent), type(_type), timesince(0), time_thresh(30)
{
  view_update_needed = false;
  if (type == "bat" || type == "battery")
  {
    bat_init();
  }
  if (type == "chg" || type == "charger")
  {
    chg_init();
  }
}

void GridModel::bat_init()
{
  for(const auto& it: BAT_GRID_ROW_LABELS) {
    row_names.emplace_back(it.second);
  }
}

void GridModel::chg_init()
{
  for(const auto& it: CHG_GRID_ROW_LABELS) {
    row_names.emplace_back(it.second);
  }
}

void GridModel::batman_update(ds_hotel_msgs::BatMan msg)
{
  if(msg.charger_ids.empty())
  {
    for(auto it = std::begin(table_data); it != std::end(table_data); ++it)
    {
      it->second.resize(BAT_GRID_ROW_N);
      it->second[BAT_GRID_ROW_CHG_ID].clear();
    } 
    return;
  }

  for(auto battery_id = 1; battery_id < msg.charger_ids.size() + 1; battery_id++)
  {
     const auto charger_id = msg.charger_ids[battery_id - 1];
     try
     {
        auto& data = table_data.at(battery_id);
	data.resize(BAT_GRID_ROW_N);
	data[BAT_GRID_ROW_CHG_ID].setNum(charger_id);
     }  
     catch (std::out_of_range& ex_)
     {
         continue;
     }
  }
}
void GridModel::bat_update(ds_hotel_msgs::Battery msg)
{
  // std::unique_lock<std::mutex> lck(table_data_mutex);
  auto& data = table_data[msg.idnum];
  data.resize(BAT_GRID_ROW_N);
  data[BAT_GRID_ROW_ID].setNum(msg.idnum);
  data[BAT_GRID_ROW_PCT].setNum(msg.percentFull, 'f', 2);
  if (msg.charging) {
    if (msg.balanceEnable)
      data[BAT_GRID_ROW_STATUS] = "Balancing";
    else
      data[BAT_GRID_ROW_STATUS] = "Charging";
  }
  else if (msg.discharging)
    data[BAT_GRID_ROW_STATUS] = "Discharging";
  else
    data[BAT_GRID_ROW_STATUS] = "Off";
  data[BAT_GRID_ROW_SAFETY] = QString::fromStdString(msg.safetyString);
  data[BAT_GRID_ROW_CHG_AHR].setNum(msg.chargeAh, 'f', 2);
  data[BAT_GRID_ROW_TOTAL_V].setNum(msg.totalVoltage, 'f', 2);
  data[BAT_GRID_ROW_MAX_CELL_V].setNum(msg.maxCellVoltage, 'f', 3);
  data[BAT_GRID_ROW_MIN_CELL_V].setNum(msg.minCellVoltage, 'f', 3);
  data[BAT_GRID_ROW_SWITCH_T].setNum(msg.switchTemp);
  data[BAT_GRID_ROW_MAX_CELL_T].setNum(msg.maxCellTemp);
  data[BAT_GRID_ROW_MIN_CELL_T].setNum(msg.minCellTemp);

  /*
  if (msg.balanceEnable)
    data[BAT_GRID_ROW_SHUNT] = QString("%1").arg(msg.balanceCommandId, 14, 10, QChar('0'));
  else
    data[BAT_GRID_ROW_SHUNT] = "";
  */
  timesince = 0;
  //table_data[msg.idnum] = std::move(data);
  emit populate();
  view_update_needed = true;

}

void GridModel::chg_update(ds_hotel_msgs::PowerSupply msg)
{
  // std::lock_guard<std::mutex> lck(table_data_mutex);
  auto& data = table_data[msg.idnum];
  data.resize(CHG_GRID_ROW_N);
  data[CHG_GRID_ROW_ID].setNum(msg.idnum);
  if (msg.constant_current)
    data[CHG_GRID_ROW_MODE] = "CC";
  else if (msg.constant_voltage)
    data[CHG_GRID_ROW_MODE] = "CV";
  else if (msg.output_enable) {
    data[CHG_GRID_ROW_MODE] = "Unknown";
    ROS_WARN("unknown charger mode. constant_current and constant_voltage are both false for charger %d", msg.idnum);
  }
  else
    data[CHG_GRID_ROW_MODE] = "";
  data[CHG_GRID_ROW_V_SET].setNum(msg.prog_volts, 'f', 2);
  data[CHG_GRID_ROW_V_MEAS].setNum(msg.meas_volts, 'f', 2);
  data[CHG_GRID_ROW_C_SET].setNum(msg.prog_amps, 'f', 2);
  data[CHG_GRID_ROW_C_MEAS].setNum(msg.meas_amps, 'f', 2);
  if (msg.frontpanel_locked)
    data[CHG_GRID_ROW_FRONTPANEL] = "Locked";
  else
    data[CHG_GRID_ROW_FRONTPANEL] = "Unlocked";

  if (msg.output_enable)
    data[CHG_GRID_ROW_OUTPUT] = "Enabled";
  else
    data[CHG_GRID_ROW_OUTPUT] = "Disabled";

  data[CHG_GRID_ROW_STATUS] = QString::fromStdString(msg.status_msg);

  timesince = 0;
  //table_data[msg.idnum] = std::move(data);
  emit populate();
  view_update_needed = true;
}

void GridModel::tick(const ros::TimerEvent&)
{
  timesince += 1;
  if (timesince > time_thresh)
  {
  }
  //    if (view_update_needed) {
  //        emit populate();
  //        view_update_needed = false;
  //    }
}

GridView::GridView(GridModel* _model, QWidget* _parent) : model(_model), QWidget(_parent)
{
  // std::lock_guard<std::mutex> lck(model->table_data_mutex);
  // set up row labels
  grid = new QGridLayout();
  for (int i = 0; i < model->row_names.size(); i++)
  {
    makeLabelAt(i, 0, model->row_names[i]);
  }
  setLayout(grid);
  connect(model, SIGNAL(populate()), this, SLOT(populate()));
}

void GridView::makeLabelAt(int row, int column, QString str)
{
  QLabel* lbl = new QLabel(str, this);
  lbl->setObjectName(QString::number(row) + "+" + QString::number(column));
  grid->addWidget(lbl, row, column);
}

void GridView::setTextAt(int row, int column, QString str)
{
  // Get label for text, or make a new label if needed
  QLabel* lbl = this->findChild<QLabel*>(QString::number(row) + "+" + QString::number(column));
  if (lbl != nullptr)
  {
    lbl->setText(str);
    return;
  }
  else
  {
    makeLabelAt(row, column, str);
  }
}

void GridView::populate()
{
  // std::lock_guard<std::mutex> lck(model->table_data_mutex);
  auto column = 1;
  for (const auto& it: model->table_data)
  {
    const auto data = it.second;
    for (auto row = 0; row < data.size(); row++)
    {
      setTextAt(row, column, data[row]);
    }
    column += 1;
  }
}
