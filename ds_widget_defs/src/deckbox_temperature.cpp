/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by llindzey on 2 October 2018.

#include "../include/ds_widget/deckbox_temperature.h"

namespace ds_widget
{
// TODO(LEL): I don't love these hard-coded magic numbers.
//    Move to parameters? (Fed from where? Other widgets are a mix of
//    magic numbers and hard-coded options in sentrysitter/module.cpp)
DeckboxTemperature::DeckboxTemperature(QWidget* parent)
  : QWidget(parent), time_since_update_(0), time_threshold_(10), temperature_threshold_(25), view_needs_update_(false)
{
  setupWidget();
}

// QUESTION(LEL): Is it necessary to have a grid layout to contain a
//    single-element widget?
void DeckboxTemperature::setupWidget()
{
  temperature_light_ = new RoundLight("Deck T");

  grid_ = new QGridLayout();
  grid_->addWidget(temperature_light_, 0, 0);
  setLayout(grid_);
}

void DeckboxTemperature::updateView()
{
  std::unique_lock<std::mutex> lock(model_mutex_);
  temperature_light_->toGreen();

  auto text = QString::number(ds_util::float_round(status_msg_.temperature, 2));
  temperature_light_->set_text(text);

  if (status_msg_.temperature > temperature_threshold_)
  {
    temperature_light_->toRed();
  }
  view_needs_update_ = false;
}

void DeckboxTemperature::statusCallback(sentry_msgs::DeckboxStatus msg)
{
  std::unique_lock<std::mutex> lock(model_mutex_);
  time_since_update_ = 0;
  status_msg_ = msg;
  view_needs_update_ = true;
}

void DeckboxTemperature::tick()
{
  time_since_update_ += 1;
  if (time_since_update_ > time_threshold_)
  {
    temperature_light_->toYellow();
  }
  if (view_needs_update_)
  {
    updateView();
  }
}

}  // namespace ds_widget
