/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by llindzey on 2 October 2018

#include "../include/ds_widget/deckbox_burnwires.h"

namespace ds_widget
{
DeckboxBurnwires::DeckboxBurnwires(ros::ServiceClient* client, QWidget* parent)
  : QGroupBox("Burnwires", parent)
  , measurement_client_(client)
  , name_("Burnwires")
  , view_needs_update_(false)
  , time_since_update_(0)
{
  setupWidget();
  setupConnections();
}

void DeckboxBurnwires::setupWidget()
{
  QVBoxLayout* vbox = new QVBoxLayout();

  measurement_button_ = new DsButton("Take Measurement", "Take Measurement?", this, true);
  vbox->addWidget(measurement_button_);
  QGridLayout* grid = new QGridLayout();
  QLabel* burnwires_age_text = new QLabel("measurement age:");
  burnwires_age_value_ = new QLabel("");
  QLabel* port_v_text = new QLabel("Port V:");
  port_v_value_ = new QLabel("");
  QLabel* port_a_text = new QLabel("Port A:");
  port_a_value_ = new QLabel("");
  QLabel* starboard_v_text = new QLabel("Stbd V:");
  starboard_v_value_ = new QLabel("");
  QLabel* starboard_a_text = new QLabel("Stbd A:");
  starboard_a_value_ = new QLabel("");
  QLabel* descent_v_text = new QLabel("Desc V:");
  descent_v_value_ = new QLabel("");
  QLabel* descent_a_text = new QLabel("Desc A:");
  descent_a_value_ = new QLabel("");
  grid->addWidget(burnwires_age_text, 0, 0, 1, 2);
  grid->addWidget(burnwires_age_value_, 0, 3, 1, 1);
  grid->addWidget(port_v_text, 1, 0);
  grid->addWidget(port_v_value_, 1, 1);
  grid->addWidget(port_a_text, 1, 2);
  grid->addWidget(port_a_value_, 1, 3);
  grid->addWidget(starboard_v_text, 2, 0);
  grid->addWidget(starboard_v_value_, 2, 1);
  grid->addWidget(starboard_a_text, 2, 2);
  grid->addWidget(starboard_a_value_, 2, 3);
  grid->addWidget(descent_v_text, 3, 0);
  grid->addWidget(descent_v_value_, 3, 1);
  grid->addWidget(descent_a_text, 3, 2);
  grid->addWidget(descent_a_value_, 3, 3);

  vbox->addLayout(grid);
  setLayout(vbox);
}

void DeckboxBurnwires::setupConnections()
{
  connect(measurement_button_, SIGNAL(confirmed()), this, SLOT(clickSampleBurnwires()));
}

void DeckboxBurnwires::clickSampleBurnwires()
{
  // send service request; if it returns failure, pop up dialogue.
  // If it succeeds, change color of QLabels showing measurements
  // to indicate that the data needs to be updated.
  sentry_msgs::SampleBurnwiresCmdRequest request;
  sentry_msgs::SampleBurnwiresCmdResponse response;

  measurement_client_->call(request, response);
  if (!response.accepted)
  {
    QMessageBox box(this);
    box.setWindowTitle("Service Call Rejected");
    box.setText(QString::fromStdString(response.text));
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
  } else {
    std::unique_lock<std::mutex> lock(msg_mutex_);
    waiting_for_measurement_ = true;
    view_needs_update_ = true;
  }
}

void DeckboxBurnwires::updateView()
{
  std::unique_lock<std::mutex> lock(msg_mutex_);
  burnwires_age_value_->setText(QString::number(time_since_update_) + " secs");
  if (view_needs_update_)
  {
    if (waiting_for_measurement_)
    {
      port_v_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
      port_a_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
      starboard_v_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
      starboard_a_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
      descent_v_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
      descent_a_value_->setStyleSheet("QLabel { background-color : yellow; color : black; }");
    } else {
      port_v_value_->setStyleSheet("");
      port_a_value_->setStyleSheet("");
      starboard_v_value_->setStyleSheet("");
      starboard_a_value_->setStyleSheet("");
      descent_v_value_->setStyleSheet("");
      descent_a_value_->setStyleSheet("");
    }

    // populate with new message stuff
    port_v_value_->setText(QString::number(msg_.port_voltage) + " V");
    port_a_value_->setText(QString::number(msg_.port_current) + " mA");
    starboard_v_value_->setText(QString::number(msg_.starboard_voltage) + " V");
    starboard_a_value_->setText(QString::number(msg_.starboard_current) + " mA");
    descent_v_value_->setText(QString::number(msg_.descent_voltage) + " V");
    descent_a_value_->setText(QString::number(msg_.descent_current) + " mA");
    view_needs_update_ = false;
  }
}

void DeckboxBurnwires::measurementCallback(sentry_msgs::BurnwiresMeasurement msg)
{
  std::unique_lock<std::mutex> lock(msg_mutex_);
  time_since_update_ = 0;
  waiting_for_measurement_ = false;
  msg_ = msg;
  view_needs_update_ = true;
}

void DeckboxBurnwires::tick()
{
  time_since_update_ += 1;
  updateView();
}

}  // namespace ds_widget
