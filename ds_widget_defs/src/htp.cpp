/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/5/18.
//

#include "../include/ds_widget/htp.h"

using namespace ds_widget;

Htp::Htp(QString _name, int _h_thresh, int _t_thresh, int _t_warn_thresh, int _p_thresh, QWidget* _parent)
  : QWidget(_parent), name(_name), timesince(0), h_thresh(_h_thresh), t_thresh(_t_thresh), t_warn_thresh(_t_warn_thresh), p_thresh(_p_thresh)
{
  view_needs_update = false;
  h = new RoundLight(name + " H");
  t = new RoundLight(name + " T");
  p = new RoundLight(name + " P");
  grid = new QGridLayout();
  grid->addWidget(h, 0, 0);
  grid->addWidget(t, 0, 1);
  grid->addWidget(p, 0, 2);
  setLayout(grid);
}

void Htp::update(ds_hotel_msgs::HTP msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  timesince = 0;
  model = msg;
  view_needs_update = true;
}

void Htp::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);

  h->set_text(QString::number(ds_util::float_round(model.humidity, 2)));
  t->set_text(QString::number(ds_util::float_round(model.temperature, 2)));
  p->set_text(QString::number(ds_util::float_round(model.pressure, 2)));

  if (model.humidity > h_thresh)
  {
    h->toRed();
  }
  else
  {
    h->toGreen();
  }
  if (model.temperature > t_thresh)
  {
    t->toRed();
  }
  else if (model.temperature > t_warn_thresh && model.temperature < t_thresh)
  {
    t->toOrange();
  }
  else
  {
    t->toGreen();
  }
  if (model.pressure > p_thresh)
  {
    p->toRed();
  }
  else
  {
    p->toGreen();
  }
  view_needs_update = false;
}

void Htp::tick()
{
  timesince += 1;
  if (timesince > 60)
  {
    h->toYellow();
    t->toYellow();
    p->toYellow();
  }
  if (view_needs_update)
  {
    update_view();
  }
}