/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/16/18.
//

#include "../include/ds_widget/phins_compass.h"

namespace ds_widget
{
PhinsCompass::PhinsCompass(QWidget* parent) : QWidget(parent), view_needs_update(false), clk(0), color("yellow")
{
  QGridLayout* grid = new QGridLayout();
  compass = new Compass(this);
  QFont f("Arial", 30, QFont::Bold);
  label = new QLabel(this);
  label->setFont(f);
  grid->addWidget(compass, 0, 0, 8, 4);
  grid->addWidget(label, 8, 0, 1, 1);
  setLayout(grid);
}

void PhinsCompass::update_model(ds_sensor_msgs::Gyro in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  msg = in_msg;
  clk = 0;
  view_needs_update = true;
}

void PhinsCompass::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);
  float angle = ds_util::float_round(msg.heading * 180.0 / 3.14159, 1);
  compass->setAngle(angle);
  label->setText(QString::number(angle) + " deg");
  if (color != "green")
  {
    color = "green";
  }
  view_needs_update = false;
}
void PhinsCompass::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  else if (clk > 20 && color != "yellow")
  {
    color = "yellow";
  }
  clk += 1;
}
}
