/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by llindzey on 2 October 2018

#include "../include/ds_widget/deckbox_relays.h"

namespace ds_widget
{
DeckboxRelays::DeckboxRelays(ros::ServiceClient* power_client, ros::ServiceClient* ethernet_client, QWidget* parent)
  : QGroupBox("DeckboxRelays", parent)
  , name_("Deckbox Relays")
  , power_client_(power_client)
  , ethernet_client_(ethernet_client)
  , time_since_update_(0)
  , time_threshold_(10)
  , view_needs_update_(true)
{
  setupWidget();
  setupConnections();
}

void DeckboxRelays::setupWidget()
{
  /**
  power_light_ = new RoundLight("Power");
  power_light_->set_text("");  // get rid of "hello"
  ethernet_light_ = new RoundLight("Ethernet");
  ethernet_light_->set_text("");  // get rid of "hello"
  **/
  power_on_button_ = new DsButton("Power On", "Close power relay?", this, true);
  power_off_button_ = new DsButton("Power Off", "Open power relay?", this, true);
  ethernet_on_button_ = new DsButton("Ethernet On", "Connect Ethernet?", this, true);
  ethernet_off_button_ = new DsButton("Ethernet Off", "Disconnect Ethernet?", this, true);
  QGridLayout* grid = new QGridLayout();
  //grid->addWidget(power_light_, 0, 0);
  grid->addWidget(power_on_button_, 0, 1);
  grid->addWidget(power_off_button_, 0, 2);
  //grid->addWidget(ethernet_light_, 1, 0);
  grid->addWidget(ethernet_on_button_, 1, 1);
  grid->addWidget(ethernet_off_button_, 1, 2);
  setLayout(grid);
}

void DeckboxRelays::setupConnections()
{
  // TODO(LEL): There should be a better way to do this to make it generic
  //    to types, but I don't know how off the top of my head ...
  //    https://stackoverflow.com/questions/5153157/passing-an-argument-to-a-slot
  //    looks like it gets partway there with functors, but it wouldn't compile
  connect(power_on_button_, SIGNAL(confirmed()), this, SLOT(clickPowerOn()));
  connect(power_off_button_, SIGNAL(confirmed()), this, SLOT(clickPowerOff()));
  connect(ethernet_on_button_, SIGNAL(confirmed()), this, SLOT(clickEthernetOn()));
  connect(ethernet_off_button_, SIGNAL(confirmed()), this, SLOT(clickEthernetOff()));
}

void DeckboxRelays::clickPowerOn()
{
  clickPower(true);
}

void DeckboxRelays::clickPowerOff()
{
  clickPower(false);
}

void DeckboxRelays::clickPower(bool close_relay)
{
  // send service request; if it returns failure, pop up dialogue.
  sentry_msgs::DeckboxRelayCmdRequest request;
  sentry_msgs::DeckboxRelayCmdResponse response;
  if (close_relay)
  {
    request.command = request.ON;
  }
  else
  {
    request.command = request.OFF;
  }
  power_client_->call(request, response);
  if (!response.accepted)
  {
    QMessageBox box(this);
    box.setWindowTitle("Service Call Rejected");
    box.setText(QString::fromStdString(response.text));
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
  }
}

void DeckboxRelays::clickEthernetOn()
{
  clickEthernet(true);
}
void DeckboxRelays::clickEthernetOff()
{
  clickEthernet(false);
}

void DeckboxRelays::clickEthernet(bool close_relay)
{
  sentry_msgs::DeckboxRelayCmdRequest request;
  sentry_msgs::DeckboxRelayCmdResponse response;
  if (close_relay)
  {
    request.command = request.ON;
  }
  else
  {
    request.command = request.OFF;
  }
  ethernet_client_->call(request, response);
  if (!response.accepted)
  {
    QMessageBox box(this);
    box.setWindowTitle("Service Call Rejected");
    box.setText(QString::fromStdString(response.text));
    box.setStandardButtons(QMessageBox::Ok);
    box.setDefaultButton(QMessageBox::Ok);
    box.exec();
  }
}

void DeckboxRelays::statusCallback(sentry_msgs::DeckboxStatus msg)
{
  std::unique_lock<std::mutex> lock(msg_mutex_);
  time_since_update_ = 0;
  status_msg_ = msg;
  view_needs_update_ = true;
}

void DeckboxRelays::updateView()
{
  std::unique_lock<std::mutex> lock(msg_mutex_);
  if (status_msg_.power_on)
  {
    //power_light_->toGreen();
    power_on_button_->setStyleSheet("QPushButton { background-color : lightGreen; color : black; }");
    power_off_button_->setStyleSheet("");
  }
  else
  {
    //power_light_->toRed();
    power_off_button_->setStyleSheet("QPushButton { background-color : red; color : black; }");
    power_on_button_->setStyleSheet("");
  }
  if (status_msg_.ethernet_on)
  {
    //ethernet_light_->toGreen();
    ethernet_on_button_->setStyleSheet("QPushButton { background-color : lightGreen; color : black; }");
    ethernet_off_button_->setStyleSheet("");
  }
  else
  {
    //ethernet_light_->toRed();
    ethernet_off_button_->setStyleSheet("QPushButton { background-color : red; color : black; }");
    ethernet_on_button_->setStyleSheet("");
  }
  view_needs_update_ = false;
}

void DeckboxRelays::tick()
{
  time_since_update_ += 1;
  if (time_since_update_ > time_threshold_)
  {
    //power_light_->toYellow();
    //ethernet_light_->toYellow();
    ethernet_on_button_->setStyleSheet("QPushButton { background-color : yellow; color : black; }");        
    ethernet_off_button_->setStyleSheet("QPushButton { background-color : yellow; color : black; }");    
    power_on_button_->setStyleSheet("QPushButton { background-color : yellow; color : black; }");        
    power_off_button_->setStyleSheet("QPushButton { background-color : yellow; color : black; }");
  }
  if (view_needs_update_)
  {
    updateView();
  }
}

}  // namespace ds_widget
