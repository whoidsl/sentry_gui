/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/18/18.
//

#include "../include/ds_widget/abort.h"

namespace ds_widget
{
Abort::Abort(ros::ServiceClient* _cl, QWidget* parent)
  : cl(_cl)
  , QGroupBox(parent)
  , view_needs_update(false)
  , is_aborted(false)
  , is_enabled(true)
  , timeout_255(0)
  , timeout_10(0)
{
  //        QGroupBox *box = new QGroupBox(this);
  box_color = "black";
  QGridLayout* grid = new QGridLayout();
  abort_btn = new RedGreen("Abort", this, true);     // CONFIRM BEFORE ABORTING
  enable_btn = new RedGreen("Enable", this, false);  // DONT CONFIRM BEFORE SAFESENTRY
  timeout_lbl = new QLabel("");
  grid->addWidget(abort_btn, 0, 0, 1, 3);
  grid->addWidget(enable_btn, 1, 0, 1, 3);
  grid->addWidget(timeout_lbl, 0, 3, 1, 1);
  setLayout(grid);
  //        setWidget(box);
  connect(abort_btn, SIGNAL(confirmed()), this, SLOT(click_abort()));
  connect(enable_btn, SIGNAL(confirmed()), this, SLOT(click_enable()));
}

void Abort::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);
  if (model.abort)
  {
    // If aborting
    abort_btn->makeRed();
  }
  else if (model.ttl < 0)
  {
    // If short deadman is disabled
    abort_btn->makeYellow();
    if (box_color != "red")
    {
      setStyleSheet("QGroupBox {  border: 3px solid red;}");
      box_color = "red";
    }
  }
  else
  {
    // Not aborting, short deadman is ticking
    abort_btn->makeGreen();
    if (box_color != "green")
    {
      setStyleSheet("QGroupBox {  border: 1px solid green;}");
      box_color = "green";
    }
  }
  is_aborted = model.abort;

  if (model.enable)
  {
    enable_btn->makeGreen();
  }
  else if (model.ttl < 0)
  {
    enable_btn->makeYellow();
  }
  else
  {
    enable_btn->makeRed();
  }
  is_enabled = model.enable;
  view_needs_update = false;
}

void Abort::enable()
{
  auto cmd = ds_hotel_msgs::AbortCmd{};
  cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ENABLE;
  if (cl->call(cmd))
  {
    ROS_INFO_STREAM("Enable cmd sent");
  }
  else
  {
    ROS_ERROR_STREAM("Enable cmd fail!");
  }
}

void Abort::disable()
{
  auto cmd = ds_hotel_msgs::AbortCmd{};
  cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_DISABLE;
  if (cl->call(cmd))
  {
    ROS_INFO_STREAM("Disable cmd sent");
  }
  else
  {
    ROS_ERROR_STREAM("Disable cmd fail!");
  }
}

void Abort::abort()
{
  auto cmd = ds_hotel_msgs::AbortCmd{};
  cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ABORT;
  if (cl->call(cmd))
  {
    ROS_INFO_STREAM("Abort cmd sent");
  }
  else
  {
    ROS_ERROR_STREAM("Abort cmd fail!");
  }
}

void Abort::unabort()
{
  auto cmd = ds_hotel_msgs::AbortCmd{};
  cmd.request.command = ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_DONTABORT;
  if (cl->call(cmd))
  {
    ROS_INFO_STREAM("Unabort cmd sent");
  }
  else
  {
    ROS_ERROR_STREAM("Unabort cmd fail!");
  }
}

void Abort::click_abort()
{
  if (is_aborted)
  {
    unabort();
  }
  else if (!is_aborted)
  {
    abort();
  }
}

void Abort::click_enable()
{
  if (is_enabled)
  {
    disable();
  }
  else if (!is_enabled)
  {
    enable();
  }
}

void Abort::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  // If the short deadman is software disabled, then flash the danger colors.
  if (box_color == "red" || box_color == "orange")
  {
    if (box_color == "red")
    {
      setStyleSheet("QGroupBox {  border: 3px solid orange;}");
      box_color = "orange";
    }
    else
    {
      setStyleSheet("QGroupBox {  border: 3px solid red;}");
      box_color = "red";
    }
  }
  timeout_10++;
  if (timeout_10 >= 11)
  {
    abort_btn->makeYellow();
    enable_btn->makeYellow();
    setStyleSheet("QGroupBox {  border: 1px solid yellow;}");
    box_color = "yellow";
  }

  if (is_aborted)
  {
    if (timeout_255 != 255)
    {
      timeout_255++;
    }
    timeout_lbl->setText(QString::number(timeout_255));
  }
  else
  {
    timeout_lbl->setText("");
  }
}

void Abort::update(ds_core_msgs::Abort in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  model = in_msg;
  view_needs_update = true;
  timeout_10 = 0;
  if (model.abort == false)
  {
    timeout_255 = 0;
  }
}
}
