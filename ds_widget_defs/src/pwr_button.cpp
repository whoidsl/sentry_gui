/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/20/18.
//

#include "ds_widget/pwr_button.h"
#include <QDialogButtonBox>
#include <QDialog>
#include <QMessageBox>
#include <QLabel>
#include <QString>
#include <QGridLayout>

namespace ds_widget
{
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void PwrButton::pwr_click()
{
  ROS_INFO_STREAM("pwr click!");
  if (on && confirm_off)
  {
    confirm_launch("Turn off ");
  }
  else if (!on && confirm_on)
  {
    confirm_launch("Turn on ");
  }
  else
  {
    click_action();
  }
}

void PwrButton::confirm_launch(QString str)
{
  QString btn_str = str + name + "?";
  QMessageBox msgBox(parent);
  msgBox.setWindowTitle("Confirm " + name);
  msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
  msgBox.setDefaultButton(QMessageBox::Ok);
  msgBox.setText(btn_str);
  int ret = msgBox.exec();

  switch (ret)
  {
    case QMessageBox::Ok:
      // Save was clicked
      click_action();
      break;
    case QMessageBox::Cancel:
      // Cancel was clicked
      break;
    default:
      // should never be reached
      break;
  }
}

void PwrButton::click_action()
{
  sentry_msgs::PWRCmd cmd;
  cmd.request.address = address;
  if (!on)
  {
    cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_ON;
  }
  else
  {
    cmd.request.command = sentry_msgs::PWRCmd::Request::PWR_CMD_OFF;
  }
  if (cl->call(cmd))
  {
    ROS_INFO_STREAM("Command sent to " << address << "!");
  }
  else
  {
    ROS_ERROR_STREAM("Command send failed");
  }
  launched = false;
}

void PwrButton::set_color(bool is_on)
{
  if (is_on == on && color != "yellow")
  {
    // Don't make changes if the state is already correct
    return;
  }
  if (is_on)
  {
    makeGreen();
  }
  else
  {
    makeRed();
  }
}

void PwrButton::makeGreen()
{
  if (color == "green")
  {
    return;
  }
  color = "green";
  on = true;
  setStyleSheet("QPushButton { background-color: lightGreen; color: black; }");
  ROS_INFO_STREAM("button " << name.toStdString() << " at " << address << " is now on");
}

void PwrButton::makeRed()
{
  if (color == "red")
  {
    return;
  }
  color = "red";
  on = false;
  setStyleSheet("QPushButton { background-color: red;  color: black;}");
  ROS_INFO_STREAM("button " << name.toStdString() << " at " << address << " is now off");
}

void PwrButton::makeYellow()
{
  if (color == "yellow")
  {
    return;
  }
  color = "yellow";
  setStyleSheet("QPushButton { background-color: yellow;  color: black;}");
  ROS_INFO_STREAM("button " << name.toStdString() << " at " << address << " has old info");
}
}
