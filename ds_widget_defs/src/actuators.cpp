/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 5/13/18.
//

#include "../include/ds_widget/actuators.h"

using namespace ds_widget;

Thruster::Thruster(QString name, QWidget* parent) : QWidget(parent), timesince(0), view_needs_update(false)
{
  QGridLayout* grid = new QGridLayout(this);
  QLabel* name_lbl = new QLabel(name, this);
  cmd_lbl = new QLabel(this);
  val_lbl = new QLabel(this);
  grid->addWidget(name_lbl, 0, 0);
  grid->addWidget(cmd_lbl, 1, 0);
  grid->addWidget(val_lbl, 2, 0);
}

void Thruster::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  timesince++;
}

void Thruster::update(ds_actuator_msgs::ThrusterState in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  model = in_msg;
  timesince = 0;
  view_needs_update = true;
}

void Thruster::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);
  cmd_lbl->setText(QString::number(ds_util::float_round(model.cmd_value, 2)));
  val_lbl->setText(QString::number(ds_util::float_round(model.actual_value, 2)));
  view_needs_update = false;
}

Servo::Servo(QString name, QWidget* parent) : QWidget(parent), timesince(0), view_needs_update(false)
{
  QGridLayout* grid = new QGridLayout(this);
  QLabel* name_lbl = new QLabel(name, this);
  cmd_lbl = new QLabel(this);
  val_lbl = new QLabel(this);
  grid->addWidget(name_lbl, 0, 0);
  grid->addWidget(cmd_lbl, 1, 0);
  grid->addWidget(val_lbl, 2, 0);
}

void Servo::tick()
{
  if (view_needs_update)
  {
    update_view();
  }
  timesince++;
}

void Servo::update(ds_actuator_msgs::ServoState in_msg)
{
  std::unique_lock<std::mutex> lck(model_mutex);
  model = in_msg;
  timesince = 0;
  view_needs_update = true;
}

void Servo::update_view()
{
  std::unique_lock<std::mutex> lck(model_mutex);
  cmd_lbl->setText(QString::number(ds_util::float_round(180 * model.cmd_radians / 3.14159, 0)) + " deg");
  val_lbl->setText(QString::number(ds_util::float_round(180 * model.actual_radians / 3.14159, 0)) + " deg");
  view_needs_update = false;
}

Actuators::Actuators(QWidget* parent)
  : QWidget(parent), servo_timesince(0), thruster_timesince(0), servo_timeout(false), thruster_timeout(false)
{
  QGridLayout* grid = new QGridLayout(this);
  QGroupBox* servos = new QGroupBox("Servos", this);
  QGridLayout* servo_grid = new QGridLayout(servos);
  fwd = new Servo("Fwd", servos);
  aft = new Servo("Aft", servos);
  servo_grid->addWidget(fwd, 0, 0);
  servo_grid->addWidget(aft, 1, 0);
  QGroupBox* thrusters = new QGroupBox("Thrusters", this);
  QGridLayout* thruster_grid = new QGridLayout(thrusters);
  fwd_stbd = new Thruster("Fwd Stbd", thrusters);
  fwd_port = new Thruster("Fwd Port", thrusters);
  aft_stbd = new Thruster("Aft Stbd", thrusters);
  aft_port = new Thruster("Aft Port", thrusters);
  thruster_grid->addWidget(fwd_stbd, 0, 1);
  thruster_grid->addWidget(fwd_port, 0, 0);
  thruster_grid->addWidget(aft_stbd, 1, 1);
  thruster_grid->addWidget(aft_port, 1, 0);
  grid->addWidget(servos, 0, 2);
  grid->addWidget(thrusters, 0, 0, 1, 2);
}

void Actuators::tick()
{
  fwd_port->tick();
  fwd_stbd->tick();
  aft_port->tick();
  aft_stbd->tick();
  fwd->tick();
  aft->tick();

  servo_timesince = std::max(fwd->get_timesince(), aft->get_timesince());
  if (servo_timesince > 5 && !servo_timeout)
  {
    servo_timeout = true;
  }
  if (servo_timesince <= 5 && servo_timeout)
  {
    servo_timeout = false;
  }
  thruster_timesince = 0;
  thruster_timesince = std::max(thruster_timesince, fwd_stbd->get_timesince());
  thruster_timesince = std::max(thruster_timesince, fwd_port->get_timesince());
  thruster_timesince = std::max(thruster_timesince, aft_stbd->get_timesince());
  thruster_timesince = std::max(thruster_timesince, aft_port->get_timesince());

  if (thruster_timesince > 5 && !thruster_timeout)
  {
    thruster_timeout = true;
  }
  if (thruster_timesince <= 5 && thruster_timeout)
  {
    thruster_timeout = false;
  }
}