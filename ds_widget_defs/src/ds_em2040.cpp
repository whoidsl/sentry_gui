#include "ds_widget/ds_em2040.h"

#include <ds_kctrl/message_utils.h>
#include <ds_kctrl_msgs/Kssis.h>
#include <ds_kctrl_msgs/KctrlCmdGoal.h>
#include <ds_kctrl_msgs/PuInfoMessage.h>
#include <ds_kctrl_msgs/KctrlStatusLabel.h>
#include <ds_kloggerd/util.h>
#include <ds_kloggerd_msgs/KloggerdCmdGoal.h>
#include <ds_kloggerd_msgs/KloggerdPingInfo.h>
#include <ds_kloggerd_msgs/KloggerdStatus.h>

#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib/client/comm_state.h>

#include <qboxlayout.h>
#include <qgridlayout.h>
#include <ros/assert.h>

#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QString>
#include <QComboBox>
#include <QMessageBox>
#include <QGroupBox>
#include <QFrame>
#include <QTextEdit>
#include <QDateTime>

namespace ds_widget
{

PuStatusLabel::PuStatusLabel(QWidget* parent)
  : QLabel(parent)
{

//   Would be nice to use stylesheets and dynamic properties, but we get
//   crashes.
//
//   see:  https://forum.qt.io/topic/158544/i-ran-into-a-strange-problem-with-qstyle/4
//   and:  https://bugreports.qt.io/browse/QTBUG-69204
//   setStyleSheet(R"(
// QLabel[status="missing"] { color: yellow }
// QLabel[status="off"] { color: rgb(169, 169, 169) }
// QLabel[status="ok"] { color: rgb(154, 205, 50) }
// QLabel[status="bad"] { color: rgb(255, 69, 0) }
// QLabel[status="other"] { color: rgb(255, 140, 0) }
// )");
    setAlignment(Qt::AlignLeft);
    auto init = ds_kctrl_msgs::KctrlStatusLabel{};
    init.status = ds_kctrl_msgs::KctrlStatusLabel::STATUS_MISSING;
    init.text.assign("NO DATA");
    setStatus(init);
}

void PuStatusLabel::setStatus(const ds_kctrl_msgs::KctrlStatusLabel& label) {
  QString color;
  switch (label.status) {
    case ds_kctrl_msgs::KctrlStatusLabel::STATUS_MISSING:
      color = QStringLiteral("yellow");
      break;
      
    case ds_kctrl_msgs::KctrlStatusLabel::STATUS_OFF:
      color = QStringLiteral("grey");
      break;
      
    case ds_kctrl_msgs::KctrlStatusLabel::STATUS_ACTIVE_OK:
      color = QStringLiteral("green");
      break;
      
    case ds_kctrl_msgs::KctrlStatusLabel::STATUS_ACTIVE_BAD:
      color = QStringLiteral("red");
      break;
      
    default:
      color = QStringLiteral("orange");
      break;
  }
  // Set label color using rich text
  const auto text = QString::fromStdString(label.text.empty() ? std::string{"NO-TEXT"} : label.text);
  setText(QStringLiteral("<font color=\"%1\">%2</font>").arg(color, text));
}

DsEm2040::DsEm2040(ros::NodeHandle& nh, QWidget* parent) 
  : QWidget(parent)
{
  system_id = nh.param<std::string>("/sentry/sonars/em2040/system_id", "");
  if (system_id.empty()) {
    ROS_FATAL("no parameter named '/sentry/sonars/em2040/system_id'");
    system_id.assign("NONE SET, CHECK CONFIG");
  }
  setupUi();  
  kctrl_pu_info = nh.subscribe("/sentry/sonars/em2040/ds_kctrl/pu_info", 1, &DsEm2040::handleKctrlPuInfo, this);
  kloggerd_ping_info = nh.subscribe("/sentry/sonars/em2040/ds_kloggerd/ping_info", 1, &DsEm2040::handleKloggerdPingInfo, this);
  kloggerd_status = nh.subscribe("/sentry/sonars/em2040/ds_kloggerd/kloggerd_status", 1, &DsEm2040::handleKloggerdStatus, this);

  kloggerd_action_client.reset(new actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>("/sentry/sonars/em2040/ds_kloggerd/kloggerd_cmd", nh.getCallbackQueue()));
  kctrl_action_client.reset(new actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>("/sentry/sonars/em2040/ds_kctrl/kctrl_cmd", nh.getCallbackQueue()));
}

void DsEm2040::setupUi()
{
  auto hlayout = new QHBoxLayout{};
  auto glayout = new QGridLayout(); 
  glayout->addWidget(new QLabel(QStringLiteral("Command:")), 0, 0);
  command = new QComboBox{};
  command->addItem("Start Pinging", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_PING_START_REQUEST});
  command->addItem("Stop Pinging", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_PING_STOP_REQUEST});
  command->addItem("Enable Watercolumn", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_WATERCOLUMN_START_REQUEST});
  command->addItem("Disable Watercolumn", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_WATERCOLUMN_STOP_REQUEST});
  command->addItem("Request #IIP/#IOP/#SVP", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_RUNTIME_PARAMS_REQUEST});
  command->addItem("Request PU XML Params", QList<QVariant>{CommandType::KCTRL, ds_kctrl_msgs::Kssis::KSSIS_PU_PARAMETER_EXPORT_REQUEST});
  command->insertSeparator(command->count());
  command->addItem("Start Recording", QList<QVariant>{CommandType::KLOGGERD, ds_kloggerd_msgs::KloggerdCmdGoal::START});
  command->addItem("Stop Recording", QList<QVariant>{CommandType::KLOGGERD, ds_kloggerd_msgs::KloggerdCmdGoal::STOP});
  glayout->addWidget(command, 0, 1);
  
  auto command_button = new QPushButton();
  command_button->setText(QStringLiteral("&Send"));
  QObject::connect(command_button, &QPushButton::clicked, this, &DsEm2040::handleCommandButtonClicked);
  
  glayout->addWidget(command_button, 1, 0);

  command_log = new QTextEdit(this);
  command_log->setReadOnly(true);
  command_log->setWindowFlags(Qt::Window);
  command_log->setWindowTitle(QStringLiteral("EM2040 Command Log"));
  command_log->setPlaceholderText(QStringLiteral("----empty----"));
  command_log->setMinimumWidth(QFontMetrics{font()}.width('x')*50);

  auto log_button = new QPushButton();
  log_button->setText(QStringLiteral("&View Log"));
  QObject::connect(log_button, &QPushButton::clicked, command_log, &QWidget::show);
  
  glayout->addWidget(log_button, 1, 1);
  glayout->setRowStretch(2,1);
   
  glayout->addWidget(new QLabel(QStringLiteral("EM2040 ID: %1").arg(QString::fromStdString(system_id))), 3, 0, 1, 2);
  hlayout->addLayout(glayout);

  glayout = new QGridLayout{};
  kctrl_status_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Status:")), 0, 0);
  glayout->addWidget(kctrl_status_label, 0, 1);

  kctrl_temperature_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Temp:")), 1, 0);
  glayout->addWidget(kctrl_temperature_label, 1, 1);

  kctrl_position_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Position:")), 2, 0);
  glayout->addWidget(kctrl_position_label, 2, 1);

  kctrl_attitude_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Attitude:")), 3, 0);
  glayout->addWidget(kctrl_attitude_label, 3, 1);

  kctrl_sound_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Sound Vel:")), 4, 0);
  glayout->addWidget(kctrl_sound_label, 4, 1);

  kctrl_heading_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Heading:")), 5, 0);
  glayout->addWidget(kctrl_heading_label, 5, 1);

  kctrl_depth_sensor_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Depth Sens:")), 6, 0);
  glayout->addWidget(kctrl_depth_sensor_label, 6, 1);

  kctrl_time_sync_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Time Sync:")), 7, 0);
  glayout->addWidget(kctrl_time_sync_label, 7, 1);

  kctrl_pps_label = new PuStatusLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("PPS:")), 8, 0);
  glayout->addWidget(kctrl_pps_label, 8, 1);

  hlayout->addLayout(glayout);

  glayout = new QGridLayout{};
  kloggerd_ping_number_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Ping #:")), 0, 0);
  glayout->addWidget(kloggerd_ping_number_label, 0, 1);
  
  kloggerd_ping_rate_hz_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Ping Rate:")), 1, 0);
  glayout->addWidget(kloggerd_ping_rate_hz_label, 1, 1);

  kloggerd_num_soundings_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Soundings:")), 2, 0);
  glayout->addWidget(kloggerd_num_soundings_label, 2, 1);

  kloggerd_pct_soundings_good_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("% Good:")), 3, 0);
  glayout->addWidget(kloggerd_pct_soundings_good_label, 3, 1);

  //glayout->setRowStretch(4,1);

  kloggerd_recording_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Recording:")), 4, 0);
  glayout->addWidget(kloggerd_recording_label, 4, 1);

  kloggerd_has_parameters_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("Has #IIP/#IOP:")), 5, 0);
  glayout->addWidget(kloggerd_has_parameters_label, 5, 1);

  kloggerd_file_name_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("kmall file:")), 6, 0);
  glayout->addWidget(kloggerd_file_name_label, 6, 1);

  kloggerd_file_size_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("kmall size:")), 7, 0);
  glayout->addWidget(kloggerd_file_size_label, 7, 1);

  kloggerd_kmwcd_name_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("kmwcd file:")), 8, 0);
  glayout->addWidget(kloggerd_kmwcd_name_label, 8, 1);

  kloggerd_kmwcd_size_label = new QLabel{};
  glayout->addWidget(new QLabel(QStringLiteral("kmwcd size:")), 9, 0);
  glayout->addWidget(kloggerd_kmwcd_size_label, 9, 1);

  hlayout->addLayout(glayout);
  setLayout(hlayout);

  // Don't know why we need to set up a queued connection here but not for the
  // log viewer widget nor the PUStatusLabel instances....
  QObject::connect(this, &DsEm2040::updateRecordingLabel, this, [this](bool enabled){
      if (enabled) {
        // kloggerd_recording_label->setText(QStringLiteral("<font color=green>true</font>"));
        this->kloggerd_recording_label->setText(QStringLiteral("<font color=\"green\">enabled</font>"));
      }
      else {
        this->kloggerd_recording_label->setText(QStringLiteral("<font color=\"red\">disabled</font>"));
      }
    },  Qt::QueuedConnection);

  QObject::connect(this, &DsEm2040::updateParametersLabel, this, [this](bool enabled){
      if (enabled) {
        // kloggerd_recording_label->setText(QStringLiteral("<font color=green>true</font>"));
        this->kloggerd_has_parameters_label->setText(QStringLiteral("<font color=\"green\">true</font>"));
        this->kloggerd_has_parameters_label->setToolTip("Has required datagrams for processing");
      }
      else {
        this->kloggerd_has_parameters_label->setText(QStringLiteral("<font color=\"red\">false</font>"));
        this->kloggerd_has_parameters_label->setToolTip("Missing one or more of #IOP, #IIP, or #SVP parameters.\nTry requesting them.");
      }
    },  Qt::QueuedConnection);
}

void DsEm2040::handleCommandButtonClicked()
{
  bool ok = false;
  const auto command_data = command->currentData().toList();
  if (command_data.empty()) {
    ROS_ERROR_STREAM("BUG:  command data for '" << command->currentText().toStdString() << "' is empty!");
    return;
  }
  const auto kind = command->currentData().toList().at(CommandDataIndex::TYPE).toInt(&ok);
  if (!ok) {
    ROS_ERROR_STREAM("invalid command data type for option: " << command->currentText().toStdString());
    return;
  }
  const auto value = command->currentData().toList().at(CommandDataIndex::VALUE).toInt(&ok);
  if (!ok) {
    ROS_ERROR_STREAM("invalid command data value for option: " << command->currentText().toStdString());
    return;
  }

  if (kind == CommandType::KCTRL) {
    auto goal = ds_kctrl_msgs::KctrlCmdGoal{};
    goal.command = value;
    goal.system_id = system_id;
    const auto command_string = ds_kctrl::kssis_id_to_string(value);
    if(value == ds_kctrl_msgs::Kssis::KSSIS_PU_PARAMETER_EXPORT_REQUEST) {
      goal.parameters = QStringLiteral("FILENAME=/tmp/config/%1_PU_PARAMETER_EXPORT_%2.xml").arg(QString::fromStdString(system_id), QDateTime::currentDateTimeUtc().toString("yyyyMMdd_hhmmss")).toStdString();

    }
    kctrl_goal_handles.push_back(kctrl_action_client->sendGoal(goal, std::bind(&DsEm2040::handleKctrlGoalTransition, this, command_number, command_string, std::placeholders::_1)));
    command_number++;
  }
  else if (kind == CommandType::KLOGGERD) {
    auto goal = ds_kloggerd_msgs::KloggerdCmdGoal{};
    goal.command = value;
    const auto command_string = ds_kloggerd::kloggerd_cmd_to_string(value);
    kloggerd_goal_handles.push_back(kloggerd_action_client->sendGoal(goal, std::bind(&DsEm2040::handleKloggerdGoalTransition, this, command_number, command_string, std::placeholders::_1)));
    command_number++;
  }
  else {
    ROS_ERROR_STREAM("invalid actionlib command type: " << kind);
  }

}

void DsEm2040::handleKctrlGoalTransition(std::size_t command_number, std::string command, const KctrlGoalHandle& gh)
{
  
  ROS_INFO_STREAM("command " << command_number << " (kctrl) goal transition: " << gh.getCommState().toString());
  
  appendLog(command_number, "kctrl", command, gh.getCommState());
  if (gh.getCommState() == actionlib::CommState::DONE) {
    // it's done, take action
    const auto state = gh.getTerminalState();
    if (state == actionlib::TerminalState::SUCCEEDED) {
      ROS_INFO_STREAM("KCTRL command finished successfully");
    }
    else {
      ROS_WARN_STREAM("KCTRL command failed.");
    }

    const auto result = gh.getResult();
    appendLog(command_number, "kctrl", command, state, result->message);

    // clear our handle
    kctrl_goal_handles.remove(gh);
    ROS_INFO_STREAM(kctrl_goal_handles.size() << " kctrl pending commands remain.");
  }
}

void DsEm2040::handleKloggerdGoalTransition(std::size_t command_number, std::string command, const KloggerdGoalHandle& gh)
{
  
  ROS_INFO_STREAM("command " << command_number << " (kloggerd) goal transition: " << gh.getCommState().toString());

  appendLog(command_number, "kloggerd", command, gh.getCommState());
  
  if (gh.getCommState() == actionlib::CommState::DONE) {
    // it's done, take action
    const auto state = gh.getTerminalState();
    if (state == actionlib::TerminalState::SUCCEEDED) {
      ROS_INFO_STREAM("KLOGGERD command finished successfully");
    }
    else {
      ROS_WARN_STREAM("KLOGGERD command failed.");
    }

    const auto result = gh.getResult();
    appendLog(command_number, "kloggerd", command, state, result->message);

    // clear our handle
    kloggerd_goal_handles.remove(gh);
    ROS_INFO_STREAM(kloggerd_goal_handles.size() << " kloggerd pending commands remain.");
  }
}

void DsEm2040::appendLog(std::size_t command_number, const std::string& command_kind, const std::string& command, const actionlib::CommState& state, const std::string& message)
{
  const auto color = [&]{
    if (state == actionlib::CommState::DONE) {
      return QStringLiteral("green");
    }
    else {
      return palette().text().color().name();
    }
  }();
  
  const auto command_num_string = QString::number(command_number);
  const auto status_string = QStringLiteral("<font color=%1>%2</font>").arg(color, QString::fromStdString(state.toString()));

  if (message.empty()) {
    command_log->append(QStringLiteral("%1: #%2 %3 %4").arg(status_string, command_num_string, QString::fromStdString(command_kind), QString::fromStdString(command)));
  }
  else {
    command_log->append(QStringLiteral("%1: #%2 %3 %4 result: '%5'").arg(status_string, command_num_string, QString::fromStdString(command_kind), QString::fromStdString(command), QString::fromStdString(message)));
  }
}

void DsEm2040::appendLog(std::size_t command_number, const std::string& command_kind, const std::string& command, const actionlib::TerminalState& state, const std::string& message)
{
  const auto color = [&]{
    if (state == actionlib::TerminalState::SUCCEEDED) {
      return QStringLiteral("green");
    }
    else if ( state == actionlib::TerminalState::REJECTED || 
              state == actionlib::TerminalState::ABORTED)
    {
      return QStringLiteral("red");
    }
    else {
      return palette().text().color().name();
    }
  }();
  
  const auto command_num_string = QString::number(command_number);
  const auto status_string = QStringLiteral("<font color=%1>%2</font>").arg(color, QString::fromStdString(state.toString()));
  if (message.empty())
  {
    command_log->append(QStringLiteral("%1: #%2 %3 %4").arg(status_string, command_num_string, QString::fromStdString(command_kind), QString::fromStdString(command)));
  }
  else {
    command_log->append(QStringLiteral("%1: #%2 %3 %4 result: '%5'").arg(status_string, command_num_string, QString::fromStdString(command_kind), QString::fromStdString(command), QString::fromStdString(message)));  
  }
}

void DsEm2040::handleKctrlPuInfo(const ds_kctrl_msgs::PuInfoMessage& msg)
{
  if (msg.system_id != system_id) {
    return;
  }
  kctrl_status_label->setStatus(msg.status);
  kctrl_temperature_label->setStatus(msg.temperature);
  kctrl_position_label->setStatus(msg.position[0]);
  kctrl_attitude_label->setStatus(msg.attitude[0]);
  kctrl_sound_label->setStatus(msg.sound_velocity);
  kctrl_heading_label->setStatus(msg.heading);
  kctrl_depth_sensor_label->setStatus(msg.depth_sensor);
  kctrl_time_sync_label->setStatus(msg.time_sync);
  kctrl_pps_label->setStatus(msg.pps);
}

void DsEm2040::handleKloggerdStatus(const ds_kloggerd_msgs::KloggerdStatus& msg)
{
  // if we try to use the same <font color=...></font> trick as the log widget
  // or pu status label here we get errors about chile widgets and threads
  // that cause segfaults
  //
  // e.g. can't just do kloggerd_recording_label->setText("<font color=\"red\">disabled</font>")
  emit updateRecordingLabel(msg.recording);
  emit updateParametersLabel(msg.has_parameters);
  
  auto file_name = QString::fromStdString(msg.file_name).trimmed();
  if (file_name.isEmpty()) {
    file_name = QStringLiteral("--NONE--");
  }
  kloggerd_file_name_label->setText(file_name);
  if (msg.file_size < 1000) {
    kloggerd_file_size_label->setText(QStringLiteral("%1 B").arg(QString::number(msg.file_size)));
  }
  else if (msg.file_size < 1000 * 1000) {
    kloggerd_file_size_label->setText(QStringLiteral("%1 kB").arg(QString::number(msg.file_size / 1000.0, 'f', 2)));
  }
  else {
    kloggerd_file_size_label->setText(QStringLiteral("%1 MB").arg(QString::number(msg.file_size / (1000.0 * 1000.0), 'f', 2))); 
  }

  file_name = QString::fromStdString(msg.wcd_name).trimmed();
  if (file_name.isEmpty()) {
    file_name = QStringLiteral("--NONE--");
  }
  kloggerd_kmwcd_name_label->setText(file_name);
  if (msg.wcd_size < 1000) {
    kloggerd_kmwcd_size_label->setText(QStringLiteral("%1 B").arg(QString::number(msg.wcd_size)));
  }
  else if (msg.wcd_size < 1000 * 1000) {
    kloggerd_kmwcd_size_label->setText(QStringLiteral("%1 kB").arg(QString::number(msg.wcd_size / 1000.0, 'f', 2)));
  }
  else {
    kloggerd_kmwcd_size_label->setText(QStringLiteral("%1 MB").arg(QString::number(msg.wcd_size / (1000.0 * 1000.0), 'f', 2))); 
  }
  
}

void DsEm2040::handleKloggerdPingInfo(const ds_kloggerd_msgs::KloggerdPingInfo& msg)
{
  kloggerd_ping_number_label->setText(QString::number(msg.ping_number));
  kloggerd_num_soundings_label->setText(QString::number(msg.num_soundings_main));
  kloggerd_pct_soundings_good_label->setText(QString::number(static_cast<double>(msg.num_soundings_main_valid)/msg.num_soundings_main * 100, 'f', 1));
  kloggerd_ping_rate_hz_label->setText(QString::number(static_cast<double>(msg.ping_rate_hz), 'f', 1));
}

}
