/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "../include/ds_widget/GeneralDial.h"

// SOURCE: coregui/include/GeneralDial.h

using namespace ds_widget;

GeneralDial::GeneralDial(QString name, double min, double max, double bigTickInterval, double medTickInterval,
                         double smallTickInterval, double step, QWidget* parent)
  : QWidget(parent)
{
  scale_start = 0;  // degrees
  scale_end = 0;    // degrees
  scale_range = 360;

  value = 0.0;
  minValue = min;
  maxValue = max;
  bigTick = bigTickInterval;
  medTick = medTickInterval;
  smallTick = smallTickInterval;
  stepSize = step;
  myName = name;
  needleColor = Qt::red;
}

void GeneralDial::resizeEvent(QResizeEvent* event)
{
  QWidget::resizeEvent(event);
}

void GeneralDial::paintEvent(QPaintEvent* event)
{
  drawBackground();
  drawValueNeedle();

  QWidget::paintEvent(event);
}

void GeneralDial::drawBackground(void)
{
  QPainter painter(this);
  QRadialGradient gradient;
  QLinearGradient linearGradient;

  QPen thickPen(palette().foreground(), 1.5);
  QPen thinPen(palette().foreground(), 1.0);
  QPen reallyThinPen(palette().foreground(), 0.5);

  // QColor   indicatorColor = Qt::yellow;
  QColor thinPenColor = Qt::white;

  QPen pen(Qt::white);
  pen.setWidth(1);

  /* Initialize painter */
  int side = qMin(width(), height());
  painter.setViewport((width() - side) / 2, (height() - side) / 2, side, side);
  painter.setWindow(-50, -50, 100, 100);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setPen(pen);
  /* Draw external circle */
  gradient = QRadialGradient(QPointF(-50, -50), 150, QPointF(-50, -50));
  gradient.setColorAt(0, QColor(224, 224, 224));
  gradient.setColorAt(1, QColor(28, 28, 28));
  painter.setPen(pen);
  painter.setBrush(QBrush(gradient));
  painter.drawEllipse(QPoint(0, 0), 48, 48);
  /* Draw inner circle */
  gradient = QRadialGradient(QPointF(0, 0), 150);
  gradient.setColorAt(0, QColor(224, 224, 224));
  gradient.setColorAt(1, QColor(28, 28, 28));
  painter.setPen(Qt::NoPen);
  painter.setBrush(QBrush(gradient));
  painter.drawEllipse(QPoint(0, 0), 45, 45);
  /* Draw inner shield */
  linearGradient = QLinearGradient(-50, -50, -50, 100);
  linearGradient.setColorAt(0, QColor(0, 0, 0));
  linearGradient.setColorAt(1, QColor(0, 0, 0));
  painter.setPen(pen);
  painter.setBrush(QBrush(linearGradient));
  painter.drawEllipse(QPoint(0, 0), 46, 46);
  // Draw center point
  painter.setPen(pen);
  painter.setBrush(Qt::black);
  painter.drawPoint(0, 0);
  painter.setBrush(palette().foreground());

  thinPen.setColor(thinPenColor);
  painter.setBrush(thinPenColor);

  QFont textFont = painter.font();
  textFont.setPointSize(6);
  painter.setFont(textFont);

  QFont smallFont = painter.font();
  smallFont.setPointSize(3);
  painter.setFont(smallFont);

  painter.setFont(textFont);
  painter.drawText(-10, 20, 20, 24, Qt::AlignHCenter | Qt::AlignTop, myName + "\n" + QString::number(value, 'f', 1));

  for (double i = minValue; i <= maxValue; i += stepSize)
  {
    painter.save();
    double rotation = (((i - minValue) * scale_range) / (maxValue - minValue)) + scale_start;
    painter.rotate(((rotation > 360) ? (rotation - 360) : rotation));
    if (0 == fmod(i, bigTick))  // draw a big tick and a label
    {
      painter.setPen(thickPen);
      painter.drawLine(0, -41, 0, -44);
      painter.setFont(textFont);
      painter.drawText(-10, -39, 20, 24, Qt::AlignHCenter | Qt::AlignTop, QString::number(i, 'f', 0));
    }
    else if (0 == fmod(i, medTick))  // draw a med tick and a label
    {
      painter.setPen(thinPen);
      painter.drawLine(0, -42, 0, -44);
      painter.setFont(smallFont);
      painter.drawText(-5, -42, 10, 12, Qt::AlignHCenter | Qt::AlignTop, QString::number(i, 'f', 0));
    }
    else if (0 == fmod(i, smallTick))  // draw a small tick
    {
      painter.setPen(reallyThinPen);
      painter.drawLine(0, -43, 0, -44);
    }
    painter.restore();
  }

  // setMinimumHeight(250);
  // setMinimumWidth(250);
  setMinimumHeight(100);
  setMinimumWidth(100);
}

void GeneralDial::setValue(double theValue)
{
  value = theValue;
  update();
}

void GeneralDial::setNeedleColor(QColor theColor)
{
  needleColor = theColor;
}

QColor GeneralDial::getNeedleColor(void)
{
  return needleColor;
}

void GeneralDial::drawValueNeedle()
{
  static const QPoint needle[5] = { QPoint(0, -1), QPoint(46, 0), QPoint(0, 1), QPoint(-3, 1), QPoint(-3, -1) };

  QPainter painter(this);
  QPen thinPen(palette().foreground(), 0.5);
  thinPen.setColor(needleColor);

  painter.setRenderHint(QPainter::Antialiasing);
  int side = qMin(width(), height());
  painter.setViewport((width() - side) / 2, (height() - side) / 2, side, side);
  painter.setWindow(-50, -50, 100, 100);
  painter.save();
  double rotation = (((value - minValue) * scale_range) / (maxValue - minValue)) + scale_start - 90.0;
  // qDebug() << value << " " << maxValue << " " << rotation;
  painter.rotate(((rotation > 360) ? (rotation - 360) : rotation));
  painter.setPen(thinPen);
  painter.setBrush(needleColor);
  painter.drawConvexPolygon(needle, 5);
  painter.drawEllipse(QPoint(0, 0), 2, 2);
  painter.restore();
}
