/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/20/18.
//

#ifndef PROJECT_LIGHT_H
#define PROJECT_LIGHT_H

#include <ds_widget/ds_widget.h>
#include <QMessageBox>

namespace ds_widget
{
class Light : public QWidget
{
  Q_OBJECT

public:
  Light(QString gs, QString rs, QString ys, QString os, QString nm, QWidget* _parent = Q_NULLPTR);
  void toRed()
  {
    color = "red";
    picLbl->setPixmap(red_pm);
  }
  void toGreen()
  {
    color = "green";
    picLbl->setPixmap(green_pm);
  }
  void toYellow()
  {
    color = "yellow";
    picLbl->setPixmap(yellow_pm);
  }
  void toOrange()
  {
    color = "orange";
    picLbl->setPixmap(orange_pm);
  }

  QPixmap red_pm, yellow_pm, orange_pm, green_pm;
  std::string color;
  QString name;
  QLabel *txtLbl, *picLbl;
  QGridLayout* grid;
  QString get_text();

public slots:
  void change_color();
  void set_text(QString txt);
};

class CircleLight : public Light
{
  Q_OBJECT
public:
  CircleLight(QString nm, QWidget* _parent = Q_NULLPTR)
    : Light("/img/lights/circle_green.png", "/img/lights/circle_red.png", "/img/lights/circle_yellow.png", "/img/lights/circle_orange.png", nm, _parent)
  {
  }
};

class RoundLight : public Light
{
  Q_OBJECT
public:
  RoundLight(QString nm, QWidget* _parent = Q_NULLPTR)
    : Light("/img/lights/round_green.png", "/img/lights/round_red.png", "/img/lights/round_yellow.png", "/img/lights/round_orange.png", nm, _parent)
  {
  }
};

class SquareLight : public Light
{
  Q_OBJECT
public:
  SquareLight(QString nm, QWidget* _parent = Q_NULLPTR)
    : Light("/img/lights/square_green.png", "/img/lights/square_red.png", "/img/lights/square_yellow.png", "/img/lights/square_orange.png", nm, _parent)
  {
  }
};

class TriangleLight : public Light
{
  Q_OBJECT
public:
  TriangleLight(QString nm, QWidget* _parent = Q_NULLPTR)
    : Light("/img/lights/triang_green.png", "/img/lights/triang_red.png", "/img/lights/triang_yellow.png", "/img/lights/triang_orange.png", nm, _parent)
  {
  }
};

}  // NAMESPACE

#endif  // PROJECT_LIGHT_H
