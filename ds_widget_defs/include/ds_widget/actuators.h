/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 5/13/18.
//

#ifndef DS_WIDGET_ACTUATORS_H
#define DS_WIDGET_ACTUATORS_H

#include <ds_widget/ds_widget.h>
#include <ds_actuator_msgs/ThrusterState.h>
#include <ds_actuator_msgs/ServoState.h>

namespace ds_widget
{
class Thruster : public QWidget
{
  Q_OBJECT
public:
  Thruster(QString name = "", QWidget* parent = Q_NULLPTR);
public slots:
  void tick();
  void update(ds_actuator_msgs::ThrusterState in_msg);
  int get_timesince()
  {
    return timesince;
  }

private:
  void update_view();
  std::mutex model_mutex;
  int timesince;
  bool view_needs_update;
  ds_actuator_msgs::ThrusterState model;
  QLabel *cmd_lbl, *val_lbl;
};

class Servo : public QWidget
{
  Q_OBJECT
public:
  Servo(QString name = "", QWidget* parent = Q_NULLPTR);
public slots:
  void tick();
  void update(ds_actuator_msgs::ServoState in_msg);
  int get_timesince()
  {
    return timesince;
  }

private:
  void update_view();
  std::mutex model_mutex;
  int timesince;
  bool view_needs_update;
  ds_actuator_msgs::ServoState model;
  QLabel *cmd_lbl, *val_lbl;
};

class Actuators : public QWidget
{
  Q_OBJECT
public:
  Actuators(QWidget* parent = Q_NULLPTR);
  Thruster *fwd_stbd, *fwd_port, *aft_stbd, *aft_port;
  Servo *fwd, *aft;
  void tick();

private:
  int servo_timesince;
  bool servo_timeout;
  int thruster_timesince;
  bool thruster_timeout;
};
}

#endif  // DS_WIDGET_ACTUATORS_H
