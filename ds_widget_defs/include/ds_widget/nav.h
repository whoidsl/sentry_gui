/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/22/18.
//

#ifndef PROJECT_NAV_H
#define PROJECT_NAV_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>
#include <ds_widget/ds_pair.h>
//#include <ds_widget/GeneralDial.h>
#include <ds_sensor_msgs/Dvl.h>
#include <ds_sensor_msgs/Gyro.h>
#include <ds_sensor_msgs/DepthPressure.h>
#include <ds_core_msgs/VoidCmd.h>

namespace ds_widget
{
class DvlRanges : public QWidget
{
  Q_OBJECT
public:
  DvlRanges(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::Dvl msg);

private:
  QLabel* ranges[4];
};

class Nav : public QWidget
{
  Q_OBJECT
public:
  Nav(ros::ServiceClient* _cl1, ros::ServiceClient* _cl2, QWidget* parent = Q_NULLPTR);

private:
  int phins_clk, paro_clk, dvl_clk;
  void setup_heading();
  void setup_velocities();
  void setup_ranges();
  void setup_timouts();
  ros::ServiceClient* cl1;
  ros::ServiceClient* cl2;
  QGroupBox *heading_box, *velocities_box, *ranges_box, *timeout_box;
  //        GeneralDial *compass;
  LabelPair *heading, *roll, *pitch;
  LabelPair *altitude, *depth, *velocities[3];
  LabelPair *dvl_timeout, *paro_timeout, *phins_timeout;
  DvlRanges* dvl;
  DsButton* zero_paro_btn;
  DsButton* phins_cal_btn;
  ds_sensor_msgs::Dvl dvl_msg;
  ds_sensor_msgs::DepthPressure paro_msg;
  ds_sensor_msgs::Gyro phins_msg;
  bool paro_needs_update, dvl_needs_update, phins_needs_update;
  std::mutex paro_mutex, dvl_mutex, phins_mutex;
  void update_paro_view();
  void update_dvl_view();
  void update_phins_view();
public slots:
  void zero_paro();
  void cal_phins();
  void update_paro(ds_sensor_msgs::DepthPressure in_paro);
  void update_phins(ds_sensor_msgs::Gyro in_phins);
  void update_dvl(ds_sensor_msgs::Dvl in_dvl);
  void tick();
};
}

#endif  // PROJECT_NAV_H
