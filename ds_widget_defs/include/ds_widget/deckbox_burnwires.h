/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by llindzey on 2 October 2018

#ifndef SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_BURNWIRES_H
#define SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_BURNWIRES_H

#include <ds_widget/ds_button.h>
#include <ds_widget/ds_widget.h>
#include <ds_widget/light.h>
#include <sentry_msgs/BurnwiresMeasurement.h>
#include <sentry_msgs/SampleBurnwiresCmd.h>

#include <mutex>

namespace ds_widget
{
// TODO(LEL): Get this formatted correctly. Emacs is being weird.
//     (I don't want namespaces to be indented)
class DeckboxBurnwires : public QGroupBox
{
  Q_OBJECT

public:
  DeckboxBurnwires(ros::ServiceClient* client, QWidget* parent = Q_NULLPTR);

private:
  void setupWidget();       // set up the layout and all elements of widget
  void setupConnections();  // Set up callbacks
  void updateView();        // actually updates the widget

  // How many ticks since last measurement was received.
  int time_since_update_;
  // Whether we need to update the view with a new msg, or just time.
  bool view_needs_update_;
  // Whether we are still waiting for a new burnwire measurement.
  bool waiting_for_measurement_;
  // Most recent measurement received
  sentry_msgs::BurnwiresMeasurement msg_;
  std::mutex msg_mutex_;
  // TODO(LEL): I'm unhappy with the measurement/sample nomenclature split here.
  ros::ServiceClient* measurement_client_;

  QString name_;
  DsButton* measurement_button_;  // pressed to start measurement.

  QLabel* burnwires_age_value_;
  QLabel* port_v_value_;
  QLabel* port_a_value_;

  QLabel* starboard_v_value_;
  QLabel* starboard_a_value_;

  QLabel* descent_v_value_;
  QLabel* descent_a_value_;

public slots:
  void clickSampleBurnwires();
  void measurementCallback(sentry_msgs::BurnwiresMeasurement msg);
  void tick();
};

}  // namespace ds_widget

#endif  // SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_BURNWIRES_H
