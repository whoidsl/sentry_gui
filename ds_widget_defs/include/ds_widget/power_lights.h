/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/3/18.
//

#ifndef PROJECT_POWER_LIGHTS_H
#define PROJECT_POWER_LIGHTS_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/light.h>
#include <ds_hotel_msgs/BatMan.h>
#include <ds_hotel_msgs/PowerSupply.h>
#include <ds_hotel_msgs/Battery.h>
#include <mutex>

namespace ds_widget
{
class BatmanLights : public QWidget
{
  Q_OBJECT

public:
  BatmanLights(QWidget* _parent = Q_NULLPTR);

private:
  void setupWidget();

  std::mutex model_mutex;
  ds_hotel_msgs::BatMan msg;
  bool view_needs_update;
  void update_view();
  QGridLayout* grid;
  int timesince, time_thresh;
  ds_widget::RoundLight *percent, *ahr;

public slots:
  void update(ds_hotel_msgs::BatMan in_msg);
  void tick();
};

class ShoreLights : public QWidget
{
  Q_OBJECT

public:
  ShoreLights(QWidget* _parent = Q_NULLPTR);

private:
  void setupWidget();

  std::mutex model_mutex;
  ds_hotel_msgs::PowerSupply msg;
  bool view_needs_update;
  void update_view();
  QGridLayout* grid;
  int timesince, time_thresh;
  ds_widget::RoundLight *voltage, *current;

public slots:
  void update(ds_hotel_msgs::PowerSupply msg);
  void tick();
};

class BatPercentLight : public QWidget
{
  Q_OBJECT

public:
  BatPercentLight(int _id = 0, QWidget* _parent = Q_NULLPTR);

private:
  void setupWidget();

  std::mutex model_mutex;
  ds_hotel_msgs::Battery msg;
  bool view_needs_update;
  void update_view();
  QGridLayout* grid;
  int id;
  int timesince, time_thresh;
  ds_widget::RoundLight* percent;
  QLabel* state;

public slots:
  void update(ds_hotel_msgs::Battery msg);
  void tick();
};
}

#endif  // PROJECT_POWER_LIGHTS_H
