/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/2/18.
//

#ifndef PROJECT_DS_BUTTON_H
#define PROJECT_DS_BUTTON_H

#include <ds_widget/ds_widget.h>
#include <QMessageBox>
#include <ros/package.h>

namespace ds_widget
{
class DsButton : public QPushButton
{
  Q_OBJECT
public:
  DsButton(QString _display_name, QString _confirm_string, QWidget* _parent = Q_NULLPTR, bool _confirm_dlog = true)
    : QPushButton(_display_name, _parent)
    , display_name(_display_name)
    , confirm_string(_confirm_string)
    , confirm_dlog(_confirm_dlog)
  {
    setFocusPolicy(Qt::NoFocus);
    connect(this, SIGNAL(pressed()), this, SLOT(confirm_click()));
  }

private:
  QString display_name;
  QString confirm_string;
  bool confirm_dlog;
public slots:
  void

  confirm_click();

  void confirm()
  {
    emit confirmed();
  }

signals:
  void

  confirmed();
};

class OnOff : public DsButton
{
  Q_OBJECT
public:
  OnOff(QString _display_name, QString _confirm_string, QString _on = "/img/lights/on_tiny.png",
        QString _off = "/img/lights/off_tiny.png", QWidget* _parent = Q_NULLPTR);

  QIcon on_pm, off_pm;
  bool on;

public slots:
  void On();

  void Off();
};

class RedGreen : public DsButton
{
  Q_OBJECT
private:
  std::string color;

public:
  RedGreen(QString _name, QWidget* parent = Q_NULLPTR, bool _confirm_dlog = true);
  void makeRed();
  void makeGreen();
  void makeYellow();
};

}  // end namespace

#endif  // PROJECT_DS_BUTTON_H
