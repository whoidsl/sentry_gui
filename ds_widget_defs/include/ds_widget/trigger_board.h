/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jogarcia on 11/23/20.
//

#ifndef PROJECT_TRIGGER_BOARD_H
#define PROJECT_TRIGGER_BOARD_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>
#include <ds_widget/ds_pair.h>
#include <ds_sensor_msgs/Dvl.h>
#include <ds_sensor_msgs/Gyro.h>
#include <ds_sensor_msgs/DepthPressure.h>
#include <ds_core_msgs/VoidCmd.h>
#include <ds_hotel_msgs/TrigPrmCtrlCmd.h>
#include <std_srvs/SetBool.h>

namespace ds_widget
{

class Trigger: public QWidget
{
  Q_OBJECT
public:
  // Constructor of the trigger board sentrysitter widget- as an rqt plugin, sentrysitter passes serviceclient pointers to Trigger that will make the following requests:
  // be_cl - used by sentrysitter to enable triggering/firing of all active channels on the trigger board
  // ci_cl - will be used by sentrysitter to load new configurations to the trigger board (Feature upcoming)
  // pc_cl - will be used by sentrysitter to validate the board's current settings against the parameter server (see ds_components/ds_trigger/config/standard_trigger_configuration.yaml) (Feature upcoming)
  Trigger(ros::ServiceClient* be_cl, ros::ServiceClient* ci_cl, ros::ServiceClient* pc_cl, QWidget* parent = Q_NULLPTR);

private:
  ros::ServiceClient* board_enablement_cl;
  ros::ServiceClient* config_install_cl;
  ros::ServiceClient* param_check_cl;
  DsButton* sonars_on_btn;                  
  DsButton* sonars_off_btn;
  DsButton* install_config_btn;
  DsButton* validate_settings_btn;
  QGroupBox *enablement_box;
public slots:
  void install_configuration();     // Install a new configuration onto the board.
  void enable_board();             // Enable triggering for all channels
  void disable_board();            // Silence all the channels
  void verify_configuration();     // Compare the board's actual settings to those on the parameter server
};
}

#endif  // PROJECT_NAV_H

