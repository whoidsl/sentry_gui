/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/18/18.
//

#ifndef PROJECT_ABORT_H
#define PROJECT_ABORT_H

#include <ds_widget/ds_widget.h>
#include <ds_core_msgs/Abort.h>
#include <ds_hotel_msgs/AbortCmd.h>
#include <ds_widget/ds_button.h>

namespace ds_widget
{
class Abort : public QGroupBox
{
  Q_OBJECT
public:
  Abort(ros::ServiceClient* _cl, QWidget* parent = Q_NULLPTR);

private:
  bool is_aborted, is_enabled;
  int timeout_255, timeout_10;
  ros::ServiceClient* cl;
  RedGreen *abort_btn, *enable_btn;
  QLabel* timeout_lbl;
  //        QGroupBox *box;
  std::string box_color;

  std::mutex model_mutex;
  ds_core_msgs::Abort model;
  bool view_needs_update;

  void update_view();
  void enable();
  void disable();
  void abort();
  void unabort();

private slots:
  void click_abort();
  void click_enable();

public slots:
  void tick();
  void update(ds_core_msgs::Abort in_msg);
};
}

#endif  // PROJECT_ABORT_H
