/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/15/18.
//

#ifndef PROJECT_BAT_GRID_WIDGET_H
#define PROJECT_BAT_GRID_WIDGET_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/light.h>
#include <vector>
#include <ds_hotel_msgs/Battery.h>
#include <ds_hotel_msgs/PowerSupply.h>
#include <ds_hotel_msgs/BatMan.h>
#include <ros/subscriber.h>
#include <ros/timer.h>
#include <mutex>

namespace ds_widget
{
class GridModel : public QWidget
{
  Q_OBJECT

public:
  GridModel(QString _type = "bat", QWidget* parent = Q_NULLPTR);

  int timesince, time_thresh;
  int rows, columns;

  QString type;
  std::vector<QString> row_names;
  std::map<int, std::vector<QString>> table_data;

  void bat_init();
  void chg_init();
  std::mutex table_data_mutex;

private:
  bool view_update_needed;
signals:
  void populate();
public slots:
  void bat_update(ds_hotel_msgs::Battery msg);
  void chg_update(ds_hotel_msgs::PowerSupply msg);
  void batman_update(ds_hotel_msgs::BatMan msg);
  void tick(const ros::TimerEvent&);
};

class GridView : public QWidget
{
  Q_OBJECT

public:
  GridView(GridModel* _model, QWidget* _parent = Q_NULLPTR);
  ~GridView()
  {
    disconnect(model, SIGNAL(populate()), this, SLOT(populate()));
  }

private:
  QGridLayout* grid;
  GridModel* model;
  void makeLabelAt(int row, int column, QString str);
  void setTextAt(int row, int column, QString str);
public slots:
  void populate();
};
}

#endif  // PROJECT_BAT_GRID_WIDGET_H
