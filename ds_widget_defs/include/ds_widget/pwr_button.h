/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/20/18.
//

#ifndef PROJECT_PWR_BUTTON_H
#define PROJECT_PWR_BUTTON_H

#include <QPushButton>
#include <ds_util/int_to_hex.h>
#include <sentry_msgs/PWRCmd.h>
#include <ros/service_client.h>
#include <QWidget>

namespace ds_widget
{
class PwrButton : public QPushButton
{
  Q_OBJECT

public:
  PwrButton(QString _name, int _address, ros::ServiceClient* _cl, bool _c_on = true, bool _c_off = true,
            QWidget* _parent = Q_NULLPTR)
    : name(_name)
    , address(_address)
    , cl(_cl)
    , confirm_off(_c_off)
    , confirm_on(_c_on)
    , on(false)
    , launched(false)
    , QPushButton(_parent)
    , parent(_parent)
  {
    setFocusPolicy(Qt::NoFocus);
    setText(name);
    makeYellow();
  }
  void makeGreen(), makeRed(), makeYellow();
  QString get_name()
  {
    return name;
  }
  int get_address()
  {
    return address;
  }

  void set_client(ros::ServiceClient* _cl)
  {
    cl = _cl;
  }
  void set_color(bool is_on);
  std::string color;

private:
  bool confirm_on;
  bool confirm_off;
  bool launched;

protected:
  ros::ServiceClient* cl;
  QString name;
  int address;
  bool on;
  void confirm_launch(QString str);
  QWidget* parent;

public slots:
  void pwr_click();
  void click_action();
};
}  // NAMESPACE

#endif  // PROJECT_PWR_BUTTON_H
