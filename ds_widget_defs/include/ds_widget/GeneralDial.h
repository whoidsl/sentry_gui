/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GENERALDIAL_H
#define GENERALDIAL_H

// SOURCE: coregui/include/GeneralDial.h
#include <QWidget>
#include <QtGui>
#include <QPainter>
#include <QColor>

namespace ds_widget
{
class GeneralDial : public QWidget
{
  Q_OBJECT
public:
  explicit GeneralDial(QString name, double min, double max, double bigTickInterval, double medTickInterval,
                       double smallTickInterval, double step, QWidget* parent = 0);

  void setValue(double theValue);

  void setNeedleColor(QColor theColor);

  QColor getNeedleColor(void);

signals:

private:
  double value;
  double minValue;
  double maxValue;
  double bigTick;
  double medTick;
  double smallTick;
  double stepSize;
  QString myName;
  QColor needleColor;

  void drawBackground();

  void drawValueNeedle();

  double scale_start;  // degrees
  double scale_end;    // degrees
  double scale_range;

public slots:

protected:
  virtual void resizeEvent(QResizeEvent* event);

  virtual void paintEvent(QPaintEvent* event);
};
}

#endif  // GENERALDIAL_H
