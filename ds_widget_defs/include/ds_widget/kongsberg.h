/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/16/19.
//

#ifndef PROJECT_KONGSBERG_H
#define PROJECT_KONGSBERG_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>
#include <ds_kongsberg_msgs/PingCmd.h>
#include <ds_kongsberg_msgs/BistCmd.h>
#include <ds_kongsberg_msgs/SettingsCmd.h>
//#include <ds_kongsberg_msgs/KongsbergKMAllRecord.h>
//#include <ds_kongsberg_msgs/KongsbergKSSIS.h>
#include <ds_kongsberg_msgs/KongsbergStatus.h>
#include <ds_core_msgs/ClockOffset.h>
#include <QComboBox>

namespace ds_widget
{
class Kongsberg : public QWidget
{
  Q_OBJECT

 public:
  Kongsberg(ros::ServiceClient* _ping_cl
            , ros::ServiceClient* _bist_cl
            , ros::ServiceClient* _settings_cl
            , QWidget* parent = Q_NULLPTR);
 public slots:
//  void update_kmall(ds_kongsberg_msgs::KongsbergKMALLRecord msg);
//  void update_kssis(ds_kongsberg_msgs::KongsbergKSSIS msg);
  void update_status(ds_kongsberg_msgs::KongsbergStatus msg);
  void update_pu_offset_status(ds_core_msgs::ClockOffset msg);
  void tick();
 private:
  void ping_click();
  void bist_click();
  void param_click();
//  int get_ping_enum(std::string name);
//  std::string get_sensor_enum(int val);
//  int get_bist_enum(std::bist);

  ros::ServiceClient *ping_cl, *bist_cl, *settings_cl;
//  ds_kongsberg_msgs::KongsbergKMALLRecord kmall;
//  ds_kongsberg_msgs::KongsbergKSSIS kssis;
  ds_kongsberg_msgs::KongsbergStatus status;
  ds_core_msgs::ClockOffset pu_offset;
//  bool kmall_u;
//  bool kssis_u;
  bool status_u;
  bool pu_status_u;
  std::mutex data_mutex;
  std::mutex pu_data_mutex;
  //
//  auto grid = new QGridLayout(this);
//  auto stat = new QGroupBox("Status", this);
  QLabel *name_lbl, *ship_lbl, *kctrl_con_lbl, *pu_pow_lbl, *pu_con_lbl, *kmall_con_lbl, *pinging_lbl;
  QComboBox *ping_combo;
  QPushButton *ping_btn;
//  auto bist = new QGroupBox("BIST", this);
  QLabel *bist_lbl, *bist_current_lbl;
  QComboBox *bist_combo;
  QPushButton *bist_btn;
//  auto data = new QGroupBox("Data", this);
//  auto sensors = new QGroupBox("Sensors", this);
  QLabel *cpu_temp_lbl, *pos1_lbl, *pos2_lbl, *pos3_lbl, *att1_lbl, *att2_lbl, *dep_lbl, *svp_lbl, *clk_lbl, *pps_lbl, *pu_offset_lbl;
//  auto ping = new QGroupBox("Ping", this);
  QLabel *ping_num_lbl, *num_soundings_lbl, *percent_good_lbl, *max_depth_lbl, *min_depth_lbl, *center_depth_lbl, *max_range_lbl, *min_range_lbl, *center_range_lbl;
//  auto params = new QGroupBox("Params", this);
  QLabel *rt_freq_lbl, *rt_trig_lbl, *rt_min_depth_lbl, *rt_max_depth_lbl, *rt_depth_mode_lbl;
  QLineEdit *p_name_edit, *p_val_edit;
  QPushButton *p_btn;
};

} //namespace
#endif //PROJECT_KONGSBERG_H
