/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivandor on 10/25/20.
//

#ifndef PROJECT_SENTRY_CAMERA_H
#define PROJECT_SENTRY_CAMERA_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>
#include <sentry_msgs/SypridCmd.h>
#include <sentry_msgs/SypridState.h>
#include <QComboBox>

namespace ds_widget
{
class Syprid : public QWidget
{
  Q_OBJECT

 public:
  Syprid(ros::ServiceClient* _cmd_cl
            , QWidget* parent = Q_NULLPTR);
 public slots:
  void update_status(sentry_msgs::SypridState msg);
  void tick();

 private:
  void cmd_click();

  ros::ServiceClient *cmd_cl;

  sentry_msgs::SypridState status;
  bool status_u;
  std::mutex data_mutex;

  QLabel *pkz_conn_lbl, *pkz_dcam_lbl, *dcam_port_state_lbl, *dcam_stbd_state_lbl, *dcam_persistence_state_lbl, *dcam_port_state_desired_lbl, *dcam_stbd_state_desired_lbl, *dcam_persistence_state_desired_lbl, *elmo_port_rpm_latest_lbl, *elmo_port_rpm_avg_lbl, *elmo_stbd_rpm_latest_lbl, *elmo_stbd_rpm_avg_lbl, *elmo_port_temp_lbl, *elmo_stbd_temp_lbl, *flow_sensor_state_lbl, *flow_sensor_state_desired_lbl;
  QComboBox *cmd_combo;
  QPushButton *cmd_btn;
  QLabel *cmd_lbl;
  QLineEdit *torque_1_edit, *torque_2_edit;
};

} //namespace
#endif //PROJECT_SENTRY_CAMERA_H
