/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
// Created by llindzey on 2 October 2018

#ifndef SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_RELAYS_H
#define SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_RELAYS_H

#include <ds_widget/ds_button.h>
#include <ds_widget/ds_widget.h>
#include <ds_widget/light.h>
#include <sentry_msgs/DeckboxRelayCmd.h>
#include <sentry_msgs/DeckboxStatus.h>

#include <mutex>

namespace ds_widget
{
class DeckboxRelays : public QGroupBox
{
  Q_OBJECT

public:
  DeckboxRelays(ros::ServiceClient* power_client, ros::ServiceClient* ethernet_client, QWidget* parent = Q_NULLPTR);

private:
  QString name_;

  void setupWidget();
  void setupConnections();
  void updateView();

  void clickPower(bool close_relay);
  void clickEthernet(bool close_relay);

  sentry_msgs::DeckboxStatus status_msg_;
  std::mutex msg_mutex_;

  ros::ServiceClient* power_client_;
  ros::ServiceClient* ethernet_client_;

  int time_since_update_;  // since status update
  int time_threshold_;     // time beyond which light turns yellow

  bool view_needs_update_;

  /**
  RoundLight* power_light_;
  RoundLight* ethernet_light_;
  **/
  DsButton* power_on_button_;
  DsButton* power_off_button_;
  DsButton* ethernet_on_button_;
  DsButton* ethernet_off_button_;

public slots:
  void tick();
  void statusCallback(sentry_msgs::DeckboxStatus msg);
  void clickPowerOn();
  void clickPowerOff();
  void clickEthernetOn();
  void clickEthernetOff();
};

}  // namespace ds_widget

#endif  // SENTRY_GUI_DS_WIDGET_DEFS_DECKBOX_RELAYS_H
