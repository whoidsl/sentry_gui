/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 6/6/18.
//

#ifndef SENTRY_SEA_WS_STATUS_H
#define SENTRY_SEA_WS_STATUS_H

#include <ds_widget/ds_widget.h>
#include <ds_core_msgs/Status.h>

namespace ds_widget
{
class StatusObj : public QWidget
{
  Q_OBJECT
public:
  StatusObj(QString _name, int _status, QWidget* parent = Q_NULLPTR);
  void update_status(int new_status);
  void tick();

private:
  int status;
  int timesince;
  //        QLabel * status_lbl;
  QLabel* name_lbl;
};

class Status : public QWidget
{
  Q_OBJECT
public:
  Status(QWidget* parent = Q_NULLPTR);
public slots:
  void update(ds_core_msgs::Status in_msg);
  void tick();

private:
  int num_sensors;
  void update_view();
  void reorder_view();
  std::mutex model_mutex;
  ds_core_msgs::Status model;
  bool view_needs_update;
  StatusObj* new_item;

  QGridLayout* grid;
  std::map<std::string, StatusObj*> statuses;
};
}

#endif  // SENTRY_SEA_WS_STATUS_H
