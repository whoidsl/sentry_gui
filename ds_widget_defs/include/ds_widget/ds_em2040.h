/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 7/16/19.
//

#ifndef DS_WIDGET_DEFS_DS_KCTRL_H
#define DS_WIDGET_DEFS_DS_KCTRL_H

#include <ds_kctrl_msgs/KctrlStatusLabel.h>
#include <ds_kctrl_msgs/KctrlCmdAction.h>
#include <ds_kloggerd_msgs/KloggerdCmdAction.h>

#include <ros/message_forward.h>
#include <ros/node_handle.h>
#include <ros/subscriber.h>

#include <actionlib/client/action_client.h>
#include <QWidget>
#include <QLabel>

class QLineEdit;
class QPushButton;
class QComboBox;
class QGridLayout;
class QTextEdit;

namespace actionlib_msgs
{
  ROS_DECLARE_MESSAGE(GoalStatusArray);
}

namespace ds_kctrl_msgs
{
  ROS_DECLARE_MESSAGE(PuInfoMessage);
  ROS_DECLARE_MESSAGE(KctrlStatusLabel);
}

namespace ds_kloggerd_msgs
{
  ROS_DECLARE_MESSAGE(KloggerdPingInfo);
  ROS_DECLARE_MESSAGE(KloggerdStatus);
}

namespace actionlib
{
  class CommState;
  class TerminalState;
}

namespace ds_widget
{
class PuStatusLabel: public QLabel
{
  Q_OBJECT

  public:
  explicit PuStatusLabel(QWidget* parent = Q_NULLPTR);
  ~PuStatusLabel() = default;

  void setStatus(const ds_kctrl_msgs::KctrlStatusLabel& status);
  
};

class DsEm2040 : public QWidget
{
  Q_OBJECT

  using KctrlActionClientPtr = std::unique_ptr<actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>>;
  using KctrlGoalHandle = actionlib::ActionClient<ds_kctrl_msgs::KctrlCmdAction>::GoalHandle;

  using KloggerdActionClientPtr = std::unique_ptr<actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>>;
  using KloggerdGoalHandle = actionlib::ActionClient<ds_kloggerd_msgs::KloggerdCmdAction>::GoalHandle;

  public:

  enum CommandType {
    KCTRL = 0,
    KLOGGERD,
  };

  enum CommandDataIndex {
    TYPE = 0,
    VALUE,
  };
 
  explicit DsEm2040(ros::NodeHandle& nh, QWidget* parent = Q_NULLPTR);
  ~DsEm2040() = default;

  protected:
  void setupUi();
  void handleKctrlPuInfo(const ds_kctrl_msgs::PuInfoMessage& msg);
  void handleKloggerdPingInfo(const ds_kloggerd_msgs::KloggerdPingInfo& msg);
  void handleKloggerdStatus(const ds_kloggerd_msgs::KloggerdStatus& msg);

  private:

  void handleCommandButtonClicked();
  void handleKctrlGoalTransition(std::size_t command_number, std::string cmd, const KctrlGoalHandle& gh);
  void handleKloggerdGoalTransition(std::size_t command_number, std::string cmd, const KloggerdGoalHandle& gh);
  void appendLog(std::size_t command_number, const std::string& command_kind, const std::string& command, const actionlib::CommState& state, const std::string& message={});  
  void appendLog(std::size_t command_number, const std::string& command_kind, const std::string& command, const actionlib::TerminalState& state, const std::string& message={});

  std::string system_id;

  KctrlActionClientPtr kctrl_action_client;
  std::list<KctrlGoalHandle> kctrl_goal_handles;

  ros::Subscriber kctrl_pu_info;
  ros::Subscriber kloggerd_ping_info;
  ros::Subscriber kloggerd_status;

  KloggerdActionClientPtr kloggerd_action_client;
  std::list<KloggerdGoalHandle> kloggerd_goal_handles;

  QComboBox* command;

  PuStatusLabel* kctrl_status_label;
  PuStatusLabel* kctrl_temperature_label;
  PuStatusLabel* kctrl_position_label;
  PuStatusLabel* kctrl_attitude_label;
  PuStatusLabel* kctrl_sound_label;
  PuStatusLabel* kctrl_heading_label;
  PuStatusLabel* kctrl_depth_sensor_label;
  PuStatusLabel* kctrl_time_sync_label;
  PuStatusLabel* kctrl_pps_label;

  QLabel* kloggerd_ping_number_label;
  QLabel* kloggerd_num_soundings_label;
  QLabel* kloggerd_pct_soundings_good_label;
  QLabel* kloggerd_ping_rate_hz_label;
  QLabel* kloggerd_recording_label;
  QLabel* kloggerd_has_parameters_label;
  QLabel* kloggerd_file_name_label;
  QLabel* kloggerd_file_size_label;
  QLabel* kloggerd_kmwcd_name_label;
  QLabel* kloggerd_kmwcd_size_label;

  std::size_t command_number = 0;
  QTextEdit* command_log;

  signals:

  // this BS private signal needed to get around QThread issues.
  // Why is it not needed for the PUStatusLabel
  void updateRecordingLabel(bool enabled);
  void updateParametersLabel(bool valid);
};



} //namespace
#endif //DS_WIDGET_DEFS_DS_KCTRL_H

