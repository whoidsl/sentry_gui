/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/5/18.
//

#ifndef PROJECT_PWR_WIDGET_H
#define PROJECT_PWR_WIDGET_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/pwr_button.h>
#include <ds_util/int_to_hex.h>
#include <sail_grd/pwr_lookup.h>
#include <ds_hotel_msgs/PWR.h>
#include <ros/param.h>
#include <mutex>

namespace ds_widget
{
class PwrGroup : public QGroupBox
{
  Q_OBJECT
public:
  PwrGroup(QString _name, QWidget* _parent = Q_NULLPTR);
  void add_pwr_btn(PwrButton* btn_ptr);
  int get_width()
  {
    return width;
  }
  int get_height()
  {
    return height;
  }

private:
  QString name;
  QGridLayout* grid;
  int max_height;
  int num_btns;
  int width;
  int height;
};

class Pwr : public QWidget
{
  Q_OBJECT

public:
  Pwr(ros::ServiceClient* _cl, std::string _param, QWidget* _parent = Q_NULLPTR);

  std::string param;
  std::vector<PwrButton*> btns;
  QGridLayout* grid;
  std::map<std::string, PwrGroup*> groups;
  int timesince, time_thresh;
  ros::ServiceClient* cl;

  std::string getParam();
  void setParam(std::string txt);

public slots:
  void update(ds_hotel_msgs::PWR in_msg);
  void tick();

private:
  std::mutex model_mutex;
  ds_hotel_msgs::PWR msg;
  bool view_needs_update;
  void update_view();
  void make_grid();
  void new_btn(QString _name, int _address, bool c_on, bool c_off, std::string _group);
};
}

#endif  // PROJECT_PWR_WIDGET_H
