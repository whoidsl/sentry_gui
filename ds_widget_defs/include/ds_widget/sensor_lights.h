/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/29/18.
//

#ifndef SENTRY_SEA_WS_SENSOR_LIGHTS_H
#define SENTRY_SEA_WS_SENSOR_LIGHTS_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/light.h>
#include <sentry_msgs/ResonRbite.h>
#include <ds_hotel_msgs/A2D2.h>
#include <ds_sensor_msgs/SoundSpeed.h>
#include <ds_sensor_msgs/Ctd.h>
#include <ds_sensor_msgs/OxygenConcentration.h>
#include <ds_sensor_msgs/Dvl.h>
#include <ds_sensor_msgs/Gyro.h>
#include <ds_sensor_msgs/DepthPressure.h>
#include <QSpacerItem>

namespace ds_widget
{
class Rbite : public RoundLight
{
  Q_OBJECT
public:
  Rbite(QWidget* parent = Q_NULLPTR);
  void update(sentry_msgs::ResonRbite in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  sentry_msgs::ResonRbite msg;
  int clk;
};

class Orp : public RoundLight
{
  Q_OBJECT
public:
  Orp(QWidget* parent = Q_NULLPTR);
  void update(ds_hotel_msgs::A2D2 in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_hotel_msgs::A2D2 msg;
  int clk;
};

class Obs : public RoundLight
{
  Q_OBJECT
public:
  Obs(QWidget* parent = Q_NULLPTR);
  void update(ds_hotel_msgs::A2D2 in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_hotel_msgs::A2D2 msg;
  int clk;
};

class Opt : public RoundLight
{
  Q_OBJECT
public:
  Opt(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::OxygenConcentration in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::OxygenConcentration msg;
  int clk;
};

class Ctd : public RoundLight
{
  Q_OBJECT
public:
  Ctd(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::Ctd in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::Ctd msg;
  int clk;
};

class Svp : public RoundLight
{
  Q_OBJECT
public:
  Svp(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::SoundSpeed in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::SoundSpeed msg;
  int clk;
};

class Dvl : public RoundLight
{
  Q_OBJECT
public:
  Dvl(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::Dvl in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::Dvl msg;
  int clk;
};

class Phins : public RoundLight
{
  Q_OBJECT
public:
  Phins(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::Gyro in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::Gyro msg;
  int clk;
};

class Paro : public RoundLight
{
  Q_OBJECT
public:
  Paro(QWidget* parent = Q_NULLPTR);
  void update(ds_sensor_msgs::DepthPressure in_msg);
  void tick();

private:
  void update_view();
  bool view_needs_update;
  std::mutex msg_mutex;
  ds_sensor_msgs::DepthPressure msg;
  int clk;
};

class SensorLights : public QWidget
{
  Q_OBJECT
public:
  SensorLights(QWidget* parent = Q_NULLPTR);
  Rbite* rbite;
  Orp* orp;
  Obs* obs;
  Opt* opt;
  Ctd* ctd;
  Svp* svp;
  Dvl* dvl;
  Phins* phins;
  Paro* paro;
  void tick();
};
}

#endif  // SENTRY_SEA_WS_SENSOR_LIGHTS_H
