/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/5/18.
//

#ifndef PROJECT_XR_WIDGET_H
#define PROJECT_XR_WIDGET_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>

#include <ds_hotel_msgs/XR.h>
#include <sentry_msgs/XRCmd.h>
#include <sentry_msgs/DeadhourCmd.h>

namespace ds_widget
{
class Xr : public QWidget
{
  Q_OBJECT

public:
  Xr(ros::ServiceClient* _cl1, ros::ServiceClient* _cl2, ros::ServiceClient* _deadhour_cl1,
     ros::ServiceClient* _deadhour_cl2, QWidget* _parent = Q_NULLPTR);

  ros::ServiceClient *cl1, *cl2, *deadhour_cl1, *deadhour_cl2;
  OnOff *p_burn, *p_hold, *s_burn, *s_hold, *d_burn, *d_hold;
  OnOff *p_drop, *s_drop, *d_drop;
  DsButton *burn_all, *snuff_all, *hold_all, *drop_all;
  DsButton *p_snuff, *s_snuff, *d_snuff;
  QLabel *xr1_dhr, *xr2_dhr;
  QLabel *xr1_short, *xr2_short;
  int timesince1, timesince2, time_thresh;
  DsButton* dead_btn;
  QLineEdit* dead_edit;

  void setupWidget();
  void setupConnections();

public slots:
  void update(ds_hotel_msgs::XR in_msg);
  void tick();

  void port_burn();
  void starboard_burn();
  void descent_burn();
  void port_snuff();
  void starboard_snuff();
  void descent_snuff();
  void port_hold();
  void starboard_hold();
  void descent_hold();
  void port_drop();
  void starboard_drop();
  void descent_drop();

  void deadhour_set();

private:
  std::mutex model_mutex_1, model_mutex_2;
  ds_hotel_msgs::XR xr1_model, xr2_model;
  bool view_needs_update_1, view_needs_update_2;
  void update_view_1();
  void update_view_2();
};
}

#endif  // PROJECT_XR_WIDGET_H
