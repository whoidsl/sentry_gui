/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 4/29/18.
//

#ifndef SENTRY_SEA_WS_SONARS_H
#define SENTRY_SEA_WS_SONARS_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_pair.h>
#include <sentry_msgs/ResonRbite.h>

namespace ds_widget
{
class Sonar : public QWidget
{
  Q_OBJECT
public:
  Sonar(QWidget* parent = Q_NULLPTR);

private:
  int rbite_clk;
  void setup_values();
  void setup_timouts();
  QGroupBox *values_box, *timeout_box;
  LabelPair* rbite;
  LabelPair* rbite_timeout;
  sentry_msgs::ResonRbite rbite_msg;
  bool rbite_needs_update;
  std::mutex rbite_mutex;
  void update_rbite_view();
public slots:
  void update_rbite(sentry_msgs::ResonRbite in_rbite);
  void tick();
};
}

#endif  // SENTRY_SEA_WS_SONARS_H
