/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 3/12/18.
//

#ifndef PROJECT_BATMAN_WIDGET_H
#define PROJECT_BATMAN_WIDGET_H

#include <ds_widget/ds_widget.h>
#include <ds_widget/ds_button.h>
#include <sentry_msgs/ChargeCmd.h>
#include <sentry_msgs/PowerCmd.h>
#include <ds_widget/ds_button.h>
#include <ds_hotel_msgs/BatMan.h>

namespace ds_widget
{
class Batman : public QWidget
{
  Q_OBJECT

public:
  Batman(ros::ServiceClient* _charge_cl, ros::ServiceClient* _bat_cl, ros::ServiceClient* _shore_cl,
         QWidget* _parent = Q_NULLPTR);

private:
  void setupWidget();
  void setupConnections();

  ros::ServiceClient *charge_cl, *bat_cl, *shore_cl;
  QGridLayout* grid;
  int timesince, time_thresh;
  ds_widget::DsButton *start_charge_btn, *stop_charge_btn, *bat_on_btn, *bat_off_btn, *shore_on_btn, *shore_off_btn;

public slots:
  void update(ds_hotel_msgs::BatMan msg);
  void tick();
  void start_charge();
  void stop_charge();
  void bat_on();
  void bat_off();
  void shore_on();
  void shore_off();
};
}

#endif  // PROJECT_BATMAN_WIDGET_H
