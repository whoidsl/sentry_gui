# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading
#import traceback
from sentry_msgs.msg import SentryControllerEnum, SentryAllocationEnum
from ds_control_msgs.msg import JoystickEnum
from qt_gui.plugin import Plugin
from python_qt_binding.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QPushButton
from python_qt_binding.QtCore import Qt, Signal, QMutex, QMutexLocker
from python_qt_binding.QtGui import QFont

import ds_param

from functools import partial

module_path = os.path.abspath(__file__)


class EnumButtons(QWidget):

    sig_newvalue = Signal(int)


    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self._layout = QVBoxLayout(self)

        self._values = []
        self._buttons = []

        self.setLayout(self._layout)

    def addEnumValue(self, name, value):
        but = QPushButton(name, self)

        self._buttons.append(but)
        self._values.append(value)
        self._layout.addWidget(but)

        but.clicked.connect(partial(self.clicked_callback, name, value))

    def clicked_callback(self, name, value):
        rospy.loginfo('Setting mode to \"%s\"=%d' % (name, value))
 
        self.sig_newvalue.emit(value)

    def active_mode(self, mode):
        # Set every button red
        for but in self._buttons:
            but.setProperty('widgetStatus', 'normal')
            but.setStyle(but.style())

        # Set the active button green
        idx = self._values.index(mode)
        self._buttons[idx].setProperty('widgetStatus', 'good')
        self._buttons[idx].setStyle(self._buttons[idx].style())


class ModePlugin(Plugin):
    def __init__(self, context):
        super(ModePlugin, self).__init__(context)
        self.setObjectName('ModePlugin')

        # Create the core widget
        self._widget = QWidget()
        self._mutex = QMutex()
        lock = QMutexLocker(self._mutex)

        # Create the widget contents
        self._layout = QHBoxLayout(self._widget)

        self._ctrl_modes = EnumButtons(self._widget)
        self._ctrl_modes.addEnumValue('Idle',    SentryControllerEnum.NONE)
        self._ctrl_modes.addEnumValue('Surface', SentryControllerEnum.JOYSTICK)
        self._ctrl_modes.addEnumValue('Survey',  SentryControllerEnum.SURVEY)

        self._alloc_modes = EnumButtons(self._widget)
        self._alloc_modes.addEnumValue('Idle',     SentryAllocationEnum.IDLE_MODE)
        self._alloc_modes.addEnumValue('Flight',   SentryAllocationEnum.FLIGHT_MODE)
        self._alloc_modes.addEnumValue('ROV',      SentryAllocationEnum.ROV_MODE)
        self._alloc_modes.addEnumValue('Vertical', SentryAllocationEnum.VERTICAL_MODE)

        self._joystick = EnumButtons(self._widget)
        self._joystick.addEnumValue('JOY',         JoystickEnum.JOY)
        self._joystick.addEnumValue('MC',          JoystickEnum.MC)
        #self._joystick.addEnumValue('CONJOY',      JoystickEnum.CONJOY)

        self._layout.addWidget(self._ctrl_modes)
        self._layout.addWidget(self._alloc_modes)
        self._layout.addWidget(self._joystick)

        # Connect to the ds_param values
        self._conn = ds_param.ParamConnection()
        self._ctrl_param  = self._conn.connect('/sentry/controllers/active_controller',
                                               ds_param.IntParam, False)


        self._alloc_param = self._conn.connect('/sentry/controllers/active_allocation',
                                               ds_param.IntParam, False)

        self._joystick_param = self._conn.connect('/sentry/goals/active_joystick',
                                               ds_param.IntParam, False)

        # Wire up updates to our updater
        self._ctrl_modes.sig_newvalue.connect(partial(self.send_mode, self._ctrl_param))
        self._alloc_modes.sig_newvalue.connect(partial(self.send_mode, self._alloc_param))
        self._joystick.sig_newvalue.connect(partial(self.send_mode, self._joystick_param))

        # Wire updates from the world
        self._conn.set_callback(self.param_update)

        self._init_stylesheet()

        # Expose the widget
        context.add_widget(self._widget)
        lock.unlock()
        self.param_update(None) # Set based on current values

    def _init_stylesheet(self):
        # fid = QtCore.QFile(":style.qss")
        # fid.open(QtCore.QFile.ReadOnly)
        # styleSheet = QtCore.QLatin1String(fid.readAll())
        # app.setStyleSheet(styleSheet)
        style_file = os.path.join(os.path.dirname(module_path),
                                  "sentrysitter.qss")
        with open(style_file, 'r') as f:
            self._widget.setStyleSheet(f.read())


    def send_mode(self, param, mode):
        lock = QMutexLocker(self._mutex)
        rospy.loginfo('Setting \"%s\" <-- %d' % (param.name(), mode))
        param.set(mode)
        self._ctrl_modes. active_mode(self._ctrl_param.get())
        self._alloc_modes.active_mode(self._alloc_param.get())
        self._joystick.active_mode(self._joystick_param.get())

    def param_update(self, params):
        lock = QMutexLocker(self._mutex)
        #print 'Cont: %d / Alloc: %d' % (self._ctrl_param.get(), self._alloc_param.get())
        #print params
        #traceback.print_stack()
        #print '\n\n\n'
        self._ctrl_modes. active_mode(self._ctrl_param.get())
        self._alloc_modes.active_mode(self._alloc_param.get())
        self._joystick.active_mode(self._joystick_param.get())


