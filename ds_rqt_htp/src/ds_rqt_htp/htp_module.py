# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading
from ds_hotel_msgs.msg import HTP

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
#from python_qt_binding.QtGui import QWidget
# PyQt5 change, it appears:
from PyQt5 import QtCore, QtGui, QtWidgets
from python_qt_binding.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout

class HTPPlugin(Plugin):

    def __init__(self, context):
        super(HTPPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('HTPPlugin')

        self.time_since = 0

        print 'HTP PLUGIN LOADED'

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        self._init_widgets()

        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))

        context.add_widget(self._widget)

        self._htp_subscription = rospy.Subscriber("/demo/bat/htp", HTP, self.htp_callback)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.time_callback)
        self.timer.start(1000)

    def htp_callback(self, htp):
        self.labelH_val.setText( str( round(htp.humidity, 1)))
        self.labelT_val.setText( str( round(htp.temperature, 1)))
        self.labelP_val.setText( str( round(htp.pressure, 1)))
        self.labeltime_val.setText( str(0))
        self.time_since = 0

    def time_callback(self):
        self.time_since += 1
        if self.time_since > 1:
            self.labeltime_val.setText( str(self.time_since))

    def run_publisher(self):
        import time
        from rosgraph_msgs.msg import Clock
        pub_clock = rospy.Publisher('/clock', Clock, queue_size=1)
        tw = time.time()
        while not rospy.is_shutdown():
            time.sleep(1)
            pub_clock.publish(rospy.Time.from_sec(tw))
            tw = time.time()

    def _init_widgets(self):
        # Create QWidget
        self._widget = QWidget()

        newfont = QtGui.QFont("Times", 14, QtGui.QFont.Bold)
        newfont_big = QtGui.QFont("Times", 24, QtGui.QFont.Bold)

        self.grid = QGridLayout(self._widget)
        self.labelH = QLabel('Humidity: ', self._widget)
        self.labelT = QLabel('Temperature: ', self._widget)
        self.labelP = QLabel('Pressure: ', self._widget)

        self.labeltime = QLabel('')
        self.grid.addWidget(self.labelH, 0, 0)
        self.grid.addWidget(self.labelT, 1, 0)
        self.grid.addWidget(self.labelP, 2, 0)
        self.grid.addWidget(self.labeltime, 4, 0)

        self.labelH_val = QLabel('', self._widget)
        self.labelT_val = QLabel('', self._widget)
        self.labelP_val = QLabel('', self._widget)
        self.labeltime_val = QLabel('', self._widget)
        self.grid.addWidget(self.labelH_val, 0, 1)
        self.grid.addWidget(self.labelT_val, 1, 1)
        self.grid.addWidget(self.labelP_val, 2, 1)
        self.grid.addWidget(self.labeltime_val, 4, 1)

        self.verticalSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        # self.grid.addStretch()
        self.grid.addItem(self.verticalSpacer, 3, 0)

        self.labelH.setFont(newfont)
        self.labelH_val.setFont(newfont_big)
        self.labelT.setFont(newfont)
        self.labelT_val.setFont(newfont_big)
        self.labelP.setFont(newfont)
        self.labelP_val.setFont(newfont_big)
        self.labeltime.setFont(newfont)
        self.labeltime_val.setFont(newfont_big)

        self._widget.setObjectName('HTPPluginUi')



    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self._htp_subscription.unregister()
        self.timer.stop()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
    # Comment in to signal that the plugin has a way to configure
    # This will enable a setting button (gear icon) in each dock widget title bar
    # Usually used to open a modal configuration dialog

