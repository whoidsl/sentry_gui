/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/15/18.
//

#ifndef SENTRY_GUI_SENTRYSITTER_MODULE_H
#define SENTRY_GUI_SENTRYSITTER_MODULE_H

#include <rqt_gui_cpp/plugin.h>

#include <qapplication.h>

#include <ds_widget/abort.h>
#include <ds_widget/actuators.h>
#include <ds_widget/bat_grid.h>
#include <ds_widget/batman.h>
#include <ds_widget/deckbox_burnwires.h>
#include <ds_widget/deckbox_relays.h>
#include <ds_widget/deckbox_temperature.h>
#include <ds_widget/ds_button.h>
#include <ds_widget/ds_pair.h>
#include <ds_widget/htp.h>
#include <ds_widget/nav.h>
#include <ds_widget/phins_compass.h>
#include <ds_widget/power_lights.h>
#include <ds_widget/pwr.h>
#include <ds_widget/sensor_lights.h>
#include <ds_widget/sonars.h>
#include <ds_widget/xr.h>
#include <ds_widget/ds_em2040.h>
#include <ds_widget/syprid.h>

#include <vector>
#include <list>

#include <ros/service_client.h>
#include <ros/subscriber.h>
#include <ros/timer.h>

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QFont>
#include <QIntValidator>
#include <QTimer>
#include <QCheckBox>

#include <QFile>
#include <QDialog>

namespace sentrysitter
{
class SentrySitterPlugin : public rqt_gui_cpp::Plugin
{
  Q_OBJECT
public:
  SentrySitterPlugin();
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings);

protected:
  void setupWidget();
  void setupConnections();
  void shutdownConnections();

public slots:
  void tick();

private:
  QTimer* timer;

  QWidget* widget_ = Q_NULLPTR;
  QGridLayout* big_grid = Q_NULLPTR;
  ds_widget::Pwr* pwr_grid = Q_NULLPTR;
  ds_widget::Htp *htp_bat, *htp_main = Q_NULLPTR;
  ds_widget::BatmanLights* batman_lights = Q_NULLPTR;
  std::vector<ds_widget::BatPercentLight*> bat_p_lights;
  ds_widget::ShoreLights* shore_lights = Q_NULLPTR;
  ds_widget::DeckboxBurnwires* deckbox_burnwires = Q_NULLPTR;
  ds_widget::DeckboxRelays* deckbox_relays = Q_NULLPTR;
  ds_widget::DeckboxTemperature* deckbox_temperature = Q_NULLPTR;
  ds_widget::SensorLights* sensor_lights = Q_NULLPTR;
  ds_widget::Xr* xr_status = Q_NULLPTR;
  ds_widget::Batman* batman = Q_NULLPTR;
  ds_widget::GridModel *bat_model, *chg_model = Q_NULLPTR;
  ds_widget::Nav* nav = Q_NULLPTR;
  ds_widget::Sonar* sonar = Q_NULLPTR;
  ds_widget::PhinsCompass* compass = Q_NULLPTR;
  ds_widget::Actuators* actuators = Q_NULLPTR;
  ds_widget::Abort* abort = Q_NULLPTR;
  ds_widget::DsEm2040* em2040 = Q_NULLPTR;
  ds_widget::Syprid* syprid = Q_NULLPTR;

  QString pwr_nodename, htp_main_nodename, htp_bat_nodename, xr1_nodename, xr2_nodename, batman_nodename;
  QString dvl_nodename, paro_nodename, phins_nodename, phinsins_nodename, reson_nodename, orp_nodename, obs_nodename, abort_nodename;
  QString opt_nodename, ctd_nodename, svp_nodename;
  QString servo_fwd_nodename, servo_aft_nodename;
  QString prop_fwd_stbd_nodename, prop_fwd_port_nodename, prop_aft_stbd_nodename, prop_aft_port_nodename;
  QString bat_path, chg_path;
  QString deckbox_nodename;
  QString syprid_nodename;

  ros::Subscriber pwr_sub, htp_main_sub, htp_bat_sub, xr1_sub, xr2_sub, batman_sub;
  ros::Subscriber dvl_sub, paro_sub, phins_sub, rbite_sub, orp_sub, obs_sub;  // phins_compass_sub,
  ros::Subscriber opt_sub, ctd_sub, svp_sub;
  ros::Subscriber servo_fwd_sub, servo_aft_sub;
  ros::Subscriber prop_fwd_stbd_sub, prop_fwd_port_sub, prop_aft_stbd_sub, prop_aft_port_sub;
  ros::Subscriber shore_sub, abort_sub;
  ros::Subscriber burnwires_measurement_sub;
  ros::Subscriber deckbox_temperature_status_sub;
  ros::Subscriber deckbox_relays_status_sub;
  ros::Subscriber syprid_status_sub;
  
  std::vector<ros::Subscriber> bat_p_subs;
  std::vector<ros::Subscriber> bat_subs;
  std::vector<ros::Subscriber> chg_subs;
  ros::ServiceClient pwr_client, xr1_client, xr2_client, xr1_deadhour_client, xr2_deadhour_client, batman_charge_client,
      batman_bat_client, batman_shore_client;
  ros::ServiceClient zero_paro_client, abort_client;
  ros::ServiceClient board_enablement_client, board_config_install_client, board_param_check_client;
  ros::ServiceClient sample_burnwires_client;
  ros::ServiceClient deckbox_power_client;
  ros::ServiceClient deckbox_ethernet_client;
  ros::ServiceClient syprid_cmd_client;
  ros::ServiceClient phinsins_cal_client;

  ds_widget::EditPair *pwr_edit, *htp_main_edit, *htp_bat_edit;
  ds_widget::EditPair *xr1_edit, *xr2_edit, *batman_edit;
  ds_widget::EditPair *dvl_edit, *paro_edit, *phins_edit, *phinsins_edit, *abort_edit;
  ds_widget::EditPair *reson_edit, *orp_edit, *obs_edit;
  ds_widget::EditPair *opt_edit, *ctd_edit, *svp_edit;
  ds_widget::EditPair *bat_edit, *chg_edit;
  ds_widget::EditPair *servo_fwd_edit, *servo_aft_edit;
  ds_widget::EditPair *prop_fwd_port_edit, *prop_fwd_stbd_edit, *prop_aft_port_edit, *prop_aft_stbd_edit;
  ds_widget::EditPair* deckbox_edit;
  ds_widget::EditPair* syprid_edit;

  ds_widget::DsButton *config_btn, *bat_btn, *chg_btn, *sensor_btn, *nav_btn, *sonar_btn, *actuators_btn, *deckbox_btn, *syprid_btn;
  QWidget *bat_dlog, *chg_dlog, *nav_dlog, *sonar_dlog, *actuators_dlog, *deckbox_dlog, *syprid_dlog;
  QCheckBox *deckbox_checkbox;
  bool chgs_open, bats_open, nav_open, sonar_open, actuators_open, deckbox_open, syprid_open;

  void batman_pass(ds_hotel_msgs::BatMan);
  void bat_pass(ds_hotel_msgs::Battery);
  void phins_pass(ds_sensor_msgs::Gyro);
  void paro_pass(ds_sensor_msgs::DepthPressure);
  void dvl_pass(ds_sensor_msgs::Dvl);

private slots:
  void set_config();
  void accept_config();
  void chg_pop();
  void bat_pop();
  void nav_pop();
  void sonar_pop();
  void actuators_pop();
  void deckbox_pop();
};  // END CLASS

}  // END NAMESPACE

#endif  // SENTRY_GUI_SENTRYSITTER_MODULE_H
