/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// ROS-compliant version of SentrySitter.
//
// This packages handles setting up all subscriptions and service clients
// and creates the top-level plugin that contains function-specific
// widgets. The widgets themselves can be found in the ds_widget_defs
// package.
//
// There are two types of widget interactions/updates:
// 1) Received messages are propagated to the separate widgets that
//    are responsible for displaying relevant data.
// 2) Individual widgets detect user input and send the appropriate
//    command, via the ServiceClient created in this class, and passed
//    via pointer to the widget at construction.

//
// Created by jvaccaro on 2/15/18.
//

#include "../include/module.h"
#include <pluginlib/class_list_macros.h>
#include <qsizepolicy.h>
#include <ros/node_handle.h>
#include <ros/param.h>
#include <QMessageBox>
#include <QScrollArea>
#include <ros/package.h>
#include <string>

namespace sentrysitter
{
SentrySitterPlugin::SentrySitterPlugin() : rqt_gui_cpp::Plugin(), widget_(0)
{
  ROS_INFO_STREAM("initialized!");
  // Constructor is called first before initPlugin function, needless to say.
  // give QObjects reasonable names
  setObjectName("SentrySitterPlugin");
  QApplication::setApplicationName("ROS SentrySitter");
}

void SentrySitterPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  ROS_INFO_STREAM("init plugin!");
  QString stylesheet = QString::fromStdString(ros::package::getPath("sentrysitter") + "/sentrysitter.qss");
  QFile File(stylesheet);
  File.open(QFile::ReadOnly);
  QString StyleSheet = QLatin1String(File.readAll());

  pwr_nodename = "/sentry/hotel/grd/pwr";
  htp_main_nodename = "/sentry/hotel/grd/htp_main";
  htp_bat_nodename = "/sentry/hotel/bat/htp_bat";
  xr1_nodename = "/sentry/hotel/grd/xr1";
  xr2_nodename = "/sentry/hotel/grd/xr2";
  batman_nodename = "/sentry/hotel/bat/batman";
  bat_path = "/sentry/hotel/bat";
  chg_path = "/sentry/hotel/bat";
  dvl_nodename = "/sentry/sensors/dvl300";
  paro_nodename = "/sentry/sensors/paro";
  phins_nodename = "/sentry/sensors/phins";
  phinsins_nodename = "/sentry/nav/phinsins";
  reson_nodename = "/sentry/sensors/reson";
  obs_nodename = "/sentry/sensors/obs";
  orp_nodename = "/sentry/sensors/orp";
  opt_nodename = "/sentry/sensors/anderaa_optode";
  ctd_nodename = "/sentry/sensors/sbe49_ctd";
  svp_nodename = "/sentry/sensors/svp70";
  servo_aft_nodename = "/sentry/actuators/servo_aft";
  servo_fwd_nodename = "/sentry/actuators/servo_fwd";
  prop_aft_stbd_nodename = "/sentry/actuators/thruster_aft_stbd";
  prop_fwd_stbd_nodename = "/sentry/actuators/thruster_fwd_stbd";
  prop_fwd_port_nodename = "/sentry/actuators/thruster_fwd_port";
  prop_aft_port_nodename = "/sentry/actuators/thruster_aft_port";
  abort_nodename = "/sentry/abort";
  deckbox_nodename = "/sentry/hotel/deckbox";
  syprid_nodename = "/sentry/pkz";

  timer = Q_NULLPTR;

  // access standalone command line arguments
  QStringList argv = context.argv();

  // create QWidget
  widget_ = new QWidget();

  widget_->setStyleSheet(StyleSheet);

  setupWidget();

  // add widget to the user interface
  context.addWidget(widget_);
}

void SentrySitterPlugin::shutdownConnections()
{
  ROS_INFO_STREAM("Shutting down connections");

  // SUBSCRIBERS
  pwr_sub.shutdown();
  htp_main_sub.shutdown();
  htp_bat_sub.shutdown();
  xr1_sub.shutdown();
  xr2_sub.shutdown();
  batman_sub.shutdown();
  for (int i = 0; i < bat_subs.size(); i++)
  {
    //            bat_p_subs[i].shutdown();
    bat_subs[i].shutdown();
  }
  for (int i = 0; i < chg_subs.size(); i++)
  {
    chg_subs[i].shutdown();
  }
  shore_sub.shutdown();
  burnwires_measurement_sub.shutdown();
//  deckbox_relays_status_sub.shutdown();
//  deckbox_temperature_status_sub.shutdown();
  paro_sub.shutdown();
  phins_sub.shutdown();
  dvl_sub.shutdown();
  //        phins_compass_sub.shutdown();
  rbite_sub.shutdown();
  orp_sub.shutdown();
  obs_sub.shutdown();
  opt_sub.shutdown();
  ctd_sub.shutdown();
  svp_sub.shutdown();
  servo_aft_sub.shutdown();
  servo_fwd_sub.shutdown();
  prop_fwd_stbd_sub.shutdown();
  prop_aft_stbd_sub.shutdown();
  prop_fwd_port_sub.shutdown();
  prop_aft_port_sub.shutdown();
  abort_sub.shutdown();
  syprid_status_sub.shutdown();

  // CLIENTS
  pwr_client.shutdown();
  xr1_client.shutdown();
  xr1_deadhour_client.shutdown();
  xr2_client.shutdown();
  xr2_deadhour_client.shutdown();
  batman_charge_client.shutdown();
  batman_bat_client.shutdown();
  batman_shore_client.shutdown();
//  deckbox_power_client.shutdown();
//  deckbox_ethernet_client.shutdown();
  board_enablement_client.shutdown();
  board_param_check_client.shutdown();
  board_config_install_client.shutdown();
  zero_paro_client.shutdown();
  abort_client.shutdown();
  syprid_cmd_client.shutdown();

  // TIMERS
  timer->stop();
}

void SentrySitterPlugin::shutdownPlugin()
{
  ROS_INFO_STREAM("shutdown!");
  // unregister all publishers, subscribers, clients, and timers here
  // SUBSCRIBERS
  shutdownConnections();

  // DIALOGS
  if (nav_dlog != Q_NULLPTR)
    delete nav_dlog;
  if (chg_dlog != Q_NULLPTR)
    delete chg_dlog;
  if (bat_dlog != Q_NULLPTR)
    delete bat_dlog;
  if (actuators_dlog != Q_NULLPTR)
    delete actuators_dlog;
  //        if (sonar_dlog !=Q_NULLPTR)
  //            delete sonar_dlog;
}

void SentrySitterPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings,
                                      qt_gui_cpp::Settings& instance_settings) const
{
  instance_settings.setValue("pwr_nodename", pwr_nodename);
  instance_settings.setValue("htp_main_nodename", htp_main_nodename);
  instance_settings.setValue("htp_bat_nodename", htp_bat_nodename);
  instance_settings.setValue("xr1_nodename", xr1_nodename);
  instance_settings.setValue("xr2_nodename", xr2_nodename);
  instance_settings.setValue("batman_nodename", batman_nodename);
  instance_settings.setValue("bat_path", bat_path);
  instance_settings.setValue("chg_path", chg_path);
  instance_settings.setValue("paro_nodename", paro_nodename);
  instance_settings.setValue("dvl_nodename", dvl_nodename);
  instance_settings.setValue("phins_nodename", phins_nodename);
  instance_settings.setValue("phinsins_nodename", phinsins_nodename);
  instance_settings.setValue("reson_nodename", reson_nodename);
  instance_settings.setValue("orp_nodename", orp_nodename);
  instance_settings.setValue("obs_nodename", obs_nodename);
  instance_settings.setValue("opt_nodename", opt_nodename);
  instance_settings.setValue("ctd_nodename", ctd_nodename);
  instance_settings.setValue("svp_nodename", svp_nodename);
  instance_settings.setValue("abort_nodename", abort_nodename);
  instance_settings.setValue("servo_fwd_nodename", servo_fwd_nodename);
  instance_settings.setValue("servo_aft_nodename", servo_aft_nodename);
  instance_settings.setValue("prop_fwd_stbd_nodename", prop_fwd_stbd_nodename);
  instance_settings.setValue("prop_fwd_port_nodename", prop_fwd_port_nodename);
  instance_settings.setValue("prop_aft_stbd_nodename", prop_aft_stbd_nodename);
  instance_settings.setValue("prop_aft_port_nodename", prop_aft_port_nodename);
  instance_settings.setValue("deckbox_nodename", deckbox_nodename);
  instance_settings.setValue("syprid_nodename", syprid_nodename);
}

void SentrySitterPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                                         const qt_gui_cpp::Settings& instance_settings)
{
  pwr_nodename = instance_settings.value("pwr_nodename", pwr_nodename).toString();
  htp_main_nodename = instance_settings.value("htp_main_nodename", htp_main_nodename).toString();
  htp_bat_nodename = instance_settings.value("htp_bat_nodename", htp_bat_nodename).toString();
  xr1_nodename = instance_settings.value("xr1_nodename", xr1_nodename).toString();
  xr2_nodename = instance_settings.value("xr2_nodename", xr2_nodename).toString();
  batman_nodename = instance_settings.value("batman_nodename", batman_nodename).toString();
  bat_path = instance_settings.value("bat_path", bat_path).toString();
  chg_path = instance_settings.value("chg_path", chg_path).toString();
  phins_nodename = instance_settings.value("phins_nodename", phins_nodename).toString();
  phinsins_nodename = instance_settings.value("phinsins_nodename", phinsins_nodename).toString();
  paro_nodename = instance_settings.value("paro_nodename", paro_nodename).toString();
  dvl_nodename = instance_settings.value("dvl_nodename", dvl_nodename).toString();
  reson_nodename = instance_settings.value("reson_nodename", reson_nodename).toString();
  orp_nodename = instance_settings.value("orp_nodename", orp_nodename).toString();
  obs_nodename = instance_settings.value("obs_nodename", obs_nodename).toString();
  opt_nodename = instance_settings.value("opt_nodename", opt_nodename).toString();
  ctd_nodename = instance_settings.value("ctd_nodename", ctd_nodename).toString();
  svp_nodename = instance_settings.value("svp_nodename", svp_nodename).toString();
  servo_fwd_nodename = instance_settings.value("servo_fwd_nodename", servo_fwd_nodename).toString();
  servo_aft_nodename = instance_settings.value("servo_aft_nodename", servo_aft_nodename).toString();
  prop_fwd_stbd_nodename = instance_settings.value("prop_fwd_stbd_nodename", prop_fwd_stbd_nodename).toString();
  prop_fwd_port_nodename = instance_settings.value("prop_fwd_port_nodename", prop_fwd_port_nodename).toString();
  prop_aft_stbd_nodename = instance_settings.value("prop_aft_stbd_nodename", prop_aft_stbd_nodename).toString();
  prop_aft_port_nodename = instance_settings.value("prop_aft_port_nodename", prop_aft_port_nodename).toString();
  abort_nodename = instance_settings.value("abort_nodename", abort_nodename).toString();
  deckbox_nodename = instance_settings.value("deckbox_nodename", deckbox_nodename).toString();
  syprid_nodename = instance_settings.value("syprid_nodename", syprid_nodename).toString();

  setupConnections();
}

/// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void SentrySitterPlugin::setupWidget()
{
  ROS_INFO_STREAM("setup widget");
  big_grid = new QGridLayout(widget_);

  /// MAIN VIEW
  // number of columns in the "main" panel (not sidebar)
  int main_width = 12;
  // number of columns for the environmental/power/sensor status lights
//  int light_width = 9;

  pwr_grid = new ds_widget::Pwr(&pwr_client, "/sentry/config/power_config");

  //QScrollArea* pwr_area = new QScrollArea();
  //pwr_area->setWidget(pwr_grid);
  QGridLayout* pwr_layout = new QGridLayout();
  big_grid->addLayout(pwr_layout, 0, 0, 7, main_width);

  //big_grid->addWidget(pwr_area, 0, 0, 7, main_width);
  pwr_layout->addWidget(pwr_grid, 0, 0, 8, main_width);

  /// Environmental
  QLabel* environ_title = new QLabel("Environmental");
  big_grid->addWidget(environ_title, 7, 0);

  QGridLayout* environ_layout = new QGridLayout();
  big_grid->addLayout(environ_layout, 8, 0, 1, main_width);


  htp_main = new ds_widget::Htp("Main", 25, 40, 35, 25);
  environ_layout->addWidget(htp_main, 0, 0, 1, 3);
  htp_bat = new ds_widget::Htp("Bat", 60, 40, 35, 10);
  environ_layout->addWidget(htp_bat, 0, 3, 1, 3);
  shore_lights = new ds_widget::ShoreLights();
  environ_layout->addWidget(shore_lights, 0, 6, 1, 2);
//  deckbox_temperature = new ds_widget::DeckboxTemperature();
//  environ_layout->addWidget(deckbox_temperature, 0, 8, 1, 1);
//  deckbox_temperature->setVisible(false);

  /// Power
  QLabel* power_title = new QLabel("Power");
  big_grid->addWidget(power_title, 9, 0);


  QGridLayout* power_layout = new QGridLayout();
  //big_grid->addLayout(power_layout, 5, 0, 1, light_width);
  big_grid->addLayout(power_layout, 10, 0, 1, main_width);


  batman_lights = new ds_widget::BatmanLights();
  power_layout->addWidget(batman_lights, 0, 0);
  bat_p_lights.resize(6);
  for (int i = 0; i < 6; i++)
  {
    bat_p_lights[i] = new ds_widget::BatPercentLight(i + 1);
    power_layout->addWidget(bat_p_lights[i], 0, 1 + i);
  }

  QLabel* sensors_title = new QLabel("Sensors");
  big_grid->addWidget(sensors_title, 11, 0);
  sensor_lights = new ds_widget::SensorLights();
  big_grid->addWidget(sensor_lights, 12, 0, 1, main_width);

  /// DECKBOX WIDGETS
//  deckbox_relays = new ds_widget::DeckboxRelays(&deckbox_power_client, &deckbox_ethernet_client);
//  big_grid->addWidget(deckbox_relays, 7, light_width, 3, 3);
//  deckbox_relays->setVisible(false);
//  deckbox_burnwires = new ds_widget::DeckboxBurnwires(&sample_burnwires_client);
//  big_grid->addWidget(deckbox_burnwires, 10, light_width, 3, 3);
//  deckbox_burnwires->setVisible(false);

  /// SIDE BUTTONS
  int side_width = 2;

  compass = new ds_widget::PhinsCompass(widget_);
  big_grid->addWidget(compass, 0, main_width, 6, side_width);

  batman = new ds_widget::Batman(&batman_charge_client, &batman_bat_client, &batman_shore_client);
  big_grid->addWidget(batman, 6, main_width, 2, side_width);

  xr_status = new ds_widget::Xr(&xr1_client, &xr2_client, &xr1_deadhour_client, &xr2_deadhour_client);
  big_grid->addWidget(xr_status, 8, main_width, 4, side_width);

  abort = new ds_widget::Abort(&abort_client, widget_);
  big_grid->addWidget(abort, 12, main_width, 1, side_width);

  /// BOTTOM BUTTONS
  QGridLayout* bottom_btns = new QGridLayout();
  big_grid->addLayout(bottom_btns, 13, 0, 1, main_width);

  config_btn = new ds_widget::DsButton("Change config", "", widget_, false);
  bottom_btns->addWidget(config_btn, 0, 0);
  connect(config_btn, SIGNAL(confirmed()), this, SLOT(set_config()));

  // TODO: these dialogs are clunky. Can we make a better dialog widget that does what this needs?

  chgs_open = false;
  chg_dlog = Q_NULLPTR;
  chg_model = new ds_widget::GridModel("chg", widget_);
  chg_btn = new ds_widget::DsButton("Chargers", "", widget_, false);
  bottom_btns->addWidget(chg_btn, 0, 1);
  connect(chg_btn, SIGNAL(confirmed()), this, SLOT(chg_pop()));

  bats_open = false;
  bat_dlog = Q_NULLPTR;
  bat_model = new ds_widget::GridModel("bat", widget_);
  bat_btn = new ds_widget::DsButton("Batteries", "", widget_, false);
  bottom_btns->addWidget(bat_btn, 0, 2);
  connect(bat_btn, SIGNAL(confirmed()), this, SLOT(bat_pop()));

  nav_open = false;
  nav_dlog = Q_NULLPTR;
  nav = new ds_widget::Nav(&phinsins_cal_client, &zero_paro_client, widget_);
  nav_btn = new ds_widget::DsButton("Nav Sensors", "", widget_, false);
  bottom_btns->addWidget(nav_btn, 0, 3);
  nav_pop();
  connect(nav_btn, SIGNAL(confirmed()), this, SLOT(nav_pop()));

  //        sonar_open = false;
  //        sonar_dlog = Q_NULLPTR;
  //        sonar = new ds_widget::Sonar(widget_);
  //        sonar_btn = new ds_widget::DsButton("Sonars", "", widget_, false);
  //        bottom_btns->addWidget(sonar_btn, 0, 4);
  //        sonar_pop();
  //        connect(sonar_btn, SIGNAL(confirmed()), this, SLOT(sonar_pop()));

  actuators_open = false;
  actuators_dlog = Q_NULLPTR;
  actuators = new ds_widget::Actuators(widget_);
  actuators_btn = new ds_widget::DsButton("Actuators", "", widget_, false);
  bottom_btns->addWidget(actuators_btn, 0, 4);
  actuators_pop();
  connect(actuators_btn, SIGNAL(confirmed()), this, SLOT(actuators_pop()));

  syprid_open = false;
  syprid_dlog = Q_NULLPTR;
  syprid = new ds_widget::Syprid(&syprid_cmd_client);
  syprid_btn = new ds_widget::DsButton("Syprid", "", widget_, false);
  bottom_btns->addWidget(syprid_btn, 0, 5);
  connect(syprid_btn, &ds_widget::DsButton::confirmed,
          [this](){
            if (!syprid_open)
            {
              syprid_open = true;
              ROS_INFO_STREAM("syprid pop!");
              syprid_dlog = new QWidget(widget_);
              syprid_dlog->setWindowFlags(Qt::Window);
              QGridLayout* gr = new QGridLayout(syprid_dlog);
              syprid_dlog->setWindowTitle("Syprid");
              gr->addWidget(syprid, 0, 0);
            }
            else
            {
              syprid_dlog->show();
            }});

  ROS_INFO_STREAM("Start Kongsberg Create!");
  
  auto em2040_button = new QPushButton();
  em2040_button->setText(QStringLiteral("EM2040"));
  bottom_btns->addWidget(em2040_button, 0, 7);

  QObject::connect(em2040_button, &QPushButton::clicked, [this] {
    if (this->em2040) {
      this->em2040->show();
    }
    else {
      ROS_ERROR_STREAM("em2040 widget is null");
    }
  });
  
//  deckbox_checkbox = new QCheckBox("Enable Deckbox");
//  bottom_btns->addWidget(deckbox_checkbox, 0, 5);


//  connect(deckbox_checkbox, SIGNAL(toggled(bool)), deckbox_relays, SLOT(setVisible(bool)));
//  connect(deckbox_checkbox, SIGNAL(toggled(bool)), deckbox_burnwires, SLOT(setVisible(bool)));
//  connect(deckbox_checkbox, SIGNAL(toggled(bool)), deckbox_temperature, SLOT(setVisible(bool)));
//  deckbox_checkbox->setChecked(false);

}

void SentrySitterPlugin::setupConnections()
{
  ROS_INFO_STREAM("setup connections");
  ros::NodeHandle nh = getMTNodeHandle();

  if(!em2040) {
    em2040 = new ds_widget::DsEm2040(nh, widget_);
    em2040->setWindowFlags(Qt::Window);
    em2040->setWindowTitle("Kongsberg EM2040");
    em2040->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
  }

  // SUBSCRIPTIONS (display info)
  pwr_sub =
      nh.subscribe<ds_hotel_msgs::PWR>((pwr_nodename + "/state").toStdString(), 1, &ds_widget::Pwr::update, pwr_grid);
  ROS_INFO_STREAM("setup connections: pwr");
  htp_main_sub = nh.subscribe<ds_hotel_msgs::HTP>((htp_main_nodename + "/htp").toStdString(), 1,
                                                  &ds_widget::Htp::update, htp_main);
  htp_bat_sub =
      nh.subscribe<ds_hotel_msgs::HTP>((htp_bat_nodename + "/htp").toStdString(), 1, &ds_widget::Htp::update, htp_bat);
  xr1_sub =
      nh.subscribe<ds_hotel_msgs::XR>((xr1_nodename + "/state").toStdString(), 1, &ds_widget::Xr::update, xr_status);
  xr2_sub =
      nh.subscribe<ds_hotel_msgs::XR>((xr2_nodename + "/state").toStdString(), 1, &ds_widget::Xr::update, xr_status);
  // batman_sub = nh.subscribe<ds_hotel_msgs::BatMan>((batman_nodename + "/state").toStdString(), 1, &ds_widget::BatmanLights::update, batman_lights);
  batman_sub = nh.subscribe<ds_hotel_msgs::BatMan>((batman_nodename + "/state").toStdString(), 1, &sentrysitter::SentrySitterPlugin::batman_pass, this);

  int num_bats = 6;
  bat_subs.resize(num_bats);
  //        bat_p_subs.resize(num_bats);
  for (int i = 0; i < num_bats; i++)
  {
    std::string sub_path = (bat_path + "/bat" + QString::number(i + 1) + "/state").toStdString();
    //            bat_p_subs[i] = nh.subscribe<ds_hotel_msgs::Battery>(sub_path, 1,
    //            &ds_widget::BatPercentLight::update, bat_p_lights[i]);
    bat_subs[i] = nh.subscribe<ds_hotel_msgs::Battery>(sub_path, 1, &sentrysitter::SentrySitterPlugin::bat_pass, this);
  }
  int num_chgs = 5;
  chg_subs.resize(num_chgs);
  for (int i = 0; i < num_chgs; i++)
  {
    std::string sub_path = (chg_path + "/chg" + QString::number(i + 1) + "/dcpwr").toStdString();
    chg_subs[i] = nh.subscribe<ds_hotel_msgs::PowerSupply>(sub_path, 1, &ds_widget::GridModel::chg_update, chg_model);
  }
//  std::string burnwires_topic((deckbox_nodename + "/burnwires").toStdString());
//  burnwires_measurement_sub = nh.subscribe<sentry_msgs::BurnwiresMeasurement>(
//      burnwires_topic, 1, &ds_widget::DeckboxBurnwires::measurementCallback, deckbox_burnwires);

//  std::string deckbox_topic((deckbox_nodename + "/state").toStdString());
//  deckbox_temperature_status_sub = nh.subscribe<sentry_msgs::DeckboxStatus>(
//      deckbox_topic, 1, &ds_widget::DeckboxTemperature::statusCallback, deckbox_temperature);
//  deckbox_relays_status_sub = nh.subscribe<sentry_msgs::DeckboxStatus>(
//      deckbox_topic, 1, &ds_widget::DeckboxRelays::statusCallback, deckbox_relays);

  ROS_INFO_STREAM("setup connections: chgs and bats");
  shore_sub = nh.subscribe<ds_hotel_msgs::PowerSupply>((chg_path + "/shore/dcpwr").toStdString(), 1,
                                                       &ds_widget::ShoreLights::update, shore_lights);
  dvl_sub = nh.subscribe<ds_sensor_msgs::Dvl>((dvl_nodename + "/dvl").toStdString(), 1,
                                              &sentrysitter::SentrySitterPlugin::dvl_pass, this);
  paro_sub = nh.subscribe<ds_sensor_msgs::DepthPressure>((paro_nodename + "/depth").toStdString(), 1,
                                                         &sentrysitter::SentrySitterPlugin::paro_pass, this);
  phins_sub = nh.subscribe<ds_sensor_msgs::Gyro>((phins_nodename + "/gyro").toStdString(), 1,
                                                 &sentrysitter::SentrySitterPlugin::phins_pass, this);
  //        phins_compass_sub =
  //        nh.subscribe<ds_sensor_msgs::Gyro>((phins_nodename+"/gyro").toStdString(), 1,
  //        &ds_widget::PhinsCompass::update_model, compass);
  rbite_sub = nh.subscribe<sentry_msgs::ResonRbite>((reson_nodename + "/rbite").toStdString(), 1,
                                                    &ds_widget::Rbite::update, sensor_lights->rbite);
  orp_sub = nh.subscribe<ds_hotel_msgs::A2D2>((orp_nodename + "/a2d2").toStdString(), 1, &ds_widget::Orp::update,
                                              sensor_lights->orp);
  obs_sub = nh.subscribe<ds_hotel_msgs::A2D2>((obs_nodename + "/a2d2").toStdString(), 1, &ds_widget::Obs::update,
                                              sensor_lights->obs);
  opt_sub = nh.subscribe<ds_sensor_msgs::OxygenConcentration>((opt_nodename + "/oxygen_concentration").toStdString(), 1,
                                                              &ds_widget::Opt::update, sensor_lights->opt);
  ctd_sub = nh.subscribe<ds_sensor_msgs::Ctd>((ctd_nodename + "/ctd").toStdString(), 1, &ds_widget::Ctd::update,
                                              sensor_lights->ctd);
  svp_sub = nh.subscribe<ds_sensor_msgs::SoundSpeed>((svp_nodename + "/sound_speed").toStdString(), 1,
                                                     &ds_widget::Svp::update, sensor_lights->svp);

  ROS_INFO_STREAM("setup connections: sensors");
  servo_fwd_sub = nh.subscribe<ds_actuator_msgs::ServoState>((servo_fwd_nodename + "/state").toStdString(), 1,
                                                             &ds_widget::Servo::update, actuators->fwd);
  servo_aft_sub = nh.subscribe<ds_actuator_msgs::ServoState>((servo_aft_nodename + "/state").toStdString(), 1,
                                                             &ds_widget::Servo::update, actuators->aft);
  prop_fwd_port_sub = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      (prop_fwd_port_nodename + "/state").toStdString(), 1, &ds_widget::Thruster::update, actuators->fwd_port);
  prop_fwd_stbd_sub = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      (prop_fwd_stbd_nodename + "/state").toStdString(), 1, &ds_widget::Thruster::update, actuators->fwd_stbd);
  prop_aft_port_sub = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      (prop_aft_port_nodename + "/state").toStdString(), 1, &ds_widget::Thruster::update, actuators->aft_port);
  prop_aft_stbd_sub = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      (prop_aft_stbd_nodename + "/state").toStdString(), 1, &ds_widget::Thruster::update, actuators->aft_stbd);
  abort_sub = nh.subscribe<ds_core_msgs::Abort>((abort_nodename).toStdString(), 1, &ds_widget::Abort::update, abort);
  syprid_status_sub = nh.subscribe<sentry_msgs::SypridState>
      ((syprid_nodename + "/syprid_state").toStdString(), 1, &ds_widget::Syprid::update_status, syprid);
  ROS_INFO_STREAM("setup connections: actuators and abort");
  // CLIENTS (button actions)
  pwr_client = nh.serviceClient<sentry_msgs::PWRCmd>((pwr_nodename + "/cmd").toStdString());
  xr1_client = nh.serviceClient<sentry_msgs::XRCmd>((xr1_nodename + "/cmd").toStdString());
  xr2_client = nh.serviceClient<sentry_msgs::XRCmd>((xr2_nodename + "/cmd").toStdString());
  xr1_deadhour_client = nh.serviceClient<sentry_msgs::DeadhourCmd>((xr1_nodename + "/deadhour").toStdString());
  xr2_deadhour_client = nh.serviceClient<sentry_msgs::DeadhourCmd>((xr2_nodename + "/deadhour").toStdString());
  batman_charge_client = nh.serviceClient<sentry_msgs::ChargeCmd>((batman_nodename + "/chg_cmd").toStdString());
  batman_bat_client = nh.serviceClient<sentry_msgs::PowerCmd>((batman_nodename + "/bat_pwr_cmd").toStdString());
  batman_shore_client = nh.serviceClient<sentry_msgs::PowerCmd>((batman_nodename + "/shore_pwr_cmd").toStdString());
  zero_paro_client = nh.serviceClient<ds_core_msgs::VoidCmd>((paro_nodename + "/zero_depth").toStdString());
  abort_client = nh.serviceClient<ds_hotel_msgs::AbortCmd>((abort_nodename + "_cmd").toStdString());
  syprid_cmd_client = nh.serviceClient<sentry_msgs::SypridCmd>((syprid_nodename + "/syprid_cmd").toStdString());
  phinsins_cal_client = nh.serviceClient<ds_core_msgs::VoidCmd>((phinsins_nodename + "/phins_manpos").toStdString());

//  std::string power_service((deckbox_nodename + "/set_power").toStdString());
//  deckbox_power_client = nh.serviceClient<sentry_msgs::DeckboxRelayCmd>(power_service);
//  std::string ethernet_service((deckbox_nodename + "/set_ethernet").toStdString());
//  deckbox_ethernet_client = nh.serviceClient<sentry_msgs::DeckboxRelayCmd>(ethernet_service);
//  std::string burnwires_service((deckbox_nodename + "/sample_burnwires").toStdString());
//  sample_burnwires_client = nh.serviceClient<sentry_msgs::SampleBurnwiresCmd>(burnwires_service);


  ROS_INFO_STREAM("setup connections: clients");
  // TIMER (update qt widgets and determine data is old)
  if (timer == Q_NULLPTR)
  {
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(tick()));
  }
  timer->start(1000);
  ROS_INFO_STREAM("started timer");
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

void SentrySitterPlugin::tick()
{
  pwr_grid->tick();
  htp_main->tick();
  htp_bat->tick();
  xr_status->tick();
  batman->tick();
  batman_lights->tick();
  for (int i = 0; i < 6; i++)
  {
    bat_p_lights[i]->tick();
  }
  shore_lights->tick();
//  deckbox_burnwires->tick();
//  deckbox_relays->tick();
//  deckbox_temperature->tick();
  sensor_lights->tick();
  nav->tick();
  compass->tick();
  actuators->tick();
  abort->tick();
}

void SentrySitterPlugin::batman_pass(ds_hotel_msgs::BatMan in_msg)
{
  batman_lights->update(in_msg);
  bat_model->batman_update(in_msg);
}

void SentrySitterPlugin::bat_pass(ds_hotel_msgs::Battery in_msg)
{
  bat_model->bat_update(in_msg);
  bat_p_lights[in_msg.idnum - 1]->update(in_msg);
}

void SentrySitterPlugin::dvl_pass(ds_sensor_msgs::Dvl in_msg)
{
  nav->update_dvl(in_msg);
  sensor_lights->dvl->update(in_msg);
}

void SentrySitterPlugin::paro_pass(ds_sensor_msgs::DepthPressure in_msg)
{
  nav->update_paro(in_msg);
  sensor_lights->paro->update(in_msg);
}

void SentrySitterPlugin::phins_pass(ds_sensor_msgs::Gyro in_msg)
{
  compass->update_model(in_msg);
  nav->update_phins(in_msg);
  sensor_lights->phins->update(in_msg);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void SentrySitterPlugin::set_config()
{
  ROS_INFO_STREAM("CHANGE CONFIG");
  QDialog* dlog = new QDialog(widget_);
  dlog->setWindowTitle("Change Config");
  QGridLayout* gr = new QGridLayout(dlog);

  pwr_edit = new ds_widget::EditPair("PWR Node", dlog, pwr_nodename);

  htp_main_edit = new ds_widget::EditPair("HTP Main Node", dlog, htp_main_nodename);
  htp_bat_edit = new ds_widget::EditPair("HTP Bat Node", dlog, htp_bat_nodename);

  xr1_edit = new ds_widget::EditPair("XR1 Node", dlog, xr1_nodename);
  xr2_edit = new ds_widget::EditPair("XR2 Node", dlog, xr2_nodename);

  batman_edit = new ds_widget::EditPair("BatMan Node", dlog, batman_nodename);
  bat_edit = new ds_widget::EditPair("Bat Path", dlog, bat_path);
  chg_edit = new ds_widget::EditPair("Chg Path", dlog, chg_path);

  reson_edit = new ds_widget::EditPair("Reson Node", dlog, reson_nodename);
  orp_edit = new ds_widget::EditPair("Orp Node", dlog, orp_nodename);
  obs_edit = new ds_widget::EditPair("Obs Node", dlog, obs_nodename);
  opt_edit = new ds_widget::EditPair("Opt Node", dlog, opt_nodename);
  ctd_edit = new ds_widget::EditPair("Ctd Node", dlog, ctd_nodename);
  svp_edit = new ds_widget::EditPair("Svp Node", dlog, svp_nodename);

  dvl_edit = new ds_widget::EditPair("Dvl Node", dlog, dvl_nodename);
  paro_edit = new ds_widget::EditPair("Paro Node", dlog, paro_nodename);
  phins_edit = new ds_widget::EditPair("Phins Node", dlog, phins_nodename);
  phinsins_edit = new ds_widget::EditPair("Phins INS Node", dlog, phinsins_nodename);

  servo_fwd_edit = new ds_widget::EditPair("Fwd Servo Node", dlog, servo_fwd_nodename);
  servo_aft_edit = new ds_widget::EditPair("Aft Servo Node", dlog, servo_aft_nodename);
  prop_fwd_port_edit = new ds_widget::EditPair("Fwd Prop Port Node", dlog, prop_fwd_port_nodename);
  prop_fwd_stbd_edit = new ds_widget::EditPair("Fwd Prop Stbd Node", dlog, prop_fwd_stbd_nodename);
  prop_aft_port_edit = new ds_widget::EditPair("Aft Prop Port Node", dlog, prop_aft_port_nodename);
  prop_aft_stbd_edit = new ds_widget::EditPair("Aft Prop Stbd Node", dlog, prop_aft_stbd_nodename);

  abort_edit = new ds_widget::EditPair("Abort Node", dlog, abort_nodename);
  deckbox_edit = new ds_widget::EditPair("Deckbox Node", dlog, deckbox_nodename);

  syprid_edit = new ds_widget::EditPair("Syprid Node", dlog, syprid_nodename);

  ds_widget::DsButton* accept_btn =
      new ds_widget::DsButton("Update subscriptions/service clients", "Update subscriptions/service clients?", dlog);

  // TODO: define a widget which is a label and an edit, so that we can do this cleaner
  gr->addWidget(pwr_edit, 0, 0);
  gr->addWidget(htp_main_edit, 1, 0);
  gr->addWidget(htp_bat_edit, 2, 0);
  gr->addWidget(xr1_edit, 3, 0);
  gr->addWidget(xr2_edit, 4, 0);
  gr->addWidget(batman_edit, 5, 0);
  gr->addWidget(bat_edit, 6, 0);
  gr->addWidget(chg_edit, 7, 0);
  gr->addWidget(abort_edit, 8, 0);

  gr->addWidget(reson_edit, 0, 1);
  gr->addWidget(orp_edit, 1, 1);
  gr->addWidget(obs_edit, 2, 1);
  gr->addWidget(opt_edit, 3, 1);
  gr->addWidget(ctd_edit, 4, 1);
  gr->addWidget(svp_edit, 5, 1);
  gr->addWidget(dvl_edit, 6, 1);
  gr->addWidget(paro_edit, 7, 1);
  gr->addWidget(phins_edit, 8, 1);
  gr->addWidget(phinsins_edit, 9, 1);

  gr->addWidget(servo_fwd_edit, 0, 2);
  gr->addWidget(servo_aft_edit, 1, 2);
  gr->addWidget(prop_fwd_port_edit, 2, 2);
  gr->addWidget(prop_fwd_stbd_edit, 3, 2);
  gr->addWidget(prop_aft_port_edit, 4, 2);
  gr->addWidget(prop_aft_stbd_edit, 5, 2);
  gr->addWidget(deckbox_edit, 6, 2);
  gr->addWidget(deckbox_edit, 7, 2);
  gr->addWidget(syprid_edit, 8, 2);

  gr->addWidget(accept_btn, 25, 0, 1, 3);

  connect(accept_btn, SIGNAL(confirmed()), dlog, SLOT(accept()));
  connect(dlog, SIGNAL(accepted()), this, SLOT(accept_config()));

  dlog->open();
}

void SentrySitterPlugin::accept_config()
{  // TODO: is there a way to improve this, coding-wise?
  ROS_ERROR_STREAM("Found callback");
  pwr_nodename = pwr_edit->get_text();
  htp_main_nodename = htp_main_edit->get_text();
  htp_bat_nodename = htp_bat_edit->get_text();
  xr1_nodename = xr1_edit->get_text();
  xr2_nodename = xr2_edit->get_text();
  batman_nodename = batman_edit->get_text();
  bat_path = bat_edit->get_text();
  chg_path = chg_edit->get_text();
  dvl_nodename = dvl_edit->get_text();
  paro_nodename = paro_edit->get_text();

  reson_nodename = reson_edit->get_text();
  orp_nodename = orp_edit->get_text();
  obs_nodename = obs_edit->get_text();
  opt_nodename = opt_edit->get_text();
  ctd_nodename = ctd_edit->get_text();
  svp_nodename = svp_edit->get_text();

  phins_nodename = phins_edit->get_text();
  phinsins_nodename = phinsins_edit->get_text();

  servo_fwd_nodename = servo_fwd_edit->get_text();
  servo_aft_nodename = servo_aft_edit->get_text();
  prop_fwd_port_nodename = prop_fwd_port_edit->get_text();
  prop_fwd_stbd_nodename = prop_fwd_stbd_edit->get_text();
  prop_aft_port_nodename = prop_aft_port_edit->get_text();
  prop_aft_stbd_nodename = prop_aft_stbd_edit->get_text();

  abort_nodename = abort_edit->get_text();
  deckbox_nodename = deckbox_edit->get_text();
  syprid_nodename = syprid_edit->get_text();

  ROS_ERROR_STREAM("Finished updating text");
  shutdownConnections();
  setupConnections();
}

void SentrySitterPlugin::chg_pop()
{
  if (!chgs_open)
  {
    chgs_open = true;
    ROS_INFO_STREAM("chg pop!");
    chg_dlog = new QWidget(widget_);
    chg_dlog->setWindowFlags(Qt::Window);
    QGridLayout* gr = new QGridLayout(chg_dlog);
    chg_dlog->setWindowTitle("Chargers");
    ds_widget::GridView* chg_grid = new ds_widget::GridView(chg_model, widget_);
    gr->addWidget(chg_grid, 0, 0);
  }
  chg_dlog->show();
}


void SentrySitterPlugin::bat_pop()
{
  if (!bats_open)
  {
    bats_open = true;
    ROS_INFO_STREAM("bat pop!");
    bat_dlog = new QWidget(widget_);
    bat_dlog->setWindowFlags(Qt::Window);
    QGridLayout* gr = new QGridLayout(bat_dlog);
    bat_dlog->setWindowTitle("Batteries");
    ds_widget::GridView* bat_grid = new ds_widget::GridView(bat_model, widget_);
    gr->addWidget(bat_grid, 0, 0);
  }
  bat_dlog->show();
}

void SentrySitterPlugin::nav_pop()
{
  if (!nav_open)
  {
    nav_open = true;
    ROS_INFO_STREAM("nav pop!");
    nav_dlog = new QWidget(widget_);
    nav_dlog->setWindowFlags(Qt::Window);
    QGridLayout* gr = new QGridLayout(nav_dlog);
    nav_dlog->setWindowTitle("Nav Sensors");
    gr->addWidget(nav, 0, 0);
  }
  else
  {
    nav_dlog->show();
  }
}
//
//    void SentrySitterPlugin::sonar_pop() {
//        if (!sonar_open){
//            sonar_open = true;
//            ROS_INFO_STREAM("sonar pop!");
//            sonar_dlog = new QWidget(widget_);
//            sonar_dlog->setWindowFlags(Qt::Window);
//            QGridLayout * gr = new QGridLayout(sonar_dlog);
//            sonar_dlog->setWindowTitle("Sonars");
//            gr->addWidget(sonar, 0, 0);
//        } else {
//            sonar_dlog->show();
//        }
//    }

void SentrySitterPlugin::actuators_pop()
{
  if (!actuators_open)
  {
    actuators_open = true;
    ROS_INFO_STREAM("actuators pop!");
    actuators_dlog = new QWidget(widget_);
    actuators_dlog->setWindowFlags(Qt::Window);
    QGridLayout* gr = new QGridLayout(actuators_dlog);
    actuators_dlog->setWindowTitle("Actuators");
    gr->addWidget(actuators, 0, 0);
  }
  else
  {
    actuators_dlog->show();
  }
}
}  // END NAMESPACE
PLUGINLIB_EXPORT_CLASS(sentrysitter::SentrySitterPlugin, rqt_gui_cpp::Plugin)
