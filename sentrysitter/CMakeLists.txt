cmake_minimum_required(VERSION 2.8.3)
project(sentrysitter)

add_compile_options(-std=c++11)
add_compile_options(-ggdb)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(catkin REQUIRED COMPONENTS
  ds_hotel_msgs
  ds_base
  ds_util
  ds_widget_defs
  roscpp
  roslint
  rqt_gui
  rqt_gui_cpp
  sentry_msgs
)

find_package(ClangFormat)
if(CLANG_FORMAT_FOUND)
    add_custom_target(clang-format-sentrysitter
            COMMENT
            "Run clang-format on all project C++ sources"
            WORKING_DIRECTORY
            ${PROJECT_SOURCE_DIR}
            COMMAND
            find src
            include
            -iname '*.h' -o -iname '*.cpp'
            | xargs ${CLANG_FORMAT_EXECUTABLE} -i
            )
endif(CLANG_FORMAT_FOUND)

if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    find_package(Qt5Widgets REQUIRED)
else()
    find_package(Qt4 COMPONENTS QtCore QtGui REQUIRED)
    include(${QT_USE_FILE})
endif()

catkin_python_setup()
roslint_cpp()

catkin_package(
        LIBRARIES ${PROJECT_NAME}
        CATKIN_DEPENDS ds_hotel_msgs ds_base ds_util ds_widget_defs roscpp rqt_gui_cpp sentry_msgs
)

set(sentrysitter_SRCS
        src/module.cpp
)

set(sentrysitter_HDRS
        include/module.h

        )

#set(sentrysitter_UIS
#        src/demo_dialog.ui
#        )

if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    qt5_wrap_cpp(sentrysitter_MOCS ${sentrysitter_HDRS})
    #    qt5_wrap_ui(sentrysitter_UIS_H ${sentrysitter_UIS})
else()
    qt4_wrap_cpp(sentrysitter_MOCS ${sentrysitter_HDRS})
    #    qt4_wrap_ui(sentrysitter_UIS_H ${sentrysitter_UIS})
endif()

#########################################################
## Build

include_directories(
        include
        ${CMAKE_CURRENT_BINARY_DIR}/..
        ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME}
        ${sentrysitter_SRCS}
        ${sentrysitter_MOCS}
        #        ${sentrysitter_UIS_H}
        )

add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(${PROJECT_NAME}
        ${catkin_LIBRARIES}
        )

if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
else()
    target_link_libraries(${PROJECT_NAME} ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY})
endif()
