# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading
#from ds_sensor_msgs.msg import *
#from ds_nav_msgs.msg import *
#from ds_hotel_msgs.msg import *
import importlib
import yaml
import sys

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
#from python_qt_binding.QtGui import QWidget
# PyQt5 change, it appears:
from PyQt5 import QtCore, QtGui, QtWidgets
# from python_qt_binding.QtGui import QDialogButtonBox

from python_qt_binding.QtWidgets import QWidget, QDialogButtonBox, QDialog, QPushButton, QLineEdit, QSpacerItem, QGroupBox, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout
# from python_qt_binding.QtGui import QSpacerItem

module_path = os.path.abspath(__file__)

class label_pair(QWidget):
    def __init__(self, name, format = "%s", parent=0):
        super(label_pair, self).__init__(parent)

        self.name = name
        self.format = format
        self.val_lbl = QLabel("", self)

        name_lbl = QLabel(name, self)
        grid = QGridLayout(self)
        grid.addWidget(name_lbl, 0, 0)
        grid.addWidget(self.val_lbl, 0, 1)
        self.setLayout(grid)
        self.set_text(0)

    def set_text(self, val):
        self.val_lbl.setText(self.format % val)

class sensor(QGroupBox):
    def __init__(self, name, attributes, address, module, type, parent=0):
        super(sensor, self).__init__(name, parent)
        self.name = name
        self.address = address
        self.type = type
        self.module = module

        grid = QGridLayout(self)
        self.attr_lbls = []
        i = 0
        for (name, format) in sorted(attributes.iteritems()):
            self.attr_lbls.append(label_pair(name, format, self))
            grid.addWidget(self.attr_lbls[i], i+1, 0)
            i += 1
        verticalSpacer = QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        grid.addItem(verticalSpacer, i+1, 0)
        self.setLayout(grid)

    def update(self, msg):
        for lbl in self.attr_lbls:
            val = getattr(msg, lbl.name)
            lbl.set_text(val)

class SensorPlugin(Plugin):

    def __init__(self, context):
        super(SensorPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('sensorPlugin')

        # self.yaml_file = "/config/sensors/sensor_config.yaml"


        self._widget = QWidget()
        self.big_grid = QGridLayout()
        self._widget.setLayout(self.big_grid)
        self._widget.setObjectName('sensorPluginUi')
        self._init_stylesheet()
        self.setup_config_btn()


        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # self._init_sensors()
        # self.setup_widget()
        # self.setup_connections()

        context.add_widget(self._widget)

    def _init_stylesheet(self):
        rospack = rospkg.RosPack()
        sentrysitter_path = rospack.get_path('sentrysitter')
        style_file = sentrysitter_path + "/sentrysitter.qss"
        with open(style_file, 'r') as f:
            self._widget.setStyleSheet(f.read())

    def _init_sensors(self):
        rospack = rospkg.RosPack()
        config_path = rospack.get_path('sentry_config')
        self.sensor_data = {}
        self.sensor_data = yaml.load(open(config_path + self.yaml_file, 'r'))
        # print self.sensor_data

    def setup_config_btn(self):
        self.config_btn = QPushButton("Set yaml file", self._widget)
        self.big_grid.addWidget(self.config_btn, 1, 0)
        self.config_btn.clicked.connect(self.pop_config)

    def pop_config(self):
        d_log = QDialog(self._widget)
        d_log.setWindowTitle("Set yaml file")
        grid = QGridLayout(d_log)
        ok_btn = QPushButton("Ok", d_log)
        cancel_btn = QPushButton("Cancel", d_log)
        self.config_edit = QLineEdit(self.yaml_file, d_log)
        hSpacer = QSpacerItem(200, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        grid.addWidget(self.config_edit, 0, 0, 1, 4)
        grid.addItem(hSpacer, 1, 0, 1, 2)
        grid.addWidget(ok_btn, 1, 3)
        grid.addWidget(cancel_btn, 1, 2)
        ok_btn.clicked.connect(self.accept_config)
        ok_btn.clicked.connect(d_log.accept)
        cancel_btn.clicked.connect(d_log.reject)
        d_log.exec_()

    def accept_config(self):
        self.yaml_file = self.config_edit.text()
        self._init_sensors()
        self.setup_widget()
        self.shutdown_connections()
        self.setup_connections()

    def setup_widget(self):
        # Create QWidget
        self.sensor_widget = QWidget(self._widget)
        grid = QGridLayout(self.sensor_widget)
        self.sensors = []
        self.groups = {}
        for (name, value) in self.sensor_data.iteritems():
            print name
            if value['group'] in self.groups:
                self.groups[value['group']][1] += 1
            else:
                self.groups[value['group']] = [len(self.groups), 0]
            i = self.groups[value['group']][1]
            j = self.groups[value['group']][0]
            snsr = sensor(value['display_name'], value['attributes'], value['address'], value['msg_package'], value['msg_type'], self.sensor_widget)
            self.sensors.append(snsr)
            grid.addWidget(snsr, j, i)
        self.big_grid.addWidget(self.sensor_widget, 0, 0)

    def setup_connections(self):
        self.subs = []
        for sensor in self.sensors:
            #rospy.logerr('Loading \"import %s from %s\"' % (sensor.type, sensor.module))
            mod = importlib.import_module(sensor.module + '.msg._' + sensor.type)
            sub = rospy.Subscriber(sensor.address, getattr(mod, sensor.type), sensor.update)
            self.subs.append(sub)
        # self.config_btn.clicked.disconnect(self.pop_config())

    def shutdown_connections(self):
        for sub in self.subs:
            sub.unregister()

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self.shutdown_connections()
        # self._sensor_subscription.unregister()
        # self.timer.stop()
        # self.config_btn.clicked.disconnect(self.pop_config())

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        instance_settings.set_value("yaml_file", self.yaml_file)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        self.yaml_file = instance_settings.value("yaml_file", "/config/sensors/sensor_config.yaml")
        self._init_sensors()
        self.setup_widget()
        self.setup_connections()
        pass
