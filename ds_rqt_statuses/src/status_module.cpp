/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 6/6/18.
//

#include "../include/status_module.h"

namespace ds_rqt_statuses
{
StatusPlugin::StatusPlugin() : rqt_gui_cpp::Plugin(), widget_(0)
{
  // Constructor is called first before initPlugin function, needless to say.
  // give QObjects reasonable names
  setObjectName("StatusPlugin");
}

void StatusPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  QFile File("src/sentry_gui/style.qss");
  File.open(QFile::ReadOnly);
  QString StyleSheet = QLatin1String(File.readAll());

  // access standalone command line arguments
  QStringList argv = context.argv();

  setupConnections();

  // create QWidget
  widget_ = new QWidget();

  widget_->setStyleSheet(StyleSheet);

  setup_widget();

  setupConnections();

  // add widget to the user interface
  context.addWidget(widget_);
}

void StatusPlugin::shutdownPlugin()
{
  // unregister all publishers, subscribers, clients, and timers here
  status_sub.shutdown();
  timer->stop();
}

void StatusPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
}

void StatusPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                                   const qt_gui_cpp::Settings& instance_settings)
{
}

/// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void StatusPlugin::setup_widget()
{
  QGridLayout* grid = new QGridLayout(widget_);
  status_widget = new ds_widget::Status(widget_);
  grid->addWidget(status_widget, 0, 0);
}

void StatusPlugin::setupConnections()
{
  status_sub = getMTNodeHandle().subscribe<ds_core_msgs::Status>("/sentry/status", 50, &ds_widget::Status::update,
                                                                 status_widget);
  timer = new QTimer(this);
  connect(timer, SIGNAL(timeout()), this, SLOT(tick()));
  timer->start(1);
}

void StatusPlugin::tick()
{
  status_widget->tick();
}

}  // END NAMESPACE
PLUGINLIB_EXPORT_CLASS(ds_rqt_statuses::StatusPlugin, rqt_gui_cpp::Plugin)
