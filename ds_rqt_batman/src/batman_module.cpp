/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/1/18.
//

#include "../include/batman_module.h"
#include <pluginlib/class_list_macros.h>
#include <sentry_msgs/ChargeCmd.h>
#include <sentry_msgs/PowerCmd.h>
//#include <QStringList>
#include <ros/node_handle.h>

namespace ds_rqt_batman
{
BatManPlugin::BatManPlugin() : rqt_gui_cpp::Plugin(), widget_(0)
{
  // Constructor is called first before initPlugin function, needless to say.

  // give QObjects reasonable names
  setObjectName("BatManPlugin");
}

void BatManPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  //        nh = Nodelet();
  // access standalone command line arguments
  QStringList argv = context.argv();
  // create QWidget
  widget_ = new QWidget();
  // extend the widget with all attributes and children from UI file

  ui_.setupUi(widget_);
  setup_widget();
  // add widget to the user interface
  context.addWidget(widget_);

  chg_client_ = getMTNodeHandle().serviceClient<sentry_msgs::ChargeCmd>("/demo/bat/batman/chg_cmd");
  bat_pwr_client_ = getMTNodeHandle().serviceClient<sentry_msgs::PowerCmd>("/demo/bat/batman/bat_pwr_cmd");

  connect(chgBtn_on, SIGNAL(clicked()), this, SLOT(charge_on()));
  connect(chgBtn_off, SIGNAL(clicked()), this, SLOT(charge_off()));
  connect(batBtn_on, SIGNAL(clicked()), this, SLOT(bat_on()));
  connect(batBtn_off, SIGNAL(clicked()), this, SLOT(bat_off()));
}

void BatManPlugin::shutdownPlugin()
{
  // unregister all publishers here
  chg_client_.shutdown();
  bat_pwr_client_.shutdown();
}

void BatManPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
  // instance_settings.setValue(k, v)
}

void BatManPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                                   const qt_gui_cpp::Settings& instance_settings)
{
  // v = instance_settings.value(k)
}

/*bool hasConfiguration() const
{
  return true;
}
void triggerConfiguration()
{
  // Usually used to open a dialog to offer the user a set of configuration
}*/

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx

void BatManPlugin::setup_widget()
{
  grid = new QGridLayout(widget_);
  chgBtn_on = new QPushButton("Start Charge", widget_);
  chgBtn_off = new QPushButton("Stop Charge", widget_);
  batBtn_on = new QPushButton("Bat On", widget_);
  batBtn_off = new QPushButton("Bat off", widget_);

  grid->addWidget(chgBtn_on, 0, 0);
  grid->addWidget(chgBtn_off, 0, 1);
  grid->addWidget(batBtn_on, 1, 0);
  grid->addWidget(batBtn_off, 1, 1);
}

void BatManPlugin::charge_on()
{
  sentry_msgs::ChargeCmd srv;
  srv.request.command = 2;
  ROS_INFO_STREAM("CHARGE CLICKED ON");
  if (chg_client_.call(srv))
    ROS_INFO_STREAM("success");
  else
    ROS_INFO_STREAM("failure");
}

void BatManPlugin::charge_off()
{
  sentry_msgs::ChargeCmd srv;
  srv.request.command = 1;
  ROS_INFO_STREAM("CHARGE CLICKED OFF");
  if (chg_client_.call(srv))
    ROS_INFO_STREAM("success");
  else
    ROS_INFO_STREAM("failure");
}

void BatManPlugin::bat_on()
{
  sentry_msgs::PowerCmd srv;
  srv.request.command = 2;
  ROS_INFO_STREAM("POWER CLICKED ON");
  if (bat_pwr_client_.call(srv))
    ROS_INFO_STREAM("success");
  else
    ROS_INFO_STREAM("failure");
}

void BatManPlugin::bat_off()
{
  sentry_msgs::PowerCmd srv;
  srv.request.command = 1;
  ROS_INFO_STREAM("POWER CLICKED OFF");
  if (bat_pwr_client_.call(srv))
    ROS_INFO_STREAM("success");
  else
    ROS_INFO_STREAM("failure");
}

}  // namespace ds_rqt_batman
PLUGINLIB_EXPORT_CLASS(ds_rqt_batman::BatManPlugin, rqt_gui_cpp::Plugin)