/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/14/18.
//

#ifndef PROJECT_XR_MODULE_H
#define PROJECT_XR_MODULE_H

#include <rqt_gui_cpp/plugin.h>

#include <ds_hotel_msgs/XR.h>
#include <sentry_msgs/XRCmd.h>
#include <sentry_msgs/DeadhourCmd.h>

#include <vector>
#include <list>

#include <ros/service_client.h>
#include <ros/subscriber.h>
#include <ros/timer.h>

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QSpacerItem>
#include <QFont>
#include <QDoubleValidator>

namespace ds_rqt_xr
{
class XrPlugin : public rqt_gui_cpp::Plugin
{
  Q_OBJECT
public:
  XrPlugin();
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings);

protected:
  void setupWidget();
  void add_xr_display(QFont font, const QString& address, const QString& status, const QString& deadhour,
                      const QString& dcams, const QString& burnwires, const QString& shortdead);
  void setupConnections();
  void xr_callback(ds_hotel_msgs::XR msg);
  void timer_callback(const ros::TimerEvent&);

private:
  QWidget* widget_;
  QGridLayout* grid;

  QPushButton *btn_hold, *btn_release, *btn_burn, *btn_snuff;
  QLineEdit* edit_deadhour;
  QPushButton* btn_deadhour;
  QPushButton *btn_hold_stbd, *btn_hold_port, *btn_hold_desc;
  QPushButton *btn_release_stbd, *btn_release_port, *btn_release_desc;
  QPushButton *btn_burn_stbd, *btn_burn_port, *btn_burn_desc;
  QPushButton *btn_snuff_stbd, *btn_snuff_port, *btn_snuff_desc;

  std::vector<QLabel *> lbl_address, lbl_status, lbl_deadhour, lbl_dcams, lbl_burnwires, lbl_shortdead, lbl_timesince;
  std::vector<int> timesince;

  ros::Subscriber xr1_sub, xr2_sub;
  ros::ServiceClient xr1_client, xr2_client;
  ros::ServiceClient xr1_deadhour_client, xr2_deadhour_client;
  ros::Timer timer;

private slots:
  void hold();
  void release();
  void burn();
  void snuff();
  void deadhour_set();
  void hold_stbd();
  void hold_port();
  void hold_desc();
  void release_stbd();
  void release_port();
  void release_desc();

  void burn_stbd();
  void burn_port();
  void burn_desc();
  void snuff_stbd();
  void snuff_port();
  void snuff_desc();
};  // END CLASS

}  // END NAMESPACE

#endif  // PROJECT_XR_MODULE_H
