/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/14/18.
//

// from rov/src/xr-sm-notes.markdown
//$XR: XR index. Currently 1 or 2.
//$DCAM: DCAM index. Currently 1 or 2.
//$STATE: Desired DCAM state: 0: Drop, 1: Hold
//
//        XR_BURN $XR $BURN $STATE - $XR: XR index. Currently 1 or 2. - $BURN: BURN wire. Currently 1 or 2. - $STATE:
//        BURN wire state: 0: Off, 1: On
//        Mappings
//
// hold_port: XR_DCAM 1 2 1 hold_stbd: XR_DCAM 2 2 1 hold_descent: XR_DCAM 2 1 1
//
// open_port: XR_DCAM 1 2 0 open_stbd: XR_DCAM 2 2 0 open_descent: XR_DCAM 2 1 0
//
// burn_port: XR_BURN 2 2 1 burn_stbd: XR_BURN 1 2 1 burn_descent: XR_BURN 1 1 1
//
// snuf_port: XR_BURN 2 2 0 snuf_stbd: XR_BURN 1 2 0 snuf_descent: XR_BURN 1 1 0

#include "../include/xr_module.h"
#include <pluginlib/class_list_macros.h>
#include <ros/node_handle.h>

namespace ds_rqt_xr
{
XrPlugin::XrPlugin() : rqt_gui_cpp::Plugin(), widget_(0)
{
  // Constructor is called first before initPlugin function, needless to say.
  // give QObjects reasonable names
  setObjectName("XrPlugin");
}

void XrPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  // access standalone command line arguments
  QStringList argv = context.argv();
  // create QWidget
  widget_ = new QWidget();

  setupWidget();

  // add widget to the user interface
  context.addWidget(widget_);

  setupConnections();
}

void XrPlugin::shutdownPlugin()
{
  // unregister all publishers, subscribers, clients, and timers here
  xr1_sub.shutdown();
  xr2_sub.shutdown();
  xr1_client.shutdown();
  xr2_client.shutdown();
  timer.stop();
}

void XrPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
}

void XrPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings)
{
}

/// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void XrPlugin::setupWidget()
{
  grid = new QGridLayout(widget_);

  QFont font1("Times", 16, QFont::Bold);
  QFont font2("Times", 24, QFont::Bold);

  edit_deadhour = new QLineEdit("", widget_);
  btn_deadhour = new QPushButton("Send Deadhour", widget_);

  QLabel* all_lbl = new QLabel("All", widget_);
  all_lbl->setFont(font1);
  btn_hold = new QPushButton("Hold", widget_);
  btn_release = new QPushButton("Release", widget_);
  btn_burn = new QPushButton("Burn", widget_);
  btn_snuff = new QPushButton("Snuff", widget_);

  QLabel* stbd_lbl = new QLabel("Starboard", widget_);
  stbd_lbl->setFont(font1);
  btn_hold_stbd = new QPushButton("Hold", widget_);
  btn_release_stbd = new QPushButton("Release", widget_);
  btn_burn_stbd = new QPushButton("Burn", widget_);
  btn_snuff_stbd = new QPushButton("Snuff", widget_);

  QLabel* port_lbl = new QLabel("Port", widget_);
  port_lbl->setFont(font1);
  btn_hold_port = new QPushButton("Hold", widget_);
  btn_release_port = new QPushButton("Release", widget_);
  btn_burn_port = new QPushButton("Burn", widget_);
  btn_snuff_port = new QPushButton("Snuff", widget_);

  QLabel* desc_lbl = new QLabel("Descent", widget_);
  desc_lbl->setFont(font1);
  btn_hold_desc = new QPushButton("Hold", widget_);
  btn_release_desc = new QPushButton("Release", widget_);
  btn_burn_desc = new QPushButton("Burn", widget_);
  btn_snuff_desc = new QPushButton("Snuff", widget_);

  QDoubleValidator* valid = new QDoubleValidator(edit_deadhour);
  edit_deadhour->setValidator(valid);
  edit_deadhour->setPlaceholderText("0.0-255.0");

  grid->addWidget(edit_deadhour, 0, 0, 1, 3);
  grid->addWidget(btn_deadhour, 0, 3);

  grid->addWidget(all_lbl, 1, 0);
  grid->addWidget(btn_hold, 2, 0);
  grid->addWidget(btn_release, 3, 0);
  grid->addWidget(btn_burn, 4, 0);
  grid->addWidget(btn_snuff, 5, 0);

  grid->addWidget(stbd_lbl, 1, 1);
  grid->addWidget(btn_hold_stbd, 2, 1);
  grid->addWidget(btn_release_stbd, 3, 1);
  grid->addWidget(btn_burn_stbd, 4, 1);
  grid->addWidget(btn_snuff_stbd, 5, 1);

  grid->addWidget(port_lbl, 1, 2);
  grid->addWidget(btn_hold_port, 2, 2);
  grid->addWidget(btn_release_port, 3, 2);
  grid->addWidget(btn_burn_port, 4, 2);
  grid->addWidget(btn_snuff_port, 5, 2);

  grid->addWidget(desc_lbl, 1, 3);
  grid->addWidget(btn_hold_desc, 2, 3);
  grid->addWidget(btn_release_desc, 3, 3);
  grid->addWidget(btn_burn_desc, 4, 3);
  grid->addWidget(btn_snuff_desc, 5, 3);

  add_xr_display(font1, "Address:", "Status:", "Deadhour:", "Dcams:", "Burnwires:", "Short Dead:");
  add_xr_display(font2, "", "", "", "", "", "");
  add_xr_display(font2, "", "", "", "", "", "");
}

void XrPlugin::add_xr_display(QFont font, const QString& address, const QString& status, const QString& deadhour,
                              const QString& dcams, const QString& burnwires, const QString& shortdead)
{
  timesince.push_back(0);

  lbl_address.push_back(new QLabel());
  lbl_status.push_back(new QLabel());
  lbl_deadhour.push_back(new QLabel());
  lbl_dcams.push_back(new QLabel());
  lbl_burnwires.push_back(new QLabel());
  lbl_shortdead.push_back(new QLabel());
  lbl_timesince.push_back(new QLabel());

  int i = lbl_address.size() - 1;

  grid->addWidget(lbl_address[i], 0, i + 5);
  grid->addWidget(lbl_status[i], 1, i + 5);
  grid->addWidget(lbl_deadhour[i], 2, i + 5);
  grid->addWidget(lbl_dcams[i], 3, i + 5);
  grid->addWidget(lbl_burnwires[i], 4, i + 5);
  grid->addWidget(lbl_shortdead[i], 5, i + 5);
  grid->addWidget(lbl_timesince[i], 7, i + 5);
  lbl_timesince[i]->hide();

  QSpacerItem* verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);
  grid->addItem(verticalSpacer, 6, i + 5);

  lbl_address[i]->setText(address);
  lbl_status[i]->setText(status);
  lbl_deadhour[i]->setText(deadhour);
  lbl_dcams[i]->setText(dcams);
  lbl_burnwires[i]->setText(burnwires);
  lbl_shortdead[i]->setText(shortdead);

  lbl_address[i]->setFont(font);
  lbl_status[i]->setFont(font);
  lbl_deadhour[i]->setFont(font);
  lbl_dcams[i]->setFont(font);
  lbl_burnwires[i]->setFont(font);
  lbl_shortdead[i]->setFont(font);
}

void XrPlugin::setupConnections()
{
  xr1_sub = getMTNodeHandle().subscribe<ds_hotel_msgs::XR>("/sentry/grd/xr1", 1, &XrPlugin::xr_callback, this);
  xr2_sub = getMTNodeHandle().subscribe<ds_hotel_msgs::XR>("/sentry/grd/xr2", 1, &XrPlugin::xr_callback, this);
  xr1_client = getMTNodeHandle().serviceClient<sentry_msgs::XRCmd>("/sentry/grd/xr1/cmd");
  xr2_client = getMTNodeHandle().serviceClient<sentry_msgs::XRCmd>("/sentry/grd/xr2/cmd");
  xr1_deadhour_client = getMTNodeHandle().serviceClient<sentry_msgs::DeadhourCmd>("/sentry/grd/xr1/deadhour");
  xr2_deadhour_client = getMTNodeHandle().serviceClient<sentry_msgs::DeadhourCmd>("/sentry/grd/xr2/deadhour");

  timer = getMTNodeHandle().createTimer(ros::Duration(1), &XrPlugin::timer_callback, this);

  connect(btn_hold, SIGNAL(clicked()), this, SLOT(hold()));
  connect(btn_release, SIGNAL(clicked()), this, SLOT(release()));
  connect(btn_burn, SIGNAL(clicked()), this, SLOT(burn()));
  connect(btn_snuff, SIGNAL(clicked()), this, SLOT(snuff()));
  connect(btn_deadhour, SIGNAL(clicked()), this, SLOT(deadhour_set()));
  connect(btn_hold_stbd, SIGNAL(clicked()), this, SLOT(hold_stbd()));
  connect(btn_release_stbd, SIGNAL(clicked()), this, SLOT(release_stbd()));
  connect(btn_hold_port, SIGNAL(clicked()), this, SLOT(hold_port()));
  connect(btn_release_port, SIGNAL(clicked()), this, SLOT(release_port()));
  connect(btn_hold_desc, SIGNAL(clicked()), this, SLOT(hold_desc()));
  connect(btn_release_desc, SIGNAL(clicked()), this, SLOT(release_desc()));

  connect(btn_burn_stbd, SIGNAL(clicked()), this, SLOT(burn_stbd()));
  connect(btn_snuff_stbd, SIGNAL(clicked()), this, SLOT(snuff_stbd()));
  connect(btn_burn_port, SIGNAL(clicked()), this, SLOT(burn_port()));
  connect(btn_snuff_port, SIGNAL(clicked()), this, SLOT(snuff_port()));
  connect(btn_burn_desc, SIGNAL(clicked()), this, SLOT(burn_desc()));
  connect(btn_snuff_desc, SIGNAL(clicked()), this, SLOT(snuff_desc()));
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void XrPlugin::hold()
{
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_CLOSE;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("XR1 SEND HOLD ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 SEND HOLD ON FAILURE");
  }
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("XR2 SEND HOLD ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 SEND HOLD ON FAILURE");
  }
}

void XrPlugin::release()
{
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_OPEN;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("XR1 SEND HOLD OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 SEND HOLD OFF FAILURE");
  }
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("XR2 SEND HOLD OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 SEND HOLD OFF FAILURE");
  }
}

void XrPlugin::burn()
{
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_ON;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("XR1 SEND BURN HIGH SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 SEND BURN HIGH FAILURE");
  }
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("XR2 SEND BURN HIGH SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 SEND BURN HIGH FAILURE");
  }
}

void XrPlugin::snuff()
{
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_OFF;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("XR1 SEND BURN LOW SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 SEND BURN LOW FAILURE");
  }
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("XR2 SEND BURN LOW SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 SEND BURN LOW FAILURE");
  }
}

void XrPlugin::deadhour_set()
{
  sentry_msgs::DeadhourCmd cmd;

  int deadhour = edit_deadhour->text().toInt();

  cmd.request.deadhour = deadhour;
  if (xr1_deadhour_client.call(cmd))
  {
    ROS_INFO_STREAM("XR1 DEADHOUR SET: " << deadhour << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR1 DEADHOUR SET: " << deadhour << " FAILURE");
  }
  if (xr2_deadhour_client.call(cmd))
  {
    ROS_INFO_STREAM("XR2 DEADHOUR SET: " << deadhour << " SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("XR2 DEADHOUR SET: " << deadhour << " FAILURE");
  }
}

void XrPlugin::hold_stbd()
{  // hold_stbd: XR_DCAM 2 2 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_CLOSE_2;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("STBD SEND HOLD ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("STBD SEND HOLD ON FAILURE");
  }
}

void XrPlugin::release_stbd()
{  // open_stbd: XR_DCAM 2 2 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_OPEN_2;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("STBD HOLD OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("STBD SEND HOLD OFF FAILURE");
  }
}

void XrPlugin::hold_port()
{  // hold_port: XR_DCAM 1 2 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_CLOSE_2;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("PORT SEND HOLD ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("PORT SEND HOLD ON FAILURE");
  }
}

void XrPlugin::release_port()
{  // open_port: XR_DCAM 1 2 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_OPEN_2;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND HOLD OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND HOLD OFF FAILURE");
  }
}

void XrPlugin::hold_desc()
{  // hold_descent: XR_DCAM 2 1 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_CLOSE_1;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND HOLD ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND HOLD ON FAILURE");
  }
}

void XrPlugin::release_desc()
{  // open_descent: XR_DCAM 2 1 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_DCAM_OPEN_1;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND HOLD OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND HOLD OFF FAILURE");
  }
}

void XrPlugin::burn_stbd()
{  // burn_stbd: XR_BURN 1 2 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_ON_2;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("STBD SEND BURNWIRE ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("STBD SEND BURNWIRE ON FAILURE");
  }
}

void XrPlugin::snuff_stbd()
{  // snuf_stbd: XR_BURN 1 2 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_OFF_2;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("STBD BURNWIRE OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("STBD SEND BURNWIRE OFF FAILURE");
  }
}

void XrPlugin::burn_port()
{  // burn_port: XR_BURN 2 2 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_ON_2;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("PORT SEND BURNWIRE ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("PORT SEND BURNWIRE ON FAILURE");
  }
}

void XrPlugin::snuff_port()
{  // snuf_port: XR_BURN 2 2 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_OFF_2;
  if (xr2_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND BURNWIRE OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND BURNWIRE OFF FAILURE");
  }
}

void XrPlugin::burn_desc()
{  // burn_descent: XR_BURN 1 1 1
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_ON_1;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND BURNWIRE ON SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND BURNWIRE ON FAILURE");
  }
}

void XrPlugin::snuff_desc()
{  // snuf_descent: XR_BURN 1 1 0
  sentry_msgs::XRCmd cmd;
  cmd.request.command = cmd.request.XR_CMD_BURNWIRE_OFF_1;
  if (xr1_client.call(cmd))
  {
    ROS_INFO_STREAM("DESC SEND BURNWIRE OFF SUCCESS");
  }
  else
  {
    ROS_ERROR_STREAM("DESC SEND BURNWIRE OFF FAILURE");
  }
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void XrPlugin::timer_callback(const ros::TimerEvent&)
{
  for (int i = 0; i < timesince.size(); i++)
  {
    timesince[i]++;
    lbl_timesince[i]->setText("");
  }
}

void XrPlugin::xr_callback(ds_hotel_msgs::XR msg)
{
  int i = 1;

  lbl_address[i]->setText(QString::fromStdString(msg.address));
  if (msg.good)
  {
    lbl_status[i]->setText("Good");
  }
  else
  {
    lbl_status[i]->setText("Bad");
  }

  lbl_deadhour[i]->setText(QString::number(msg.deadhour));
  if (msg.motor_2_drop && msg.motor_1_drop)
  {
    lbl_dcams[i]->setText("Dropping motors " + QString::number(msg.motor_1_secs));
  }
  else if (msg.motor_1_hold && msg.motor_2_hold)
  {
    lbl_dcams[i]->setText("Holding motors " + QString::number(msg.motor_1_secs));
  }
  else
  {
    lbl_dcams[i]->setText("Off");
  }
  if (msg.burnwire_2_drive && msg.burnwire_1_drive)
  {
    lbl_burnwires[i]->setText("Driving " + QString::number(msg.burnwire_1_secs));
  }
  else
  {
    lbl_burnwires[i]->setText("Off");
  }
  lbl_shortdead[i]->setText(QString::number(msg.short_deadsecs));

  timesince[i] = 0;
  lbl_timesince[i]->setText("0");
  lbl_timesince[i]->show();
}

}  // END NAMESPACE
PLUGINLIB_EXPORT_CLASS(ds_rqt_xr::XrPlugin, rqt_gui_cpp::Plugin)
