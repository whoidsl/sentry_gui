/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/15/18.
//

#ifndef PROJECT_PWR_MODULE_H
#define PROJECT_PWR_MODULE_H

#include <rqt_gui_cpp/plugin.h>

#include <ds_util/int_to_hex.h>
#include <sentry_msgs/PWRCmd.h>
#include <ds_widget/pwr_button.h>
#include <ds_widget/light.h>
#include <qapplication.h>
#include <ds_hotel_msgs/PWR.h>

#include <vector>
#include <list>

#include <ros/service_client.h>
#include <ros/subscriber.h>
#include <ros/timer.h>

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>
#include <QGridLayout>
#include <QSpacerItem>
#include <QFont>
#include <QIntValidator>

namespace ds_rqt_pwr
{
class PwrPlugin : public rqt_gui_cpp::Plugin
{
  Q_OBJECT
public:
  PwrPlugin();
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings);

protected:
  void setupWidget();
  void add_pwr_btn(QString _id, int _address, int _num, bool c_on, bool c_off);
  void add_pwr_btn(std::string _id, int _address, int _num, bool c_on, bool c_off)
  {
    QString qstr = QString::fromStdString(_id);
    add_pwr_btn(qstr, _address, _num, c_on, c_off);
  }
  void add_pwr_light(int _num);
  void setupConnections();
  void pwr_callback(ds_hotel_msgs::PWR msg);
  void timer_callback(const ros::TimerEvent&);

private:
  int max_width;

  QWidget* widget_;
  QGridLayout* grid;

  std::vector<ds_widget::PwrButton*> pwr_btns;
  std::vector<QLabel*> pwr_lbls;
  std::vector<ds_widget::Light*> lights;

  ros::Subscriber pwr_sub;
  ros::ServiceClient pwr_client;
  ros::Timer timer;

private slots:
  void hold_on();
  void hold_off();
  void burn_high();
  void burn_low();
  void deadhour_set();
};  // END CLASS

}  // END NAMESPACE

#endif  // PROJECT_PWR_MODULE_H
