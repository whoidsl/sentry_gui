/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by jvaccaro on 2/15/18.
//

#include "../include/pwr_module.h"
#include <pluginlib/class_list_macros.h>
#include <ros/node_handle.h>
#include <ros/param.h>
#include <QFile>

namespace ds_rqt_pwr
{
PwrPlugin::PwrPlugin() : rqt_gui_cpp::Plugin(), widget_(0)
{
  // Constructor is called first before initPlugin function, needless to say.
  // give QObjects reasonable names
  setObjectName("PwrPlugin");
}

void PwrPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  QFile File("src/sentry_gui/style.qss");
  File.open(QFile::ReadOnly);
  QString StyleSheet = QLatin1String(File.readAll());

  // access standalone command line arguments
  QStringList argv = context.argv();

  setupConnections();

  // create QWidget
  widget_ = new QWidget();

  widget_->setStyleSheet(StyleSheet);

  setupWidget();

  // add widget to the user interface
  context.addWidget(widget_);
}

void PwrPlugin::shutdownPlugin()
{
  // unregister all publishers, subscribers, clients, and timers here
  pwr_sub.shutdown();
  pwr_client.shutdown();
  timer.stop();
}

void PwrPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
}

void PwrPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                                const qt_gui_cpp::Settings& instance_settings)
{
}

/// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void PwrPlugin::setupWidget()
{
  grid = new QGridLayout(widget_);

  max_width = 5;

  for (int i = 0; i < 8; i++)
  {
    QLabel* lbl = new QLabel();
    grid->addWidget(lbl, i, 7);
    pwr_lbls.push_back(lbl);
  }

  int num_btns = 0;
  std::string sensor_address;
  bool confirm_on;
  bool confirm_off;
  std::string i_str;

  for (uint16_t i = 0x00; i <= 0xFF; i++)
  {
    i_str = ds_util::int_to_hex(i);
    sensor_address = ros::param::param<std::string>("/demo/grd/pwr/addresses/x" + i_str, "");
    confirm_on = ros::param::param<bool>("/demo/grd/pwr/addresses/x" + i_str + "/confirm_on", "false");
    confirm_off = ros::param::param<bool>("/demo/grd/pwr/addresses/x" + i_str + "/confirm_off", "false");
    if (sensor_address != "")
    {  // If there was an actual value set by the parameter server
      add_pwr_btn(sensor_address, i, num_btns, confirm_on, confirm_off);
      //                add_pwr_btn(sensor_address, i, num_btns);
      num_btns++;
    }
  }
  for (num_btns; num_btns < 30; num_btns++)
  {
    ROS_INFO_STREAM("num btns: " << num_btns);
    //            add_pwr_light(num_btns);
    //                auto rl = new ds_widget::RoundLight();
    //                grid->addWidget(rl, i % max_width, i/max_width);
    //            num_btns ++;
  }

  QFont font1("Times", 16, QFont::Bold);
  QFont font2("Times", 24, QFont::Bold);
}

void PwrPlugin::add_pwr_btn(QString _name, int _address, int _num, bool c_on, bool c_off)
{
  ds_widget::PwrButton* new_btn = new ds_widget::PwrButton(_name, _address, &pwr_client, c_on, c_off, widget_);

  connect(new_btn, SIGNAL(clicked()), new_btn, SLOT(pwr_click()));

  grid->addWidget(new_btn, _num % max_width, _num / max_width);
  pwr_btns.push_back(new_btn);
}

void PwrPlugin::add_pwr_light(int _num)
{
  //        ds_widget::Light * new_light = new ds_widget::Light("round_green.svg",
  //                                              "triangle_red.svg",
  //                                              "square_yellow.svg");
  //
  //        grid->addWidget(new_light, _num / max_width, _num % max_width);
  //        lights.push_back(new_light);
}

void PwrPlugin::setupConnections()
{
  pwr_sub = getMTNodeHandle().subscribe<ds_hotel_msgs::PWR>("/demo/grd/pwr", 1, &PwrPlugin::pwr_callback, this);
  pwr_client = getMTNodeHandle().serviceClient<sentry_msgs::PWRCmd>("/demo/grd/pwr/cmd");
  timer = getMTNodeHandle().createTimer(ros::Duration(1), &PwrPlugin::timer_callback, this);
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

void PwrPlugin::timer_callback(const ros::TimerEvent&)
{
  for (int i = 0; i < pwr_btns.size(); i++)
  {
  }
}

void PwrPlugin::pwr_callback(ds_hotel_msgs::PWR msg)
{
  int size_msg = msg.devices.size();
  int size_btn = pwr_btns.size();

  for (int i = 0; i < pwr_btns.size(); i++)
  {
    if (msg.devices[i].name == pwr_btns[i]->get_name().toStdString())
    {
      pwr_btns[i]->set_color(msg.devices[i].is_on);
    }
  }

  for (int i = 0; i < 8; i++)
  {
    pwr_lbls[i]->setText(QString::number(msg.pwr_cmd[i]));
  }
}

}  // END NAMESPACE
PLUGINLIB_EXPORT_CLASS(ds_rqt_pwr::PwrPlugin, rqt_gui_cpp::Plugin)
